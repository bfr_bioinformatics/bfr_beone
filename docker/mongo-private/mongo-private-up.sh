#!/bin/bash
set -Eeuo pipefail

## (1) Argument logic - template from https://betterdev.blog/minimal-safe-bash-script-template/
cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "Done."
}

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS]

Description:
This script initialises a BeONE MongoDB via Docker-Compose.
It generates/overwrites the files docker-compose.yml and mongo-init.js with an individualized config.
For more information, please visit https://gitlab.com/bfr_bioinformatics/bfr_beone

Options:
  -u, --username             Username string, e.g. your institute 'bfr'
  -w, --password             Password string, default: ''
  -p, --port                 Local port for the MongoDB, default: '27017'
  -n, --hostname             Hostname string as prefix for the credentials/.uri filename

EOF
  exit
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  username=''
  port=''
  hostname=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -u | --username)
      username="$2"
      shift 2
      ;;
    -p | --port)
      port="$2"
      shift 2
      ;;
    -n | --hostname)
      hostname="$2"
      shift 2
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
  done

  ## check required params and arguments
  [[ -z "${username-}" ]] && die "Please provide a parameter for the argument --username. Try $(basename "${BASH_SOURCE[0]}") --help."
  [[ -z "${hostname-}" ]] && die "Please provide a parameter for the argument --hostname. Try $(basename "${BASH_SOURCE[0]}") --help."
  [[ -z "${password-}" ]] && password="mypassword"
  [[ -z "${port-}" ]] && port=27017

  return 0
}

# Retrieve script paths
SCRIPT=$(readlink -f $0)
SCRIPT_DIR=$(dirname $SCRIPT)
INSTALL_DIR=$(dirname $(dirname $SCRIPT_DIR))

# Setup cleanup-on-exit function
trap cleanup SIGINT SIGTERM ERR EXIT

# Parse arguments
echo "For help, type $(basename "${BASH_SOURCE[0]}") --help"
echo ""
parse_params "$@"

## (2) Script logic

## default values
root_user="root"
root_password="password"

## docker-compose.yml
cat << EOF > ${SCRIPT_DIR}/docker-compose.yaml
version: '3.7'

services:
  mongodb:
    image: mongo:latest
    container_name: mongodb-${username}
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: $root_user
      MONGO_INITDB_ROOT_PASSWORD: $root_password
    ports:
      - ${port}:27017
    volumes:
      - ./mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro
      - ./mongo-volume-${username}:/data/db
EOF

cat << EOF > ${SCRIPT_DIR}/docker-compose-${username}.yaml
version: '3.7'

services:
  mongodb:
    ports:
      - ${port}:27017
EOF

# create credential files holding the URI
echo "mongodb://${username}:${password}@localhost:${port}/beone_${username}_private?authSource=beone_${username}_private" > ${INSTALL_DIR}/credentials/${hostname}.${username}.beone_${username}_private.uri
echo "mongodb://${username}:${password}@localhost:${port}/beone_${username}_merge?authSource=beone_${username}_merge"     > ${INSTALL_DIR}/credentials/${hostname}.${username}.beone_${username}_merge.uri
echo "URIs for users with readWrite permissions are stored in:"
echo "Private: ${INSTALL_DIR}/credentials/${hostname}.${username}.beone_${username}_private.uri"
echo "Merge:   ${INSTALL_DIR}/credentials/${hostname}.${username}.beone_${username}_merge.uri"
echo

## mongo-init.js for DB initialisation and user creation
cat << EOF > ${SCRIPT_DIR}/mongo-init.js
db.auth('$root_user', '$root_password')

db = db.getSiblingDB('beone_${username}_private')
db.createUser({
  user: '$username',
  pwd: '$password',
  roles: [
    {
      role: 'readWrite',
      db: 'beone_${username}_private'
    }
  ]
});
db.createCollection("dummy_${username}")

db = db.getSiblingDB('beone_${username}_merge')
db.createUser({
  user: '$username',
  pwd: '$password',
  roles: [
    {
      role: 'readWrite',
      db: 'beone_${username}_merge'
    }
  ]
});
db.createCollection("dummy_${username}")
EOF


[[ ! -d "${SCRIPT_DIR}/mongo-volume-${username}" ]] && mkdir "${SCRIPT_DIR}/mongo-volume-${username}"
echo "docker-compose --file ${SCRIPT_DIR}/docker-compose.yaml --file ${SCRIPT_DIR}/docker-compose-${username}.yaml --project-name mongo-${username} up --detach"
cd ${SCRIPT_DIR} && docker-compose --file ${SCRIPT_DIR}/docker-compose.yaml --file ${SCRIPT_DIR}/docker-compose-${username}.yaml --project-name mongo-${username} up --detach

echo "mongodb://${username}:${password}@localhost:${port}/beone_${username}_private" > ${INSTALL_DIR}/credentials/${username}.beone_${username}_private.uri
echo "mongodb://${username}:${password}@localhost:${port}/beone_${username}_merge"   > ${INSTALL_DIR}/credentials/${username}.beone_${username}_merge.uri

cat << EOF

Here are your local MongoDB connection URIs:
"mongodb://${root_user}:${root_password}@localhost:${port}/?authSource=admin"
"mongodb://${username}:${password}@localhost:${port}/beone_${username}_private?authSource=beone_${username}_private"
"mongodb://${username}:${password}@localhost:${port}/beone_${username}_merge?authSource=beone_${username}_merge"

To stop the MongoDB use: "docker-compose -f ${SCRIPT_DIR}/docker-compose.yaml --project-name mongo-${username} down"
To erase the MongoDB use: "sudo rm -R ${SCRIPT_DIR}/mongo-volume-${username} && mkdir ${SCRIPT_DIR}/mongo-volume-${username}"
To erase all traces: "cd ${SCRIPT_DIR} && rm docker-compose-bfr.yaml docker-compose.yaml mongo-init.js"
Remember the BeONE convention: URIs with "localhost" are local, URIs with "127.0.0.1" are remote and require a SSH tunnel

EOF
