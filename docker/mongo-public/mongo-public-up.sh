#!/bin/bash
set -Eeuo pipefail

## (1) Argument logic - template from https://betterdev.blog/minimal-safe-bash-script-template/
cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "Done."
}

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS]

Description:
This script initialises a BeONE MongoDB via Docker-Compose.
It generates/overwrites the files docker-compose.yml and mongo-init.js with an individualized config.
For more information, please visit https://gitlab.com/bfr_bioinformatics/bfr_beone

Options:
  -u, --userlist             A quoted string of comma separated userlists, e.g. '--users "bfr,insa,rivm,ssi"'
  -w, --password             Password string for the user with read/write permission, default: ''
  -p, --port                 Local port for the MongoDB, default: '27017'
  -n, --hostname             Hostname string as prefix for the credentials/.uri filename

EOF
  exit
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  userlist=''
  port=''
  hostname=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -u | --userlist)
      userlist="$2"
      shift 2
      ;;
    -p | --port)
      port="$2"
      shift 2
      ;;
    -n | --hostname)
      hostname="$2"
      shift 2
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
  done

  ## check required params and arguments
  [[ -z "${userlist-}" ]] && die "Please provide a parameter for the argument --userlist. Try $(basename "${BASH_SOURCE[0]}") --help."
  [[ -z "${hostname-}" ]] && die "Please provide a parameter for the argument --hostname. Try $(basename "${BASH_SOURCE[0]}") --help."
  [[ -z "${password-}" ]] && password="mypassword"
  [[ -z "${port-}" ]] && port=27017

  return 0
}

# Retrieve script paths
SCRIPT=$(readlink -f $0)
SCRIPT_DIR=$(dirname $SCRIPT)
INSTALL_DIR=$(dirname $(dirname $SCRIPT_DIR))

# Setup cleanup-on-exit function
trap cleanup SIGINT SIGTERM ERR EXIT

# Parse arguments
echo "For help, type $(basename "${BASH_SOURCE[0]}") --help"
echo ""
parse_params "$@"

## (2) Script logic

## default values
root_user="root"
root_password="password"

## docker-compose.yml
cat << EOF > ${SCRIPT_DIR}/docker-compose.yaml
version: '3.7'

services:
  mongodb:
    image: mongo:latest
    container_name: mongodb-beone
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: $root_user
      MONGO_INITDB_ROOT_PASSWORD: $root_password
    ports:
      - ${port}:27017
    volumes:
      - ./mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro
      - ./mongo-volume-beone:/data/db
EOF

## mongo-init.js for DB initialisation and user creation
cat << EOF > ${SCRIPT_DIR}/mongo-init.js
db.auth('$root_user', '$root_password')
EOF

## read userlist
IFS=',' read -r -a userarray <<< "$userlist"
#IFS=$' \t\n'  # restore

for ((i=0; i<${#userarray[@]}; ++i)); do

  ## create public with readWrite owner and users with read-only
  write_user=${userarray[$i]}
  echo "##############################"
  echo "Database: beone_${write_user}_public"
  echo "Write Access: $write_user"
  echo "mongodb://${write_user}:${password}@127.0.0.1:${port}/beone_${write_user}_public?authSource=beone_${write_user}_public" > ${INSTALL_DIR}/credentials/${hostname}.${write_user}.beone_${write_user}_public.uri

  cat << EOF >> ${SCRIPT_DIR}/mongo-init.js
db = db.getSiblingDB('beone_${write_user}_public')
db.createUser({
  user: '$write_user',
  pwd: '$password',
  roles: [
    {
      role: 'readWrite',
      db: 'beone_${write_user}_public'
    }
  ]
});
db.createCollection("dummy_${write_user}")
EOF

  read_user_temp=("${userarray[@]}")
  unset 'read_user_temp[$i]'
  read_user_list=("${read_user_temp[@]}")
  echo "Read Access: ${read_user_list[@]}"
  for read_user in ${read_user_list[@]}; do
    echo "mongodb://${read_user}:i_am_${read_user}@127.0.0.1:${port}/beone_${write_user}_public?authSource=beone_${write_user}_public" > ${INSTALL_DIR}/credentials/${hostname}.${read_user}.beone_${write_user}_public.uri
    cat << EOF >> ${SCRIPT_DIR}/mongo-init.js
db.createUser({
  user: '${read_user}',
  pwd: 'i_am_${read_user}',
  roles: [
    {
      role: 'read',
      db: 'beone_${write_user}_public'
    }
  ]
});
EOF
  done
done

echo
echo "docker-compose -f ${SCRIPT_DIR}/docker-compose.yaml up --detach"
cd ${SCRIPT_DIR} && docker-compose up -d

cat << EOF

Here are your MongoDB connection URIs:
"mongodb://${root_user}:${root_password}@127.0.0.1:${port}/?authSource=admin"
"mongodb://[${userlist}]:${password}@127.0.0.1:${port}/beone_[${userlist}]_public?authSource=beone_[${userlist}]_public"

To stop the MongoDB use: "docker-compose -f ${SCRIPT_DIR}/docker-compose.yaml down"
To erase the MongoDB use: "sudo rm -R ${SCRIPT_DIR}/mongo-volume-beone && mkdir ${SCRIPT_DIR}/mongo-volume-beone"
To erase all traces: "cd ${SCRIPT_DIR} && rm docker-compose-bfr.yaml docker-compose.yaml mongo-init.js"
Remember the BeONE convention: URIs with "localhost" are local, URIs with "127.0.0.1" are remote and require a SSH tunnel

EOF
