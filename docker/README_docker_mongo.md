# BfR-BeONE

**Prerequisite**: 

### Installation of repository and docker-compose

* [Docker](https://docs.docker.com/get-docker/), 
* [Docker-Compose](https://docs.docker.com/compose/install/) and 
* [MongoSh](https://www.mongodb.com/docs/mongodb-shell/install/#install-mongosh)
* (mongodb-clients works, but is deprecated)

The installation requires _Admin_ rights on the host system. 
Hence, above software is preinstalled for the workshop/simulation exercises:

```shell
ssh looneyVenter.deNBI.ubuntu
sudo apt-get update && sudo apt-get install docker-compose mongodb-clients
```

## Installation of MongoDB via Docker

Users are members of the *docker* group and can pull the latest MongoDB image by executing:

```shell
docker pull mongo:latest
```

Verify pulled images by executing `docker images`.

```text
REPOSITORY   TAG       IMAGE ID       CREATED        SIZE
mongo        latest    0850fead9327   4 days ago     700MB
```

## Initialize MongoDB via mongo-private-up.sh

```shell
bash ~/git/bfr_beone/docker/mongo-private/mongo-private-up.sh --help
```

```text
For help, type mongo-private-up.sh --help

Usage: mongo-private-up.sh [OPTIONS]

Description:
This script initialises a BeONE MongoDB via Docker-Compose.
It generates/overwrites the files docker-compose.yml and mongo-init.js with an individualized config.
For more information, please visit https://gitlab.com/bfr_bioinformatics/bfr_beone

Options:
  -u, --username             Username string, e.g. your institute 'bfr'
  -w, --password             Password string, default: ''
  -p, --port                 Local port for the MongoDB, default: '27017'
  -n, --hostname             Hostname string as prefix for the credentials/.uri filename

Done.
```

## Simulation Setup

This section summarizes the DB setup described in detail in the [Simulation Playbooks](https://gitlab.com/bfr_bioinformatics/bfr_beone/-/wikis/home).

Login to either `looneyVenter.deNBI.<institute>` or `tipsyDawkins.deNBI.<institute>` or `timelessEuler.deNBI.beone`.

### The simulation scenario will be:

| Institute | Type           | Server Name   | Server IP     | Server Port | MongoDB Port |
|-----------|----------------|---------------|---------------|-------------|--------------|
| SSI       | private, merge | looneyVenter  | 134.176.27.78 | 30282       | 29019        |
| BfR       | private, merge | tipsyDawkins  | 134.176.27.78 | 30461       | 29019        |
| all       | public         | timelessEuler | 134.176.27.78 | 30472       | 29017        |

### Private DB Setup - Institute A "SSI"

```shell
ssh looneyVenter.deNBI.ssi
mkdir ~/git && cd ~/git && git clone https://gitlab.com/bfr_bioinformatics/bfr_beone.git
cd ~/git/bfr_beone/docker/mongo-private && bash mongo-private-up.sh --username ssi --port 29019 --hostname looneyVenter
```

```text
For help, type mongo-private-up.sh --help

URIs for users with readWrite permissions are stored in:
Private: /home/ssi/git/bfr_beone/credentials/looneyVenter.ssi.beone_ssi_private.uri
Merge:   /home/ssi/git/bfr_beone/credentials/looneyVenter.ssi.beone_ssi_merge.uri

docker-compose --file /home/ssi/git/bfr_beone/docker/mongo-private/docker-compose.yaml --file /home/ssi/git/bfr_beone/docker/mongo-private/docker-compose-ssi.yaml --project-name mongo-ssi up --detach
Creating network "mongo-ssi_default" with the default driver
Creating mongodb-ssi ... done

Here are your local MongoDB connection URIs:
"mongodb://root:password@localhost:29019/?authSource=admin"
"mongodb://ssi:mypassword@localhost:29019/beone_ssi_private?authSource=beone_ssi_private"
"mongodb://ssi:mypassword@localhost:29019/beone_ssi_merge?authSource=beone_ssi_merge"

To stop the MongoDB use: "docker-compose -f /home/ssi/git/bfr_beone/docker/mongo-private/docker-compose.yaml --project-name mongo-ssi down"
To erase the MongoDB use: "sudo rm -R /home/ssi/git/bfr_beone/docker/mongo-private/mongo-volume-ssi && mkdir /home/ssi/git/bfr_beone/docker/mongo-private/mongo-volume-ssi"
To erase all traces: "cd /home/ssi/git/bfr_beone/docker/mongo-private && rm docker-compose-bfr.yaml docker-compose.yaml mongo-init.js"
Remember the BeONE convention: URIs with "localhost" are local, URIs with "127.0.0.1" are remote and require a SSH tunnel

Done.
```

### Private DB Setup - Institute B "BfR"

This mongoDB container hosts both databases - `private` and `merge`.

```shell
ssh tipsyDawkins.deNBI.bfr
mkdir ~/git && cd ~/git && git clone https://gitlab.com/bfr_bioinformatics/bfr_beone.git
cd ~/git/bfr_beone/docker/mongo-private && bash mongo-private-up.sh --username bfr --port 29019 --hostname tipsyDawkins
```

```text
For help, type mongo-private-up.sh --help

URIs for users with readWrite permissions are stored in:
Private: /home/bfr/git/bfr_beone/credentials/tipsyDawkins.bfr.beone_bfr_private.uri
Merge:   /home/bfr/git/bfr_beone/credentials/tipsyDawkins.bfr.beone_bfr_merge.uri

docker-compose --file /home/bfr/git/bfr_beone/docker/mongo-private/docker-compose.yaml --file /home/bfr/git/bfr_beone/docker/mongo-private/docker-compose-bfr.yaml --project-name mongo-bfr up --detach
Creating network "mongo-bfr_default" with the default driver
Creating mongodb-bfr ... done

Here are your local MongoDB connection URIs:
"mongodb://root:password@localhost:29019/?authSource=admin"
"mongodb://bfr:mypassword@localhost:29019/beone_bfr_private?authSource=beone_bfr_private"
"mongodb://bfr:mypassword@localhost:29019/beone_bfr_merge?authSource=beone_bfr_merge"

To stop the MongoDB use: "docker-compose -f /home/bfr/git/bfr_beone/docker/mongo-private/docker-compose.yaml --project-name mongo-bfr down"
To erase the MongoDB use: "sudo rm -R /home/bfr/git/bfr_beone/docker/mongo-private/mongo-volume-bfr && mkdir /home/bfr/git/bfr_beone/docker/mongo-private/mongo-volume-bfr"
To erase all traces: "cd /home/bfr/git/bfr_beone/docker/mongo-private && rm docker-compose-bfr.yaml docker-compose.yaml mongo-init.js"
Remember the BeONE convention: URIs with "localhost" are local, URIs with "127.0.0.1" are remote and require a SSH tunnel

Done.
```

### Public DB Setup - centralized for convienience

This mongoDB container hosts both `public` databases - `bfr` and `ssi`.

```shell
ssh timelessEuler.deNBI.beone
mkdir ~/git && cd ~/git && git clone https://gitlab.com/bfr_bioinformatics/bfr_beone.git
cd ~/git/bfr_beone/docker/mongo-public && bash mongo-public-up.sh --userlist "bfr,ssi" --port 27017 --hostname timelessEuler
```

```text
For help, type mongo-public-up.sh --help

##############################
Database: beone_bfr_public
Write Access: bfr
Read Access: ssi
##############################
Database: beone_ssi_public
Write Access: ssi
Read Access: bfr

docker-compose -f /home/beone/git/bfr_beone/docker/mongo-public/docker-compose.yaml up --detach
Creating network "mongo-public_default" with the default driver
Creating mongodb-beone ... done

Here are your MongoDB connection URIs:
"mongodb://root:password@127.0.0.1:27017/?authSource=admin"
"mongodb://[bfr,ssi]:mypassword@127.0.0.1:27017/beone_[bfr,ssi]_public?authSource=beone_[bfr,ssi]_public"

To stop the MongoDB use: "docker-compose -f /home/beone/git/bfr_beone/docker/mongo-public/docker-compose.yaml down"
To erase the MongoDB use: "sudo rm -R /home/beone/git/bfr_beone/docker/mongo-public/mongo-volume-beone && mkdir /home/beone/git/bfr_beone/docker/mongo-public/mongo-volume-beone"
To erase all traces: "cd /home/beone/git/bfr_beone/docker/mongo-public && rm docker-compose-bfr.yaml docker-compose.yaml mongo-init.js"
Remember the BeONE convention: URIs with "localhost" are local, URIs with "127.0.0.1" are remote and require a SSH tunnel

Done.
```

# Notes

```shell
export ATLAS_ROOT_URI=$(cat ~/.mongoatlas-credentials)
mongo $ATLAS_ROOT_URI
```
