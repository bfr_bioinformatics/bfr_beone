# Preparation of BeONE result dataset for MongoDB import and subsequent DB query

## Backup of combined metadata - Partner and SRA data

```shell
cd /home/Brendeba/data/git/beone-metadata-parsing/form/_confidential
sha256sum BeONE_metadata.jsonl > BeONE_metadata.jsonl.sha256
sha256sum -c BeONE_metadata.jsonl.sha256
pigz --best --keep --verbose BeONE_metadata.jsonl
sha256sum BeONE_metadata.jsonl.gz > BeONE_metadata.jsonl.gz.sha256
sha256sum -c BeONE_metadata.jsonl.gz.sha256
rsync -uvP --checksum BeONE_metadata.jsonl.* /cephfs/abteilung4/brendebach/data/beone/results/archives/
```

## JSON merge of AQUAMIS, chewieSnake and metadata results

The arguments `--username bfr --hostname looneyVenter` are dummy values to keep the Snakefile running with `--until`.  

```shell
cd /home/Brendeba/data/git/bfr_beone/beone_dataset
/bin/bash /home/Brendeba/data/git/bfr_beone/beone_wrapper.sh --username bfr --hostname looneyVenter
```

This wraps the core command:

```shell
snakemake  \
  --snakefile /home/Brendeba/data/git/bfr_beone/Snakefile  \
  --jobs 20  \
  --config  \
    workdir=/home/Brendeba/data/git/bfr_beone/beone_dataset \
    username=bfr \
    format=json_line \
    urifile_private=looneyVenter.bfr.beone_bfr_private.uri \
    urifile_public=timelessEuler.bfr.beone_bfr_public.yaml \
    urifile_merge=looneyVenter.bfr.beone_bfr_merge.uri \
  --until filter_json \
  --dryrun --quiet
```

## Casting one-sample-per-file data into JSON Line format

```shell
find 5_merge/ -type f -name "*.merge.json" | sort | xargs -I {} jq -c '.' {} > BeONE_dataset_all_merge.jsonl
find 6_filtered/ -type f -name "*.beone.json" | sort | xargs -I {} jq -c '.' {} > BeONE_dataset_all_beone.jsonl
```

```text
# 6.1G Nov 16 19:15 BeONE_dataset_all_merge.jsonl
# 1.4G Nov 16 19:07 BeONE_dataset_all_beone.jsonl
```

## QC of BeONE datamodel filtering

```shell
find 6_filtered/ -type f -name "*validationErrors*" -print -exec cat {} \;
```

## Backup of Merged data

### Full dataset

```shell
sha256sum BeONE_dataset_all_merge.jsonl > BeONE_dataset_all_merge.jsonl.sha256
sha256sum -c BeONE_dataset_all_merge.jsonl.sha256
time pigz --best --keep --verbose BeONE_dataset_all_merge.jsonl
```

runtime for 6.1G:

```text
real    0m24.423s - yeah!
user    12m16.385s
sys     27m47.868s
```

```shell
sha256sum BeONE_dataset_all_merge.jsonl.gz > BeONE_dataset_all_merge.jsonl.gz.sha256
sha256sum -c BeONE_dataset_all_merge.jsonl.gz.sha256
```

### Filtered dataset

```shell
sha256sum BeONE_dataset_all_beone.jsonl > BeONE_dataset_all_beone.jsonl.sha256
sha256sum -c BeONE_dataset_all_beone.jsonl.sha256
time pigz --best --keep --verbose BeONE_dataset_all_beone.jsonl
```

runtime for 1.4G:

```text
real    0m5.297s
user    2m43.715s
sys     4m39.140s
```

```shell
sha256sum BeONE_dataset_all_beone.jsonl.gz > BeONE_dataset_all_beone.jsonl.gz.sha256
sha256sum -c BeONE_dataset_all_beone.jsonl.gz.sha256
```

### Transfer of archives

```shell
rsync -uvP --checksum BeONE_dataset_all*.jsonl.gz*    /cephfs/abteilung4/brendebach/data/beone/results/archives/
rsync -uvP --checksum BeONE_dataset_all*.jsonl.sha256 /cephfs/abteilung4/brendebach/data/beone/results/archives/
```

```text
3.4G Nov 17 11:06 BeONE_dataset_all_beone.json.tar
273M Nov 17 11:06 BeONE_dataset_all_beone.json.tar.gz
254M Nov 16 19:23 BeONE_dataset_all_beone.jsonl.gz

 19G Nov 17 11:08 BeONE_dataset_all_merge.json.tar
1.1G Nov 17 11:08 BeONE_dataset_all_merge.json.tar.gz
974M Nov 16 19:23 BeONE_dataset_all_merge.jsonl.gz
```

## 2022-11-16 suggested edits

```shell
COUNTER=1
for json in $(find /home/Brendeba/data/git/bfr_beone/beone_dataset/5_merge/ -type f -name "*.merge.json"); do
  echo $COUNTER $json
  sed -i -E 's/"comparison_allele_database": "NULL"/"comparison_allele_database": ""/g' $json  # obsolete in future chewieSnake results
#  sed -i -E 's/"qc_cgmlst": \{\}/"qc_cgmlst": ""/g' $json  # obsolete: causing code in json_merger.py was corrected
  perl -0777 -pi -e 's/,\s*\{\}(?=\s*\])//igs' $json
#  perl -0777 -pi -e 's/,\s*"bakcharak": \{\}//igs' $json  # obsolete: causing code in json_merger.py was corrected
#  perl -0777 -pi -e 's/,\s*"chewiesnake": \{\}//igs' $json  # obsolete: causing code in json_merger.py was corrected
  perl -0777 -pi -e 's/,\s*"pipelines": \{\}//igs' $json  # caused by json_filter.py when no module results are left
  COUNTER=$((COUNTER + 1))
done
```

## Query the JSON Lines file with JQ

```shell
jq -r '.sample.summary.sample+","+.sample.qc_assessment.qc_cgmlst' BeONE_dataset_all_beone_extract.jsonl | mlr --icsv --hi --opprint label sample,allele_qc

## performance
time jq -r '.sample.summary.sample+","+.sample.qc_assessment.qc_cgmlst' BeONE_dataset_all_beone.jsonl > /dev/null
# 1.4G
# real    0m30.265s
# user    0m29.799s
# sys     0m0.452s

time jq -r '.sample.summary.sample+","+.sample.qc_assessment.qc_cgmlst' BeONE_dataset_all_merge.jsonl > /dev/null
## 6.1G
# real    2m30.602s
# user    2m28.377s
# sys     0m2.140s

## parsing size matters, almost linear with factor 1.15
```

```shell
## miller non-transposed
jq -r 'select(.sample.summary.sample == "Cj_0002") | .pipelines.chewiesnake.allele_profile' BeONE_dataset_all_beone_extract.jsonl | mlr --ijson --opprint head

## jq, transposed
jq -r '[.pipelines.chewiesnake.allele_profile[]] | map(.locus), map(.allele_crc32) | @tsv' 6_filtered/Cj_0002.beone.json | sed -E '1s/^/FILE\t/; 2,$s/^/'"Cj_0002"'\t/' | lesss
```

**BEWARE:** this code does not perform a join based on loci names, but does a sufficient check after the merge. 

```shell
## filter for single schema && passing QC
cat <<'EOF' > query_jsonl_cgMLST.sh
#!/bin/bash
jsonlines="BeONE_dataset_all_beone_extract.jsonl"
jsonlines="BeONE_dataset_all_beone.jsonl"
schema="pubmlst_campylobacter_cgmlst_newchewie"
schema="enterobase_senterica_cgmlst"
jq -r --arg schema "$schema" 'select((.pipelines.chewiesnake.allele_stats.allele_qc == "PASS") and (.pipelines.chewiesnake.run_metadata.database_information.scheme_name == $schema)) | .pipelines.chewiesnake.run_metadata.sample_description.sample' ${jsonlines} | sed -E 's/^/FILE\n/g' > jq_allele_profiles_names.txt
jq -r --arg schema "$schema" 'select((.pipelines.chewiesnake.allele_stats.allele_qc == "PASS") and (.pipelines.chewiesnake.run_metadata.database_information.scheme_name == $schema)) | ([.pipelines.chewiesnake.allele_profile[]] | map(.locus), map(.allele_crc32)) | @tsv' ${jsonlines} > jq_allele_profiles_nonames.tsv
paste jq_allele_profiles_names.txt jq_allele_profiles_nonames.tsv > jq_allele_profiles_raw.tsv
MERGE_QC=0
[[ $(sed -n 'p;n' jq_allele_profiles_raw.tsv | sort -u | wc -l) -eq 1 ]] && MERGE_QC=1
if [[ MERGE_QC -eq 1 ]]; then
  echo "PASS: all samples have the same loci"
  sed -n 'n;p' jq_allele_profiles_raw.tsv | sed "1s/^/$(head -n1 jq_allele_profiles_raw.tsv)\n/" > jq_allele_profiles.tsv
else
  echo "FAIL: the samples do not share the same loci"
  exit 1
fi
EOF

time /bin/bash query_jsonl_cgMLST.sh

## BeONE_dataset_all_beone_extract.jsonl ; input=4 , output=4 for Campy
# PASS: all samples have the same loci
# real    0m0.154s
# user    0m0.144s
# sys     0m0.018s

## BeONE_dataset_all_beone.jsonl ; input=14172 , output=3499 for Campy
# PASS: all samples have the same loci
# real    1m5.123s
# user    1m3.377s 
# sys     0m1.814s

## BeONE_dataset_all_beone.jsonl ; input=14172 , output=3499 for Salmonella
# PASS: all samples have the same loci
# real    1m13.697s
# user    1m11.695s
# sys     0m2.123s
```

jq -r 'select((.pipelines.chewiesnake != null) && (.pipelines.chewiesnake != null))

## Query the MongoDB with PyMongo

### Import JSON Lines into MongoDB

via `scripts/query_non-bifrost_beone-db_cgMLST.py`

Use `Singularity` image on BfR HPC03.

* URI: _mongodb://127.0.0.1:26016_
* DB: _bfr_bulk_
* Collection: _beone_

|name    | sizeOnDisk  |
|--------|-------------|
|bfr_bulk| 516.415.488 |

### Query MongoDB and derive Allele Profiles table for all samples of a schema

via `scripts/query_non-bifrost_beone-db_cgMLST.py`

```shell
cd scripts
time python query_non-bifrost_beone-db_cgMLST.py 2>&1 | tee -a query_non-bifrost_beone-db_cgMLST.py.log
# [2022-11-18 23:25:44.866335] Querying Database.
# [2022-11-18 23:26:16.774241] Restructuring metadata from DB query.
# [2022-11-18 23:26:16.782322] Restructuring allele profiles from DB query.
# [2022-11-18 23:26:22.871112] MapReduce dataframe.
# [2022-11-18 23:29:57.808598] Exporting metadata table.
# [2022-11-18 23:29:57.818564] Exporting allele table.
# [2022-11-18 23:30:03.826580] Done.
# real    4m25.979s
# user    4m20.998s
# sys     0m17.934s

cd ../beone_dataset && ls -al BeONE_allele_profile_*
# 144K Nov 18 15:49 BeONE_allele_profile_enterobase_senterica_cgmlst_metadata.tsv
# 105M Nov 18 15:49 BeONE_allele_profile_enterobase_senterica_cgmlst.tsv

wc -l BeONE_allele_profile_enterobase_senterica_cgmlst*
#      2960 BeONE_allele_profile_enterobase_senterica_cgmlst_metadata.tsv
#      3001 BeONE_allele_profile_enterobase_senterica_cgmlst.tsv

diff -ys --suppress-common-lines \
  <(cut -d $'\t' -f 3 BeONE_allele_profile_enterobase_senterica_cgmlst_metadata.tsv | sort -h)  \
  <(head -n1 BeONE_allele_profile_enterobase_senterica_cgmlst.tsv | sed 's/\t/\n/g' | (read -r; printf "%s\n" "$REPLY"; sort -h))
#                                                               >
# name                                                          | locus

tail -n4 BeONE_allele_profile_enterobase_senterica_cgmlst_metadata.tsv
# 2955    63778ae02e5bd8d2d681e8b1        Se_1783 1041544056
# 2956    63778ae02e5bd8d2d681e8b2        Se_1784 1709526287
# 2957    63778ae02e5bd8d2d681e8b3        Se_1785 450044781
# 2958    63778ae02e5bd8d2d681e8b4        Se_1786 977929341

cellA1="FILE"
sed -i -E '1s/^/'$cellA1'/' BeONE_allele_profile_enterobase_senterica_cgmlst.tsv
sed -i -E 's/LOTSC//' jq_allele_profiles.tsv

diff -ys --suppress-common-lines BeONE_allele_profile_enterobase_senterica_cgmlst.tsv jq_allele_profiles.tsv
# Files /dev/fd/63 and /dev/fd/62 are identical
```

**Weird observation:** `grep -c LOTSC /cephfs/abteilung4/brendebach/data/beone/results/chewieSnake/SE_DATASET/SE_DATASET_20220927-194511/cgmlst/Se_0813/*`

Room for improvement in query_non-bifrost_beone-db_cgMLST.py shift data restructuring from python to mongodb.

**From here:** concat chewieSnake scripts for distance matrix calculation and grapetree.

******

## Prepare Simulation dataset and copy to deNBI

```bash
grep -P '{"Microorganism":"Salmonella enterica"' /home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_metadata.jsonl > /home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_metadata_SE.jsonl
grep -P "^Se_" /home/Brendeba/data/git/bfr_beone/beone_dataset/samples.tsv | head -n200 > /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi.tsv
grep -P "^Se_" /home/Brendeba/data/git/bfr_beone/beone_dataset/samples.tsv | tail -n200 > /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr.tsv
awk 'BEGIN{FS="\t"}{print $3}' /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr.tsv | sort -u | sed 's@/home/Brendeba/cephfs_home/data/beone/results/AQUAMIS/WP3/Se/@@g' > /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr_json-aquamis.txt
awk 'BEGIN{FS="\t"}{print $3}' /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi.tsv | sort -u | sed 's@/home/Brendeba/cephfs_home/data/beone/results/AQUAMIS/WP3/Se/@@g' > /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi_json-aquamis.txt
awk 'BEGIN{FS="\t"}{print $5}' /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr.tsv | sort -u | sed 's@/home/Brendeba/cephfs_home/data/beone/results/chewieSnake/SE_DATASET/SE_DATASET_20220927-194511/cgmlst/json/@@g' > /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr_json-chewiesnake.txt
awk 'BEGIN{FS="\t"}{print $5}' /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi.tsv | sort -u | sed 's@/home/Brendeba/cephfs_home/data/beone/results/chewieSnake/SE_DATASET/SE_DATASET_20220927-194511/cgmlst/json/@@g' > /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi_json-chewiesnake.txt
rsync -aPn --files-from=/home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr_json-aquamis.txt /home/Brendeba/cephfs_home/data/beone/results/AQUAMIS/WP3/Se/ tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/bfr/2_aquamis/
rsync -aPn --files-from=/home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi_json-aquamis.txt /home/Brendeba/cephfs_home/data/beone/results/AQUAMIS/WP3/Se/ tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/ssi/2_aquamis/
rsync -aPn --files-from=/home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr_json-chewiesnake.txt /home/Brendeba/cephfs_home/data/beone/results/chewieSnake/SE_DATASET/SE_DATASET_20220927-194511/cgmlst/json/ tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/bfr/4_chewiesnake/
rsync -aPn --files-from=/home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi_json-chewiesnake.txt /home/Brendeba/cephfs_home/data/beone/results/chewieSnake/SE_DATASET/SE_DATASET_20220927-194511/cgmlst/json/ tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/ssi/4_chewiesnake/
rsync -aPn /home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_metadata_SE.jsonl tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/bfr/1_metadata/
rsync -aPn /home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_metadata_SE.jsonl tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/ssi/1_metadata/
rsync -aPn /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_bfr.tsv tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/bfr/
rsync -aPn /home/Brendeba/data/git/bfr_beone/beone_dataset/samples_ssi.tsv tipsyDawkins.deNBI.ubuntu:~/datastore/data/beone/ssi/
ubuntu@tipsydawkins-69f9a:~/datastore/data/beone/bfr/2_aquamis$ find $(pwd) -type f -name "*.aquamis.json" -exec mv {} $(pwd)/ \;
ubuntu@tipsydawkins-69f9a:~/datastore/data/beone/ssi/2_aquamis$ find $(pwd) -type f -name "*.aquamis.json" -exec mv {} $(pwd)/ \;
```
