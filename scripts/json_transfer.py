#!/usr/bin/env python3

'''
BeONE JSON transfer from a source DB to target DB, SSH-enabled.
Make sure mongod is up and running and environment variable BIFROST_DB_KEY is set to a TEST database.
$ sudo systemctl restart mongod
'''

# %% Packages #################################################################
import json
import os
import sys
import logging
import traceback
import argparse
import glob
from datetime import datetime
from functools import reduce
from pathlib import Path
import pprint

import bson
from bson import json_util
import pymongo
import pandas
import yaml
from paramiko import SSHConfig
import beoneapi
from helper_functions import *
import json_xcrypt


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Define Source of Arguments ###############################################

def parse_arguments():
    parser = argparse.ArgumentParser()

    # required params
    parser.add_argument('--db_key_source', '-s', help = 'JSON String of the Source MongoDB database to copy FROM: {"uristring": key}',
                        required = True)
    parser.add_argument('--db_key_target', '-t', help = 'JSON String of the Target MongoDB database to copy TO: {"uristring": key}',
                        required = True)
    parser.add_argument('--username', '-u', help = 'String of the cryptographic mode, either (A) "encrypt" or (B) "decrypt"',
                        required = False)
    # optional params
    parser.add_argument('--mode', '-m', help = 'String of the cryptographic mode, either (A) "encrypt" or (B) "decrypt"',
                        required = False)
    parser.add_argument('--keychain', '-k', help = 'Path to the BeONE keychain of AES keys; will be generated if none is provided for encryption',
                        type = os.path.abspath, required = False)
    # database
    parser.add_argument('--drop_collections', '-d', help = 'String of a MongoDB connection, i.e. either "source" or "target". Drop all BeONE collections therein. USE WITH CAUTION!',
                        required = False)
    # log
    parser.add_argument('--log', '-l', help = 'Path for the logfile',
                        type = os.path.abspath, required = True)

    return parser.parse_args()


# Create Arguments for Debugging
def create_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    debug_workdir = os.path.join(repo_dir, "test_data_workshop")  # "test_data" "test_data_beone" "test_data_workshop"
    debug_username = 'bfr'  # institute name
    debug_mode = 'encrypt'  # 'encrypt' or 'decrypt'
    # debug_mode = 'decrypt'  # 'encrypt' or 'decrypt'
    debug_drop_collections = ''  # '' or "source" or "target"

    args = argparse.Namespace(
        db_key_source = '{"uristring": "mongodb://bfr:mypassword@localhost:28018/beone_bfr_private?authSource=beone_bfr_private"}',
        db_key_target = '{"uristring": "mongodb://bfr:mypassword@localhost:26016/beone_bfr_public?authSource=beone_bfr_public"}',
        # db_key_source = '{"host": "134.176.27.78", "port": 30216, "user": "beone", "key": "/home/brendy/.ssh/beone/id_beone.pem", "key_password": "GQ0fPM0a3lpSsj6L", "uri": "mongodb://bfr:mypassword@127.0.0.1:27017/beone_bfr_public?authSource=beone_bfr_public"}',
        # db_key_source = '{"host": "134.176.27.78", "port": 30216, "user": "beone", "key": "/home/brendy/.ssh/beone/id_beone.pem", "key_password": "GQ0fPM0a3lpSsj6L", "uri": "mongodb://bfr:i_am_bfr@127.0.0.1:27017/beone_ssi_public?authSource=beone_ssi_public"}',
        # db_key_source = '{"uristring": "mongodb://bfr:mypassword@localhost:26016/beone_bfr_public?authSource=beone_bfr_public"}',
        # db_key_target = '{"uristring": "mongodb://bfr:mypassword@localhost:28018/beone_bfr_merge?authSource=beone_bfr_merge"}',
        username = debug_username,
        mode = debug_mode,
        keychain = os.path.join(debug_workdir, '9_crypto', 'keychain.json'),
        drop_collections = debug_drop_collections,
        json_out = os.path.join(debug_workdir, '10_transfer', 'placeholder.transfer.encrypted.json'),
        log = os.path.join(debug_workdir, '0_logs', 'transfer_encryptJson_debug.log')
    )
    return args


# Translate ArgParse to Snakemake Object
def snakemake_to_arguments(snakemake):
    args = argparse.Namespace(
        db_key_source = snakemake.params['db_key_source'],
        db_key_target = snakemake.params['db_key_target'],
        username = snakemake.params['username'],
        mode = snakemake.params['mode'],
        keychain = snakemake.params['keychain'],
        drop_collections = snakemake.params['drop_collections'],
        json_out = snakemake.output['json_out'],
        log = snakemake.log['log'],
    )
    return args


# Define Source of Arguments
def python_or_snakemake(debug: bool):
    global args
    try:
        snakemake.params[0]
    except NameError:
        if debug:
            args = create_arguments()
        else:
            args = parse_arguments()
    else:
        args = snakemake_to_arguments(snakemake = snakemake)


# %% Helper Functions #########################################################

def has_a_database_connection(connection_name: str = 'default') -> bool:
    """Checks if you have a database connection

    Returns:
        bool: True - has a connection, False - No connection established
    """
    try:
        connection = beoneapi.get_connection(connection_name = connection_name)
        logging.info(f'Connection to MongoDB "{connection_name}" established. DB version is {connection.server_info().get("version", "")}.')
        return True
    except:
        message = """
        For SSH tunneling, conversion of the private key into a format that is recognized by paramiko might help:
        `cp id.pem id.pem.bak && ssh-keygen -p -m pem -f id.pem` 
        """
        print(message)
        print(traceback.format_exc())
        return False


def test_database(connection_name: str = 'default'):
    print("##############################")
    if has_a_database_connection(connection_name = connection_name):
        connection = beoneapi.get_connection(connection_name = connection_name)
        db = connection.get_database()
        print("Connection:  " + connection_name)
        print("Collections: " + json.dumps(db.list_collection_names()))
    else:
        logging.error(f'Cannot connect to "{connection_name}" database.')


def clear_collections(uri):
    """Perform db.collection.drop() on all BeONE collections

    :param uri: URI of the MongoDB to clear from the BeONE collections (run_components, runs, sample_components, samples, components)

    Returns:
        System Exit
    """
    logging.info(f"Clearing all BeOne collections from {uri} and exiting")
    print(f"Clearing all BeOne collections from '{os.path.basename(uri).split('?')[0]}' and exiting")
    client = pymongo.MongoClient(uri)
    db = client.get_database()
    db.drop_collection("run_components")
    db.drop_collection("runs")
    db.drop_collection("sample_components")
    db.drop_collection("samples")
    db.drop_collection("components")
    print("Collections cleared")
    sys.exit()


def get_uri(credentials, host, db_substring):
    _uri = [value for key, value in credentials.items() if db_substring in key and host in key][0]
    return _uri


def read_openssh_config(host, config_file = None):
    """Parses user's OpenSSH config for per hostname configuration for
    hostname, user, port and identity file
    :param host: Hostname to lookup in config
    """
    _ssh_config_file = config_file if config_file else os.path.join(os.path.expanduser('~'), '.ssh', 'config')
    # Load ~/.ssh/config if it exists to pick up username
    # and host address if set
    if not os.path.isfile(_ssh_config_file):
        return
    ssh_config = SSHConfig()
    ssh_config.parse(open(_ssh_config_file))
    host_config = ssh_config.lookup(host)
    credentials = {'host': host_config['hostname'] if 'hostname' in host_config else host,
                   'port': int(host_config['port']) if 'port' in host_config else 22,
                   'user': host_config['user'] if 'user' in host_config else None,
                   'key' : host_config['identityfile'][0] if 'identityfile' in host_config else None}
    return credentials


# %% Database Functions #######################################################

def retrieve_cgmlst_profiles(connection_name: str,
                             schema_name: str = 'enterobase_senterica_cgmlst',
                             method: str = 'aggregation',
                             verbose_switch: bool = False):
    '''
    This section retrieves all samples from a database and retrieves a their allele
        profiles by searching for their _id's and retricting output by a projection.
    One may replace the search by a pymongo aggregation function with additional filters,
        e.g. same cgMLST schema, cgmlst_qc passed, etc. and reshape the aggregation output
        instead of the dictionary mapreduce.
    '''

    # TODO: Maybe add failsafe for samples without allele_profile

    # By find(Projection)
    if method == "projection":
        all_samples = pandas.DataFrame.from_dict([dict(item, **{'_id': str(item['_id'])}) for item in beoneapi.get_all_samples(connection_name = connection_name)]).assign(source = connection_name).set_index(keys = '_id')
        allele_projection = {'_id': False, 'display_name': True, 'categories': {'cgmlst': {'report': {'chewiesnake': {'allele_profile': True}}}}}
        allele_profiles_projection = beoneapi.get_samples(sample_id_list = [bson.ObjectId(oid = item) for item in all_samples.index.to_list()], projection = allele_projection, connection_name = connection_name)
        allele_profiles_projection_mapreduce = [pandas.DataFrame(item['categories']['cgmlst']['report']['chewiesnake']['allele_profile']).rename(columns = {'allele_crc32': item['display_name']}) for item in allele_profiles_projection]
        allele_profiles_projection_df = reduce(lambda x, y: pd.merge(x, y, on = 'locus', how = 'outer'), allele_profiles_projection_mapreduce)
        pprint.pprint(allele_profiles_projection_df) if verbose_switch else None
        return allele_profiles_projection_df

    # By custom aggregation function with filter for cgMLST==PASS and schema_name; sample_id_list filter is optional
    if method == "aggregation":
        allele_profiles_aggregation = beoneapi.get_allele_profiles(schema_name = schema_name, connection_name = connection_name)  # sample_id_list = [bson.ObjectId(oid = item) for item in all_samples_local.index.to_list()]
        allele_profiles_aggregation_mapreduce = [pandas.DataFrame(item['allele_profile']).rename(columns = {'allele_crc32': item['display_name']}) for item in allele_profiles_aggregation]
        allele_profiles_aggregation_df = reduce(lambda x, y: pd.merge(x, y, on = 'locus', how = 'outer'), allele_profiles_aggregation_mapreduce)
        allele_profiles_aggregation_meta = pandas.DataFrame([{k: v for k, v in d.items() if k != 'allele_profile'} for d in allele_profiles_aggregation])
        pprint.pprint(allele_profiles_aggregation_df) if verbose_switch else None
        pprint.pprint(allele_profiles_aggregation_meta) if verbose_switch else None
        return allele_profiles_aggregation_df, allele_profiles_aggregation_meta


# %% Main Function ############################################################

def main():
    verbose_switch = False
    debug_switch = False

    global args

    # Parse Arguments
    python_or_snakemake(debug = debug_switch)

    # Configure Logging
    logging.basicConfig(filename = args.log,
                        encoding = 'utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')
    logging.info("api_testing.py is runnning in DEBUG mode") if debug_switch else None

    # Add URI to API
    uri_source_input = json.loads(s = args.db_key_source)
    uri_target_input = json.loads(s = args.db_key_target)
    uri_source = uri_source_input.get("uristring", uri_source_input)
    uri_target = uri_target_input.get("uristring", uri_target_input)
    beoneapi.add_URI(mongoURI = uri_source, connection_name = 'source')
    beoneapi.add_URI(mongoURI = uri_target, connection_name = 'target')
    pprint.pprint(beoneapi.CONNECTION_URIS) if verbose_switch else None

    # Check DB Connection
    if not has_a_database_connection(connection_name = "source"):
        logging.error("No connection to the SOURCE database could be established")
        print("No connection to the SOURCE database could be established")
        sys.exit(1)
    if not has_a_database_connection(connection_name = "target"):
        logging.error("No connection to the TARGET database could be established")
        print("No connection to the TARGET database could be established")
        sys.exit(1)
    test_database(connection_name = 'source') if verbose_switch else None
    test_database(connection_name = 'target') if verbose_switch else None

    # Clear Collections
    if args.drop_collections:
        clear_collections(uri = beoneapi.CONNECTION_URIS.get(args.drop_collections))

    # %% Query both Databases #################################################

    # Determine run_type for source and target
    if args.db_key_source.endswith('private"}'):
        source_run_type = 'json_import'
    else:
        source_run_type = 'virtual'

    if args.db_key_target.endswith('private"}'):
        target_run_type = 'json_import'
    else:
        target_run_type = 'virtual'

    # Show all runs and samples in source
    print("############################## source|get_run_list") if verbose_switch else None
    source_run_list = beoneapi.get_run_list(run_type = source_run_type, connection_name = 'source')
    source_run_list_df = pandas.json_normalize(source_run_list, record_path = 'samples', sep = '.')  # meta = 'name', meta_prefix = "run_" avoided, use cell split to recover run_name
    source_run_df = deepcopy(source_run_list_df)
    source_run_df[['run', 'name']] = source_run_df.name.str.split("___", expand = True) if not source_run_df.empty else print(f"The source database has no samples in {source_run_type} runs.")
    source_run_df = source_run_df[['run', 'name', '_id']] if not source_run_df.empty else pandas.DataFrame(columns = ['run', 'name', '_id'])
    pprint.pprint(source_run_df) if verbose_switch else None

    # Show all runs and samples in target
    print("############################## target|get_run_list") if verbose_switch else None
    target_run_list = beoneapi.get_run_list(run_type = target_run_type, connection_name = 'target')
    target_run_list_df = pandas.json_normalize(target_run_list, record_path = 'samples', sep = '.')  # meta = 'name', meta_prefix = "run_" avoided, use cell split to recover run_name
    target_run_df = deepcopy(target_run_list_df)
    target_run_df[['run', 'name']] = target_run_df.name.str.split("___", expand = True) if not target_run_df.empty else print(f"The target database has no samples in {target_run_type} runs.")
    target_run_df = target_run_df[['run', 'name', '_id']] if not target_run_df.empty else pandas.DataFrame(columns = ['run', 'name', '_id'])
    pprint.pprint(target_run_df) if verbose_switch else None

    # %% Transfer non-duplicate Samples between Databases #######################################

    transfer_oid_list = [source_sample for source_sample in source_run_df._id.tolist() if source_sample not in target_run_df._id.tolist()]

    logging.info(f'There are {len(transfer_oid_list)} samples to be transfered.')
    print(f"There are {len(transfer_oid_list)} samples to be transfered.")

    json_xcrypt.get_keychain(path = args.keychain)
    transfer_fails_oid = []
    if len(transfer_oid_list) > 0:
        for transfer_sample_oid in transfer_oid_list:
            transfer_sample_name = source_run_list_df[source_run_df['_id'] == transfer_sample_oid]['name'].to_string(index = False)
            logging.info(f'Transfering {transfer_sample_name}...')
            print(f'Transfering {transfer_sample_name}...')
            transfer_sample = beoneapi.get_sample(sample_id = transfer_sample_oid, connection_name = 'source')

            # Sanity check of sample json
            if transfer_sample['categories'].get('beone', None) is None:
                logging.error(f'The sample {transfer_sample["name"]} does not contain the category "beone"')
                print(f'The sample {transfer_sample["name"]} does not contain the category "beone"')
                transfer_fails_oid.append(transfer_sample_oid)
                continue

            if transfer_sample['categories'].get('cgmlst', {}).get('report', None) is None:
                logging.error(f'The sample {transfer_sample["name"]} does not contain the category "cgmlst"')
                print(f'The sample {transfer_sample["name"]} does not contain the category "cgmlst"')

            # Xcrypt Sample
            if args.mode:
                json_xcrypt.xcrypt_sample(sample = {'sample': {**transfer_sample['categories']['beone']['summary']}}, mode = args.mode)

            # Save transfer_sample from source as new Sample to target db, IMPORTANT: the ObjectId remains the same - method 'Upsert'
            transfer_sample_in_target = beoneapi.save_sample(data_dict = transfer_sample, upsert = True, connection_name = 'target')  # this fails for a user with read-only privileges

            # logging of transfer
            logging.info(f'The OIDs of transferred samples were logged to {args.json_out}')
            print(f'The OIDs of transferred samples were logged to {args.json_out}')
            with open(args.json_out.replace("placeholder", transfer_sample_name), 'w') as file:
                json.dump(
                    obj = transfer_sample_in_target,
                    default = json_util.default,
                    fp = file,
                    indent = 4,
                    sort_keys = False,
                    ensure_ascii = False,
                    allow_nan = False)

        # Associate newly inserted Sample with a new Virtual Run which should contain infos about user, institute and time (only done once, hence commented-out)
        transfer_oid_samples = [sample for sublist in [run['samples'] for run in source_run_list] for sample in sublist]
        transfer_oid_samples_pass = [sample for sample in transfer_oid_samples if sample['_id'] not in transfer_fails_oid]

        timestamp = datetime.now().strftime("%Y-%m-%dT%X")
        new_run_id = beoneapi.create_virtual_run(name = args.username, ip = args.username + '_' + timestamp, samples = transfer_oid_samples_pass, connection_name = 'target')
        beoneapi.add_samples_to_virtual_run(name = args.username, ip = args.username + '_' + timestamp, samples = transfer_oid_samples_pass, connection_name = 'target')

        logging.info(f'A new run with the OID={str(new_run_id)} was created in the target database.\nTransfer finished.')
        print(f'A new run with the OID={str(new_run_id)} was created in the target database.\nTransfer finished.')

    # snakemake logic support
    Path(args.json_out).touch(exist_ok = True)


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
