#!/usr/bin/env python3

print("""
json_merger.py - a script from the BfR Genome Surveillance pipelines:
https://gitlab.com/bfr_bioinformatics/bfr_beone.git
""")

# %% Packages #################################################################

# Python Standard Packages
import sys
import os
import traceback
import warnings
import logging
import argparse
import json

# ScriptInfo
from pathlib import Path
from datetime import datetime

# Other Packages
from helper_functions import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(msg = ''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Warning Behaviour ########################################################

def _warning(message, category = UserWarning, filename = '', lineno = -1, file = '', line = ''):
    """
    Omit excessive warning message on STDOUT
    """
    logging.warning(msg = message)


warnings.simplefilter('error', RuntimeWarning)  # test with 'always', arm with 'error'
warnings.simplefilter('always', UserWarning)
warnings.showwarning = _warning


# %% Script Info ##############################################################

class ScriptInfo:
    repo_path: Path
    script_name: str
    timestamp: datetime
    debug_switch: bool

    def __init__(self, debug_switch: bool = False):
        self.repo_path = Path(__file__).resolve().parent.parent
        self.script_name = Path(__file__).name
        self.timestamp = datetime.now()
        self.debug_switch = debug_switch


global script_info


# %% Define Source of Arguments ###############################################

def argument_parser() -> argparse.ArgumentParser:
    global script_info
    parser = argparse.ArgumentParser()

    # required JSON input
    parser.add_argument('-a', '--aquamis', type = Path, required = True, help = 'Path to the JSON file of AQUAMIS results')
    # optional JSON input
    parser.add_argument('-b', '--bakcharak', type = Path, required = False, help = 'Path to the JSON file of BakCharak results')
    parser.add_argument('-c', '--chewiesnake', type = Path, required = False, help = 'Path to the JSON file of chewieSnake results')
    parser.add_argument('-m', '--metadata', type = Path, required = False, help = 'Path to the single-sample JSON or sample-containing JSON Line metadata file derived from the BeONE sample sheet')
    parser.add_argument('-f', '--format', required = False, default = "json", help = 'Format of the metadata JSON file, either "json" or "json_line"; default: json')
    # required output
    parser.add_argument('-o', '--outfile', type = Path, required = True, help = 'Output file')
    # optional logfile (logging is always on)
    parser.add_argument('-l', '--logfile', type = Path, required = False, help = 'Path to the logfile; default: <Output_directory>/<Sample>.merge.log')

    return parser


# Create Arguments for Debugging
def create_arguments() -> argparse.Namespace:
    global script_info
    debug_workdir = script_info.repo_path / "results-bfr-abc"  # "results-bfr-abc"
    debug_sample = "ERR1759085"  # "ERR1759085" "SRR14998767" "SRR16193751"

    _args = argparse.Namespace(
        metadata = _get_path(wildcards = debug_sample, column = 'metadata'),
        format = debug_format,
        aquamis = _get_path(wildcards = debug_sample, column = 'aquamis'),
        bakcharak = _get_path(wildcards = debug_sample, column = 'bakcharak'),
        chewiesnake = _get_path(wildcards = debug_sample, column = 'chewiesnake'),
        outfile = debug_workdir / f"{debug_sample}.merge.json",  # "5_merge"
        logfile = debug_workdir / "log" / f"{debug_sample}_merge.debug.log"  # "0_logs" "0_logs"
    )

    ## legacy version with hardcoded paths
    # args = argparse.Namespace(
    #     # metadata = Path(debug_workdir) / "1_metadata" / "BeONE_BfR_Salmonella_SampleSheet.jsonl"
    #     # metadata = Path(debug_workdir) / "1_metadata" / "BeONE_BfR_Salmonella_SampleSheet.jsonl"
    #     metadata = Path(debug_workdir) / "1_metadata" / "BeONE_Salmonella_SampleSheet.jsonl"
    #     format = debug_format
    #     aquamis = Path(debug_workdir) / "2_aquamis" / "json" / "post_qc" / f"{debug_sample}.aquamis.json"
    #     bakcharak = Path(debug_workdir) / "3_bakcharak" / "json" / f"{debug_sample}.bakcharak.json"
    #     chewiesnake = Path(debug_workdir) / "4_chewiesnake" / "cgmlst" / "json" / f"{debug_sample}.chewiesnake.json"
    #     outfile = Path(debug_workdir) / "5_merge" / f"{debug_sample}.merge.json"
    #     logfile = Path(debug_workdir) / "0_logs" / f"{debug_sample}_mergeJson.log"
    # )

    return _args


# Translate Snakemake to ArgParse Object
def snakemake_to_args(snakemake) -> argparse.Namespace:
    _args = argparse.Namespace(
        metadata = snakemake.params['metadata'],
        format = snakemake.params['format'],
        aquamis = snakemake.input['aquamis'],
        bakcharak = snakemake.params['bakcharak'],
        chewiesnake = snakemake.params['chewiesnake'],
        outfile = snakemake.output['merge'],
        logfile = snakemake.log['log']
    )
    return _args


# Define Source of Arguments
def python_or_snakemake_args() -> argparse.Namespace:
    global script_info
    try:
        snakemake.input[0]
    except NameError:
        if script_info.debug_switch:
            _args = create_arguments()
        else:
            parser = argument_parser()
            _args = parser.parse_args()
    else:
        _args = snakemake_to_args(snakemake = snakemake)
    return _args


# %% JSON Functions ###########################################################

def import_json(path: Path, filetype: str) -> dict:
    """Check file presence and import JSON

    Args:
        :param path: path to json file
        :param filetype: json type description

    Returns:
        dict:
    """
    try:
        json_module = json.load(fp = path.open('r'), object_pairs_hook = unquote_hook)
        logging.info(f'Importing {filetype} JSON: {path}')
    except (FileNotFoundError, TypeError, AttributeError):
        warnings.warn(message = f'Importing {filetype} JSON failed: {path}', category = UserWarning)
        json_module = dict()
    return json_module


def import_jsonl(path, sample, filetype):
    """Check file presence and import JSON Line

    Args:
        :param path: path to json file
        :param sample: sample name
        :param filetype: json type description

    Returns:
        dict:
    """
    json_module = dict()
    try:
        with open(path, 'r') as file:
            for line in file:
                if sample.replace("-", "_") in line:
                    json_module.update(json.loads(s = line, object_pairs_hook = unquote_hook))
                    return json_module
            warnings.warn(message = f"The json line file did not match the given sample string: {sample.replace('-', '_')}", category = UserWarning)
    except (FileNotFoundError, TypeError):
        warnings.warn(message = f"The {filetype} path was not found: '{path}' for {sample}", category = UserWarning)
        json_module = dict()
    return json_module


# %% Main #####################################################################

def main():
    # %% Setup #####

    # Script Information
    global script_info
    script_info = ScriptInfo(debug_switch = False)

    # Parse Arguments
    args = python_or_snakemake_args()

    # Complement Arguments
    input_basename = args.aquamis.stem.split('.')[0]
    args.logfile = args.aquamis.parent / f"{input_basename}.merge.log" if not args.logfile else args.logfile

    # Configure Logging
    logging.basicConfig(filename = args.logfile,
                        encoding = 'utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')

    # add additional STDOUT handler for logger
    root = logging.getLogger()
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    root.addHandler(handler)

    # %% Workflow #####

    logging.info(f"[START] {__file__}")
    warnings.warn(message = f"{script_info.script_name} is running in DEBUG mode", category = UserWarning) if script_info.debug_switch else None
    logging.info(msg = args) if script_info.debug_switch else None

    # Import JSON
    json_module_aquamis = import_json(path = args.aquamis, filetype = "aquamis")  # required

    if args.format == "json":
        logging.info(f"Using sample-specific JSON metadata file: {args.metadata}")
        json_module_metadata = import_json(path = args.metadata, filetype = "metadata")  # optional
    elif args.format == "json_line":
        logging.info(f"Using joint JSONL metadata file: {args.metadata}")
        json_module_metadata = import_jsonl(path = args.metadata, sample = json_module_aquamis["sample"]["summary"]["sample"], filetype = "metadata")  # optional
    else:
        json_module_metadata = dict()
    json_module_bakcharak = import_json(path = args.bakcharak, filetype = "bakcharak")  # optional
    json_module_chewiesnake = import_json(path = args.chewiesnake, filetype = "chewiesnake")  # optional

    # %% legacy data model conversion #####

    # aquamis before 2022-05-09 (pre-b99082fd)
    if 'analysis' in json_module_aquamis['sample']:
        json_module_aquamis['sample']['run_metadata'] = json_module_aquamis['sample'].pop('analysis')

        for run_metadatum in json_module_aquamis['sample']['run_metadata']:
            read1_md5 = checksum_md5(run_metadatum['read1']) if Path(run_metadatum['read1']).is_file() else None
            read2_md5 = checksum_md5(run_metadatum['read2']) if Path(run_metadatum['read2']).is_file() else None

            aquamis_metadatum_tmp = {
                "pipeline"            : "AQUAMIS",
                "timestamp"           : run_metadatum.pop('timestamp'),
                "sample_description"  : {
                    "sample"    : json_module_aquamis['sample']['summary']['sample'],
                    "read1_file": run_metadatum.pop('read1'),
                    "read2_file": run_metadatum.pop('read2'),
                    "read1_md5" : read1_md5,
                    "read2_md5" : read2_md5
                },
                "pipeline_metadata"   : {
                    "stage"      : "",
                    "host_system": run_metadatum.pop('host_system'),
                    "config"     : run_metadatum.pop('config'),
                    "software"   : {}
                },
                "database_information": {}
            }

            run_metadatum.update(aquamis_metadatum_tmp)

    if not 'species' in json_module_aquamis['sample']['summary']:
        _kraken_df = json_module_aquamis['pipelines']['kraken2']['contig_based']['kraken_summary_species']
        kraken_contig_majority_species = _kraken_df[0].get('species', None)
        json_module_aquamis['sample']['summary']['species'] = kraken_contig_majority_species

    # %% JSON Aggregation #####

    ## (1) start aggregation with mandatory aquamis
    sample_name = json_module_aquamis['sample']['summary']['sample']
    json_assembly = json_module_aquamis

    ## weave-in metadata
    json_assembly['sample'].update({'metadata': json_module_metadata})

    ## (2) weave-in bakcharak

    # >>> bakcharak temporary restructuring >>>

    # TODO: bakcharak.json
    # * restructure run metadata into one clade
    # * reformat version_information_list from metric array to query-able table
    # * reformat mlst; compare it with previous aquamis mlst?

    timestamp = datetime.now()

    json_module_bakcharak['meta_information'].pop("report")
    json_module_bakcharak['meta_information'].pop("bakcharak_version")

    bakcharak_tmp = {
        "technical_metadata": {
            "pipeline"            : "bakcharak",
            "timestamp"           : str(timestamp),
            "sample_description"  : json_module_bakcharak.get('meta_information', {}),
            "pipeline_metadata"   : {
                "config"  : import_json(path = f"results-bfr-abc/modules/config_bakcharak_{json_module_aquamis['sample']['summary']['sample']}.json", filetype = "config"),
                "software": pd.DataFrame.from_dict(json_module_bakcharak.get('version_information_list', {}).get('software_versions', {})).set_index('Software').to_dict()['Version']
            },
            "database_information": pd.DataFrame.from_dict(json_module_bakcharak.get('version_information_list', {}).get('database_versions', {})).to_dict(orient = "records")
        }

    }

    json_module_bakcharak.pop("sample")
    json_module_bakcharak.pop("meta_information")
    json_module_bakcharak.pop("version_information_list")
    bakcharak_tmp.update(json_module_bakcharak) if bool(json_module_bakcharak) else None
    json_module_bakcharak = deepcopy(bakcharak_tmp)
    json_assembly['sample']['run_metadata'].append(json_module_bakcharak.get('technical_metadata', {})) if bool(json_module_bakcharak) else None
    # <<< bakcharak temporary restructuring <<<

    json_assembly['pipelines'].update({'bakcharak': json_module_bakcharak}) if bool(json_module_bakcharak) else None

    ## (3) weave-in chewiesnake

    # >>> chewiesnake temporary restructuring >>>
    json_module_chewiesnake.get('technical_metadata', {}).update({
        "pipeline" : "chewiesnake",
        "timestamp": json_module_chewiesnake.get('technical_metadata', {}).get('sample_description', {}).get('timestamp', {})
    })
    # <<< chewiesnake temporary restructuring <<<

    json_assembly['pipelines'].update({'chewiesnake': json_module_chewiesnake}) if bool(json_module_chewiesnake) else None
    json_assembly['sample']['run_metadata'].append(json_module_chewiesnake.get('technical_metadata', {})) if bool(json_module_chewiesnake) else None  # 'run_metadata' 'technical_metadata'
    json_assembly['sample']['qc_assessment'].update({'qc_cgmlst': json_module_chewiesnake.get('allele_stats', {}).get('allele_qc', "")}) if bool(json_module_chewiesnake) else None  # TODO: rename allele_qc?

    # %% Integrity Check #####

    '''
    NOTE: The integrity check section tests if json_modules describe the same fasta/fastq files.
    
    Critical integrity violations should raise warnings of the "category=RuntimeWarning".
    The default is "category=UserWarning". Adjust behaviour in the code cell "Warning Behaviour".
    
    Sample Name:
    "mlst": "id" | not unique (!)
    "confindr": "Sample" | unique
    "quast": "misassemblies": "Assembly" | unique
    "all others": "sample"
    '''

    if len(set(json_extract(obj = json_assembly, keys = ["sample", "Sample", "Assembly"]))) > 1:  # this one misses out on MLST
        warnings.warn(message = "The sample names evaluated in the pipelines are not the same.", category = RuntimeWarning)

    if len(set(json_extract(obj = json_assembly, keys = ["fasta_md5"]))) > 1:
        warnings.warn(message = "The fasta files evaluated in the pipelines do not share the same MD5 hash.", category = RuntimeWarning)

    if len(set(json_extract(obj = json_assembly, keys = ["read1_md5"]))) > 1:
        warnings.warn(message = "The read1 files evaluated in the pipelines do not share the same MD5 hash.", category = RuntimeWarning)

    if len(set(json_extract(obj = json_assembly, keys = ["read2_md5"]))) > 1:  # TODO: test with single-end data
        warnings.warn(message = "The read2 files evaluated in the pipelines do not share the same MD5 hash.", category = RuntimeWarning)

    if json_assembly['sample']['qc_assessment']['qc_VOTE'] == "FAIL":
        warnings.warn(message = f"The sample did not pass the AQUAMIS QC: {json_assembly['sample']['summary']['sample']}", category = UserWarning)

    if bool(json_assembly['pipelines'].get('chewiesnake', {})):
        if json_assembly['pipelines']['chewiesnake'].get('allele_stats', {}).get('allele_qc', {}) == "FAIL":
            warnings.warn(message = f"The sample did not pass the chewieSnake QC: {json_assembly['sample']['summary']['sample']}", category = UserWarning)

        if json_assembly['pipelines']['chewiesnake'].get('run_metadata', {}).get('database_information', {}).get('scheme_md5', {}) != \
                json_assembly['pipelines']['chewiesnake'].get('run_metadata', {}).get('sample_description', {}).get('allele_profile_md5', {}):
            warnings.warn(message = "Allele profile locus names do not have the same MD5 digest as the schema.", category = RuntimeWarning)

    # %% Write Merged JSON structure #####

    if args.outfile.is_file():
        warnings.warn(message = f"Outfile already exists and will be overwritten: {args.outfile}", category = UserWarning)
    else:
        logging.info(f"Writing output to: {args.outfile}")

    args.outfile.parent.mkdir(parents = True, exist_ok = True)
    with open(args.outfile, 'w') as file:
        json.dump(
            obj = strictJsonObj(
                nestedObj = json.loads(
                    s = nestedDict_to_json(
                        nestedObj = json_assembly)),
                uriCompliant = True),
            fp = file,
            indent = 4,
            sort_keys = False,
            ensure_ascii = False,
            allow_nan = False)  # Improvement: Use .encode('utf-8') in combo with ensure_ascii=False if None values are omitted instead of coerced to None

    print(f'JSON Merging of BfR-ABC results for {sample_name} finished')
    logging.info(f"[STOP] {__file__}")


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
