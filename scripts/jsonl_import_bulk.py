import pymongo
import json
import jsonlines
from pymongo import MongoClient, InsertOne

client = pymongo.MongoClient(host = "mongodb://127.0.0.1:26016")
db = client.bfr_bulk
collection = db.beone
requesting = []

with jsonlines.open(r"/home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_dataset_all_beone.jsonl") as reader:
    for jsonObj in reader:
        requesting.append(InsertOne(jsonObj))

result = collection.bulk_write(requesting)
client.close()
