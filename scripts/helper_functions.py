#!/usr/bin/env python3

'''
helper_functions.py - a script from the BfR Genome Surveillance pipelines:
https://gitlab.com/bfr_bioinformatics/bfr_beone.git
'''

# %% Packages

# Python Standard Packages
import os
import sys
import json
import hashlib
import pprint
import urllib.parse
import argparse
from pathlib import Path
from datetime import datetime
from copy import deepcopy

# Other Packages
import pandas as pd
import numpy as np

# %% Dictionaries

defaultValues_dict = {
    'string' : str(),
    'int'    : int(0),
    'integer': int(0),
    'float'  : float(0),
    'number' : int(0),
    'array'  : [],
    'list'   : [],
    'tuple'  : [],
    'dict'   : {},
    'object' : {},
    'boolean': False,
    'null'   : None
}

defaultTypes_dict = {
    'string' : 'string',
    'object' : 'dict',
    'integer': 'integer',
    'number' : 'float',
    'array'  : 'list',
    'list'   : 'list',
    'boolean': 'boolean',
    'null'   : None
}


# %% Functions

def mtime_file(filePath: str):
    try:
        _timestamp = datetime.fromtimestamp(os.path.getmtime(filePath))
    except:
        _timestamp = None
    return _timestamp


class Head(object):
    def __init__(self, lines, fd = sys.stdout):
        self.lines = lines
        self.fd = fd

    def write(self, msg):
        if self.lines <= 0:
            return
        n = msg.count('\n')
        if n < self.lines:
            self.lines -= n
            return self.fd.write(msg)
        ix = 0
        while (self.lines > 0):
            iy = msg.find('\n', ix + 1)
            self.lines -= 1
            ix = iy
        return self.fd.write(msg[:ix])


def pprint_head(to_print, length = 10):
    pprint.pprint(to_print, stream = Head(length))
    print()


def printjs(jsonObj: str, indent = 4, sort_keys = False, allow_nan = False):
    print(json.dumps(obj = jsonObj, indent = indent, sort_keys = sort_keys, allow_nan = allow_nan))


def merge_dict(original: dict, patch: dict, path: list = None) -> dict:
    if path is None:
        path = []
    for key in patch:
        if key in original:
            if isinstance(original[key], dict) and isinstance(patch[key], dict):
                merge_dict(original[key], patch[key], path + [str(key)])
            elif original[key] == patch[key]:
                pass  # same leaf value
            else:
                raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            original[key] = patch[key]
    return original


def nestedDict_to_json(nestedObj: (dict, list), name: str = None):
    jsonstring = str()
    try:
        jsonstring += json.dumps(nestedObj, ensure_ascii = False, allow_nan = True)
    except:
        if isinstance(nestedObj, dict):
            for k, v in nestedObj.items():
                if isinstance(v, pd.DataFrame):
                    jsonstring += '"' + k + '"' + ": " + v.to_json(orient = 'records', force_ascii = False)
                else:
                    try:
                        jsonstring += '"' + k + '"' + ": " + json.dumps(v, ensure_ascii = False, allow_nan = True)
                    except:
                        jsonstring += nestedDict_to_json(nestedObj = v, name = k)
                if k == list(nestedObj.keys())[-1]:
                    jsonstring = '{' + jsonstring + '}'
                else:
                    jsonstring += ','
        elif isinstance(nestedObj, list):
            for element in nestedObj:
                try:
                    jsonstring += json.dumps(element, ensure_ascii = False, allow_nan = True)
                except:
                    jsonstring += nestedDict_to_json(nestedObj = element)
            jsonstring = '[' + jsonstring + ']'
    if name is not None:
        jsonstring = '"' + name + '": ' + jsonstring
    return jsonstring


def strictJsonObj(nestedObj, uriCompliant: bool):
    if isinstance(nestedObj, list):
        new_list = []
        for element in nestedObj:
            if element is not None and isinstance(element, float):
                if not np.isfinite(element):
                    new_list.append(None)
            elif isinstance(element, (list, dict)):
                new_list.append(strictJsonObj(nestedObj = element, uriCompliant = uriCompliant))
            else:
                new_list.append(element)
        nestedObj = new_list
    if isinstance(nestedObj, dict):
        nullStrings = ['-', '--', 'NA', 'ND']
        for key in nestedObj.keys():
            if nestedObj[key] is not None and isinstance(nestedObj[key], float):
                if not np.isfinite(nestedObj[key]):
                    nestedObj[key] = None
            if nestedObj[key] is not None and isinstance(nestedObj[key], str):
                if nestedObj[key] in nullStrings:
                    nestedObj[key] = None
            elif isinstance(nestedObj[key], (list, dict)):
                nestedObj[key] = strictJsonObj(nestedObj = nestedObj[key], uriCompliant = uriCompliant)
        if uriCompliant:
            nestedObj = {urllib.parse.quote(key): value for (key, value) in nestedObj.items()}
    return nestedObj


def unquote_hook(quotedObj):
    obj = {urllib.parse.unquote(key): value for (key, value) in dict(quotedObj).items()}
    return obj


def translateSchema_json2cerberus(schemaObj, defaultValues = None, objName = 'Root'):
    debug_switch = False
    newDict = dict()
    if isinstance(schemaObj['type'], list):
        newDict[objName] = {'type': [defaultTypes_dict[type] for type in schemaObj['type']]}
    else:
        newDict[objName] = {'type': defaultTypes_dict[schemaObj['type']]}
    try:
        schemaObj['$id']
    except KeyError:
        if debug_switch:
            print(objName + ' has no $id.')
        objId = objName
    else:
        objId = schemaObj['$id']
    try:
        schemaObj['properties']
    except KeyError:
        if debug_switch:
            print(objId + ' has no properties.')
        pass
    else:
        newDict[objName]['schema'] = {}
        for key in schemaObj['properties'].keys():
            newDict[objName]['schema'][key] = translateSchema_json2cerberus(schemaObj = schemaObj['properties'][key],
                                                                            defaultValues = defaultValues,
                                                                            objName = key)[key]
    if schemaObj['type'] == 'array':
        try:
            schemaObj['items']
        except KeyError:
            if debug_switch:
                print(objId + ' has no items.')
            pass
        else:
            try:
                schemaObj['properties']
            except KeyError:
                if debug_switch:
                    print(objId + ' has no properties.')
                pass
            else:
                newDict[objName]['schema'] = {}
                for item in schemaObj['items']['properties'].keys():
                    newDict[objName]['schema'][item] = translateSchema_json2cerberus(schemaObj = schemaObj['items']['properties'][item],
                                                                                     defaultValues = defaultValues,
                                                                                     objName = item)[item]
    if defaultValues is not None:
        try:
            schemaObj[objName]['default']
        except KeyError:
            if debug_switch:
                print(objId + ' has no default value.')
            newDict[objName]['default'] = defaultValues_dict[newDict[objName]['type']]
        else:
            newDict[objName]['default'] = schemaObj[objName]['default']
    return newDict


def schemaSkeleton(schemaObj, insertField: dict = None, excludeField: list = None):
    exclusion_list = ['properties', 'items']  # these are branch points
    if insertField is not None:
        exclusion_list += list(insertField.keys())
    if excludeField is not None:
        exclusion_list += excludeField
    if isinstance(schemaObj, list):
        new_list = []
        for element in schemaObj:
            if isinstance(element, (list, dict)) and len(schemaSkeleton(schemaObj = element, insertField = insertField, excludeField = excludeField)) != 0:
                new_list.append(schemaSkeleton(schemaObj = element, insertField = insertField, excludeField = excludeField))
        new_obj = new_list
    if isinstance(schemaObj, dict):
        new_dict = deepcopy(schemaObj)
        for key in schemaObj.keys():
            if key == '$id' and insertField is not None:
                new_dict.update(insertField)
            if isinstance(schemaObj[key], (list, dict)):
                new_dict[key] = schemaSkeleton(schemaObj = schemaObj[key], insertField = insertField, excludeField = excludeField)
                if len(new_dict[key]) == 0:
                    new_dict.pop(key)
            else:
                if key not in exclusion_list:
                    new_dict.pop(key)
        new_obj = new_dict
    return new_obj


def sortSchema(target, template):
    if isinstance(target, list):
        new_list = []
        for num, element in enumerate(template):
            if isinstance(element, (list, dict)):
                new_list.append(sortSchema(target = target[num], template = template[num]))
            else:
                new_list.append(element)
        new_obj = new_list
    if isinstance(target, dict):
        new_dict = {}
        unionSet = list(template.keys()) + [x for x in target.keys() if x not in template.keys()]
        for key in unionSet:
            if isinstance(target[key], (list, dict)):
                new_dict[key] = sortSchema(target = target[key], template = template[key])
            elif key == 'title':
                new_dict[key] = urllib.parse.unquote(os.path.basename(target['$id'])).capitalize()
            else:
                new_dict[key] = target[key]
        new_obj = new_dict
    return new_obj


def docker_path_replace(nestedObj: dict, newPath: str):
    for key in nestedObj.keys():
        if isinstance(nestedObj[key], dict):
            nestedObj[key] = docker_path_replace(nestedObj = nestedObj[key], newPath = newPath)
        elif isinstance(nestedObj[key], str):
            nestedObj[key] = nestedObj[key].replace('/AQUAMIS/analysis', newPath)
    return nestedObj


def checksum_md5(filename, buffer_size = (128 * (2 ** 9))):
    """Create MD5 checksum of file.

    Args:
        filename (str): file path
        buffer_size (int): read data in 64k chunks, exponent is variable

    Returns:
        str: HEX digest
    """
    md5sum = hashlib.md5()
    with open(filename, 'rb') as f:
        for chunk in iter(lambda: f.read(buffer_size), b''):
            md5sum.update(chunk)
    return md5sum.hexdigest()


def checksum_md5s(string):
    """Create MD5 checksum of string.

    Args:
        string (str): string

    Returns:
        str: HEX digest
    """
    md5sum = hashlib.md5()
    md5sum.update(string.encode('utf-8'))
    return md5sum.hexdigest()


def json_extract_single(obj, key):
    """Recursively fetch values from nested JSON with exakt matches of a string.

    Args:
        obj: complex, nested JSON object
        key (str): single string to match a key

    Returns:
        list: values of matched keys
    """
    arr = []

    def extract(obj, arr, key):
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    values = extract(obj, arr, key)
    return values


def json_extract(obj, keys: list):
    """Recursively fetch values from nested JSON based on a list of key strings.
    Args:
        obj: complex, nested JSON object
        keys (list): list of strings to match a key
    Returns:
        list: values of matched keys
    """
    result = []
    for item in keys:
        result.extend(json_extract_single(obj = obj, key = item))
    return result


# %% Import EFSA dictionaries

def import_efsa_vocabulary(directory: Path, inverted: bool = True) -> dict:
    """
    Import the EFSA vocabularies from JSON files in the specified directory.

    Args:
        directory (Path): The path to the directory containing the JSON files.
        inverted (bool): Whether to invert the dictionary.

    Returns:
        dict: A dictionary containing the EFSA vocabularies.
    """
    # Initialize an empty dictionary to store the JSON data
    json_data = {}

    # Iterate through the files in the directory
    for filename in os.listdir(directory):
        if filename.endswith('.json'):
            # Get the filename without the extension
            key = os.path.splitext(filename)[0]

            # Construct the full file path
            file_path = os.path.join(directory, filename)

            # Load the JSON file
            with open(file_path, 'r') as file:
                json_data[key] = {v: k for k, v in json.load(file).items()} if inverted else json.load(file)

    return json_data


def abbreviate_genus(species_name: str) -> str:
    """
    Abbreviate the genus of a species name.

    Args:
        species_name (str): The species name.

    Returns:
        str: The abbreviated species name.
    """
    # Split the species name into genus and species
    words = species_name.split()
    if not words:
        return species_name
    first_word_abbr = words[0][0].upper() + '.'
    return first_word_abbr + ' ' + ' '.join(words[1:])


def parse_args_to_str(args_dict: dict) -> dict:
    """
    Convert all Path arguments in args to their string representation.

    Args:
        args_dict (argparse.Namespace): The arguments namespace.

    Returns:
        dict: A dictionary with all arguments, converting Path types to str.
    """
    for key, value in args_dict.items():
        if isinstance(value, Path):
            args_dict[key] = str(value)
    return args_dict
