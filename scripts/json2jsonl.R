#!/usr/bin/R

## Libraries for Debug
library(tidyverse)
library(rrapply)
library(urltools)

## Current Data Directory
cdd <- file.path('', 'home', 'Brendeba', 'data', 'git', 'bfr_beone')
cdd <- file.path('', 'home', 'brendy', 'data', 'git', 'bfr_beone')

target <- file.path(cdd, 'test_data_beone', '5_merge')

## Load merged JSONs
json_input_path <- list.files(path = target, pattern = "\\.merge\\.json$", full.names = TRUE)
json_input_names <- gsub(pattern = "\\.merge\\.json$", replacement = "", basename(json_input_path))

## Import JSON strings
json_sample_str_post <- purrr::set_names(
  x = purrr::map(json_input_path,
                 function(.x)readr::read_file(file = .x)),
  nm = json_input_names)

## Parse JSON strings to R object
json_sample_obj_post <- purrr::set_names(
  x = purrr::map(json_sample_str_post,
                 function(.x)jsonlite::fromJSON(
                   txt = .x,
                   simplifyVector = TRUE,
                   simplifyDataFrame = TRUE,
                   simplifyMatrix = TRUE,
                   flatten = FALSE)) %>% 
    rrapply::rrapply(object = .,
                     classes = c("list", "data.frame"),
                     f = function(.x) {
                       if (!is_null(names(.x))) {names(.x) <- urltools::url_decode(names(.x))}
                       return(.x)
                     },
                     how = "recurse") %>%
    rrapply::rrapply(object = .,  # this eliminates NULL fields and mitigates conversion errors into data.frames, this error only happens in R v3.5.1
                     classes = "NULL",
                     f = function(.x) if (is_null(.x)) {.x <- NA},
                     how = "recurse"),
  nm = names(json_sample_str_post))

## Export R Object as JSONL a.k.a. NDJSON file - one sample per line
jsonl_out <- file.path(target, paste0(gsub(pattern = "-\\d+$", replacement = "", basename(json_input_names[[1]])), ".jsonl"))
con_jsonl <- file(jsonl_out, open = "wb")
jsonlite::stream_out(
  x = unnest_wider(data = tibble(json = json_sample_obj_post), col = "json", simplify = FALSE),
  con = con_jsonl,
  pagesize = 500,
  verbose = TRUE,
  prefix = "",
  # arguments for toJSON 
  dataframe = "rows", # c("rows", "columns", "values")
  matrix = "rowmajor", # c("rowmajor", "columnmajor")
  Date = "ISO8601", # c("ISO8601", "epoch")
  POSIXt = "ISO8601", # c("string", "ISO8601", "epoch", "mongo")
  factor = "string", # c("string", "integer")
  null = "null", # c("list", "null")
  na = "null", # c("null", "string")
  auto_unbox = TRUE,
  digits = 4,
  pretty = 2,
  force = TRUE
)
close(con_jsonl)
