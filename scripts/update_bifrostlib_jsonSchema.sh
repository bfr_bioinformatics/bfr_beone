#!/bin/bash

CONDA_INFO=$(conda info --json)
CONDA_BASE=$(echo "$CONDA_INFO" | grep -Po '(?<="root_prefix": ).*$' | tr -d '",\0')
cd ${CONDA_BASE}/envs/pyjson/lib/python3.9/site-packages/bifrostlib/schemas/
mv bifrost.jsonc bifrost.jsonc.bak
wget https://raw.githubusercontent.com/HBrendy/bifrostlib/holgers_branch/bifrostlib/schemas/bifrost.jsonc
