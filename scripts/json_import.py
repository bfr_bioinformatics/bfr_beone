#!/usr/bin/env python3

'''
BeONE JSON import into Bifrost DB structure via new Component "beone" as well as categories "beone" and "chewiesnake"
BEWARE: there's a snakemake params switch clear_collections ... in your database ... instantly!
Make sure mongod is up and running and environment variable BIFROST_DB_KEY is set to a TEST database.
$ sudo systemctl restart mongod
'''

# %% Packages #################################################################

import argparse
import datetime
import logging
import pprint
import re
import sys
import time
import traceback
from typing import List, Dict, TextIO, Tuple
from json import loads, dumps

import pandas
import pymongo
import yaml
from bifrostlib import common
from bifrostlib import datahandling
from bifrostlib.datahandling import Category
from bifrostlib.datahandling import Component
from bifrostlib.datahandling import ComponentReference
from bifrostlib.datahandling import Run
from bifrostlib.datahandling import RunReference
from bifrostlib.datahandling import Sample
from bifrostlib.datahandling import SampleComponent
from bifrostlib.datahandling import SampleComponentReference
from bifrostlib.datahandling import SampleReference

from helper_functions import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Define Source of Arguments ###############################################

def parse_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    parser = argparse.ArgumentParser()

    # input
    parser.add_argument('--json_in', '-i', help = 'Path to JSON file of BeONE results, e.g. sample.beone.json',
                        type = os.path.abspath, required = True)
    # output
    parser.add_argument('--json_run', '-x', help = 'Output path for the db representation of the created run object, e.g. sample.bifrost_run.json',
                        type = os.path.abspath, required = True)
    parser.add_argument('--json_sample', '-y', help = 'Output path for the db representation of the created sample object, i.e. e.g. sample.bifrost_sample.json',
                        type = os.path.abspath, required = True)
    # params
    parser.add_argument('--sample', '-s', help = 'String of the Sample name',
                        required = True)
    parser.add_argument('--run_name', '-r', help = 'String of the Run name. If left empty, a run name will be created in section prepare_args()',
                        required = False)
    parser.add_argument('--component_path', '-c', help = 'Path to the BIFROST component BeONE description directory, default: ' + repo_dir,
                        type = os.path.abspath, required = True)
    parser.add_argument('--db_key', '-d', help = 'JSON String of the local MongoDB database: {"uristring": key}',
                        required = True)
    # log
    parser.add_argument('--log', '-l', help = 'Path for the logfile',
                        type = os.path.abspath, required = True)

    return parser.parse_args()


# Create Arguments for Debugging
def create_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    debug_sample = 'Se-BfR-0001'  # 'GMI-17-001-DNA' 'Se-Germany-BfR-0001' 'Se-BfR-0001'
    debug_runname = 'BeONE'
    debug_workdir = os.path.join(repo_dir, "test_data_workshop")  # 'test_data' 'test_data_beone' 'test_data_workshop'

    args = argparse.Namespace(
        json_in = os.path.join(debug_workdir, '6_filtered', debug_sample + '.beone.json'),  # BeONE
        json_run = os.path.join(debug_workdir, '7_import', debug_sample + '.bifrost_run.json'),
        json_sample = os.path.join(debug_workdir, '7_import', debug_sample + '.bifrost_sample.json'),
        sample = debug_sample,
        run_name = debug_runname,
        component_path = os.path.join(repo_dir, 'bifrost_beone'),
        db_key = '{"uristring": "mongodb://bfr:mypassword@localhost:28018/beone_bfr_private?authSource=beone_bfr_private"}',  # os.environ['BIFROST_DB_KEY']
        log = os.path.join(debug_workdir, '0_logs', debug_sample + '_importJson_debug.log')
    )
    return args


# Translate ArgParse to Snakemake Object
def arguments_to_snakemake(args):
    global snakemake
    snakemake_input_df = pandas.DataFrame(
        data = [['json_in', args.json_in]],
        columns = ['index', 'input'])
    snakemake_output_df = pandas.DataFrame(
        data = [['json_run', args.json_run],
                ['json_sample', args.json_sample]],
        columns = ['index', 'output'])
    snakemake_params_df = pandas.DataFrame(
        data = [['sample', args.sample],
                ['run_name', args.run_name],
                ['component_path', args.component_path],
                ['db_key', args.db_key]],
        columns = ['index', 'params'])
    snakemake_log_df = pandas.DataFrame(
        data = [['log', args.log]],
        columns = ['index', 'log'])
    snakemake_df = snakemake_input_df.merge(snakemake_output_df.merge(snakemake_params_df.merge(snakemake_log_df, how = 'outer'), how = 'outer'), how = 'outer')
    snakemake = snakemake_df.set_index(snakemake_df['index'])


# Define Source of Arguments
def python_or_snakemake(debug: bool):
    try:
        snakemake.input[0]
    except NameError:
        if debug:
            arguments_to_snakemake(args = create_arguments())
        else:
            arguments_to_snakemake(args = parse_arguments())


# %% Import Sample ############################################################

def import_sample():
    global beone_sample
    global beone_metadata_pre_assembly
    global beone_metadata_post_assembly
    global beone_chewiesnake

    with open(snakemake.input['json_in'], 'r') as file:
        beone_sample = json.load(fp = file, object_pairs_hook = unquote_hook)
    beone_metadata_pre_assembly  = [item for item in beone_sample['sample']['run_metadata'] if item.get('pipeline_metadata', {}).get('stage', {}) == 'parse_json_before_assembly'][0]
    beone_metadata_post_assembly = [item for item in beone_sample['sample']['run_metadata'] if item.get('pipeline_metadata', {}).get('stage', {}) == 'parse_json_after_assembly'][0]
    beone_chewiesnake = {'chewiesnake': beone_sample['pipelines'].pop('chewiesnake', {})}  # TODO: avoid cgmlst module if chewisnake is empty
    logging.info("Import of sample " + snakemake.params['sample'] + " complete")
    logging.info(f"{snakemake.params['component_path']}")


# %% Clear Collections ########################################################

def clear_collections(uri):
    """Perform db.collection.drop() on all BeONE collections

    :param uri: URI of the MongoDB to clear from the BeONE collections (run_components, runs, sample_components, samples, components)

    Returns:
        System Exit
    """
    logging.info(f"Clearing all BeOne collections from {uri} and exiting")
    print(f"Clearing all BeOne collections from '{os.path.basename(uri).split('?')[0]}' and exiting")
    client = pymongo.MongoClient(uri)
    db = client.get_database()
    db.drop_collection("run_components")
    db.drop_collection("runs")
    db.drop_collection("sample_components")
    db.drop_collection("samples")
    db.drop_collection("components")
    print("Collections cleared")
    sys.exit()


# %% Install Component ########################################################

def install_component():
    COMPONENT['install']['path'] = snakemake.params['component_path']  # CONCERN: the component path string for BeONE is irrelevant, I hope it won't be used elsewhere
    logging.info(f'Installing component "{COMPONENT["name"]}" with path: {COMPONENT["install"]["path"]}')
    try:
        COMPONENT.save()
        logging.info(f'Installing component "{COMPONENT["name"]}": Done installing')
    except:
        logging.error(f'Installing component "{COMPONENT["name"]}": FAILED')
        print(traceback.format_exc(), file = snakemake.log['log'])
        sys.exit(0)


def initialize_component():
    with open(os.path.join(snakemake.params['component_path'], 'config.yaml')) as fh:
        config: Dict = yaml.load(fh, Loader = yaml.FullLoader)

    if not (datahandling.has_a_database_connection()):
        raise ConnectionError("BIFROST_DB_KEY is not set or other connection error")

    global COMPONENT
    try:
        component_ref = ComponentReference(name = config["name"])
        COMPONENT = Component.load(component_ref)
        if COMPONENT is not None and '_id' in COMPONENT.json:
            logging.info(f'Installing component "{COMPONENT["name"]}": Component is already installed')
            return
        else:
            COMPONENT = Component(value = config)
            install_component()
    except Exception as e:
        logging.error(f'Installing component "{COMPONENT["name"]}": FAILED')
        print(traceback.format_exc(), file = snakemake.log['log'])
    return


# %% Init Run and Sample - code from run_launcher pipeline.py #################

def format_metadata(run_metadata: TextIO, rename_column_file: TextIO = None) -> pandas.DataFrame:
    df = None
    try:
        # Map BeONE json as df
        beone_metadata_dict = {'changed_sample_names'   : False,
                               'duplicated_sample_names': False,
                               'filenames'              : (beone_metadata_pre_assembly['sample_description'].get('read1_file', ''),
                                                           beone_metadata_pre_assembly['sample_description'].get('read2_file', '')),
                               'haveMetaData'           : True,
                               'haveReads'              : False,
                               'provided_species'       : beone_sample['sample']['metadata']['Microorganism'],
                               'sample_name'            : beone_sample['sample']['summary']['sample'],
                               'temp_sample_name'       : beone_sample['sample']['summary']['sample']}
        df = pandas.json_normalize(beone_metadata_dict)
        if rename_column_file is not None:
            with open(rename_column_file, "r") as rename_file:
                df = df.rename(columns = json.load(rename_file))
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        samples_no_index = df[df["sample_name"].isna()].index  # drop unnamed samples
        samples_no_files_index = df[df["filenames"].isnull()].index  # drop samples missing reads
        idx_to_drop = samples_no_index.union(samples_no_files_index)
        missing_files = ", ".join([df["sample_name"].iloc[i] for i in samples_no_files_index])
        if missing_files != '':
            logging.warning(f"samples {missing_files} missing files")
        df = df.drop(idx_to_drop)
        # df = df.drop(samples_no_index)
        df["sample_name"] = df["sample_name"].astype('str')
        df["temp_sample_name"] = df["sample_name"]
        df["sample_name"] = df["sample_name"].apply(lambda x: x.strip())
        df["sample_name"] = df["sample_name"].str.replace(re.compile("[^a-zA-Z0-9-_]"), "_")
        df["changed_sample_names"] = df['sample_name'] != df['temp_sample_name']
        df["duplicated_sample_names"] = df.duplicated(subset = "sample_name", keep = "first")
        df["haveReads"] = False
        df["haveMetaData"] = True
        return df
    except:
        logging.error('Function format_metadata() failed')
        with pandas.option_context('display.max_rows', None, 'display.max_columns', None):
            print(df, file = snakemake.log['log'])
            print(traceback.format_exc(), file = snakemake.log['log'])
            raise Exception("bad metadata and/or rename column file")


def get_sample_names(metadata: pandas.DataFrame) -> List["str"]:
    return list(set(metadata["sample_name"].tolist()))


def get_file_pairs(metadata: pandas.DataFrame) -> List[Tuple[str, str]]:
    return list(set(metadata["filenames"].tolist()))


def initialize_run(run: Run, samples: List[Sample], component: Component, input_folder: str = ".",
                   run_metadata: str = "run_metadata.txt", run_type: str = None, rename_column_file: str = None,
                   # regex_pattern: str = r"^(?P<sample_name>[a-zA-Z0-9_\-]+?)(_S[0-9]+)?(_L[0-9]+)?_(R?)(?P<paired_read_number>[1|2])(_[0-9]+)?(\.fastq\.gz)$",
                   component_subset: str = "ccc,aaa,bbb") -> Tuple[Run, List[Sample]]:
    metadata = format_metadata(run_metadata, rename_column_file)

    # BeONE: generate sample dict from json to avoid filesystem lookup
    # file_names_in_metadata = get_file_pairs(metadata)
    # sample_dict, unused_files = parse_directory()
    sample_dict = {beone_sample['sample']['summary']['sample']: [beone_metadata_pre_assembly['sample_description'].get('read1_file', 'NA'),
                                                                 beone_metadata_pre_assembly['sample_description'].get('read2_file', 'NA')]}
    unused_files = []

    run_reference = run.to_reference()
    for sample_name in sample_dict:
        metadata.loc[metadata["sample_name"] == sample_name, "haveMetaData"] = True
        metadata.loc[metadata["sample_name"] == sample_name, "haveReads"] = True
        sample = Sample(name = run.sample_name_generator(sample_name))  # IMPORTANT for BeONE
        sample["run"] = run_reference
        sample["display_name"] = sample_name
        sample_exists = False
        for i in range(len(samples)):
            if samples[i]["name"] == sample_name:
                sample_exists = True
                sample = samples[i]
        paired_reads = Category(
            value = {"name"     : "paired_reads",
                     "component": {"id"  : component["_id"],
                                   "name": component["name"]},  # giving paired reads component id?
                     "summary"  : {"data"     : [os.path.abspath(os.path.join(input_folder, sample_dict[sample_name][0])),
                                                 os.path.abspath(os.path.join(input_folder, sample_dict[sample_name][1]))],
                                   "read1_md5": beone_metadata_pre_assembly['sample_description']['read1_md5'],
                                   "read2_md5": beone_metadata_pre_assembly['sample_description']['read2_md5']}
                     })
        sample.set_category(paired_reads)
        # sample_metadata = json.loads(metadata.iloc[metadata[metadata["sample_name"] == sample_name].index[0]].to_json())
        sample_metadata = metadata.loc[metadata['sample_name'] == sample_name].to_dict(orient = 'records')[0]  # more stable to missing fields
        sample_metadata['filenames'] = list(sample_metadata['filenames'])  # changing from tuple to list to match original
        sample_info = Category(value = {
            "name"     : "sample_info",
            "component": {"id": component["_id"], "name": component["name"]},
            "summary"  : sample_metadata
        })
        sample.set_category(sample_info)

        sample.save()
        if sample_exists is False:
            samples.append(sample)
    run['component_subset'] = component_subset  # this might just be for annotating in the db
    run["type"] = run_type
    run["path"] = beone_metadata_pre_assembly['pipeline_metadata']['config']['workdir']
    run["issues"] = {
        "duplicated_samples"      : list(metadata[metadata['duplicated_sample_names'] == True]['sample_name']),
        "changed_sample_names"    : list(metadata[metadata['changed_sample_names'] == True]['sample_name']),
        "unused_files"            : unused_files,
        "samples_without_reads"   : list(metadata[metadata['haveReads'] == False]['sample_name']),
        "samples_without_metadata": list(metadata[metadata['haveMetaData'] == False]['sample_name']),
    }
    run.samples = [i.to_reference() for i in samples]
    run.save()

    with open(snakemake.output['json_run'], "w") as fh:
        fh.write(pprint.pformat(run.json))
    with open(snakemake.output['json_sample'], "w") as fh:
        for sample in samples:
            fh.write(pprint.pformat(sample.json))
    return (run, samples)


def run_pipeline() -> None:
    args = prepare_args(run_name = snakemake.params['run_name'])
    run_reference = RunReference(_id = args.run_id, name = args.run_name)
    logging.info("Run Reference Object: " + json.dumps(run_reference.json))
    if "_id" in run_reference.json:
        run: Run = Run.load(run_reference)
    else:
        run: Run = Run(name = args.run_name)
    if run == None:  # mistyped id
        raise ValueError("_id not in db")
    samples: List[Sample] = []
    for sample_reference in run.samples:
        samples.append(Sample.load(sample_reference))
    samples_in_run = set([i['name'] for i in samples])  # BeONE: avoid duplicate samples per run
    # check if the run has an id and whether it exists in the db
    client = pymongo.MongoClient(os.environ["BIFROST_DB_KEY"])
    db = client.get_database()
    runs = db.runs
    run_name_matches = [str(i["_id"]) for i in runs.find({"name": run['name']})]
    if "_id" not in run.json:  # and len(run_name_matches) < 1  # Check here is to ensure run isn't in DB, might wanna check if name exists
        logging.info(f"Adding sample to new Run {run['name']}")
        run, samples = initialize_run(run = run,
                                      samples = samples,
                                      component = args.component,
                                      input_folder = args.reads_folder,
                                      run_metadata = args.run_metadata,
                                      run_type = args.run_type,
                                      rename_column_file = args.run_metadata_column_remap,
                                      component_subset = args.component_subset)
    elif args.sample_subset != None:  # TODO: this doesnt look to be finished; source of code: bifrost_whats_my_species
        logging.info(f"Reprocessing samples from Run {run['name']}")  # we only want to subset samples from a pre-existing run
        if args.sample_subset != None:
            sample_subset = set(args.sample_subset.split(","))
            sample_inds_to_keep = []
            if len(samples) >= 1:
                sample_names_orig = set([i['categories']['sample_info']['summary']['sample_name'] for i in samples])
                missentered_subset_samples = ",".join([str(i) for i in (sample_subset - sample_names_orig)])
                if len(missentered_subset_samples) > 0:
                    logging.warning(f"{missentered_subset_samples} not present in run")
                for i, sample in enumerate(samples):
                    if sample['categories']['sample_info']['summary']['sample_name'] in sample_subset:
                        sample_inds_to_keep.append(i)
            samples = [samples[i] for i in sample_inds_to_keep]
        return
    elif not {f"{args.run_name}___{beone_sample['sample']['summary']['sample']}"}.issubset(samples_in_run):  # BeONE: using initialize_run with preexisting DB just adds the sample to it
        logging.info(f"Adding sample to preexisting Run {run['name']}")
        run, samples = initialize_run(run = run,
                                      samples = samples,
                                      component = args.component,
                                      input_folder = args.reads_folder,
                                      run_metadata = args.run_metadata,
                                      run_type = args.run_type,
                                      rename_column_file = args.run_metadata_column_remap,
                                      component_subset = args.component_subset)
    else:
        print(f'The sample "{beone_sample["sample"]["summary"]["sample"]}" is already in the run "{args.run_name}". No actions have been taken')
        logging.warning(f'The sample "{beone_sample["sample"]["summary"]["sample"]}" is already in the run "{args.run_name}". No actions have been taken')
        with open(snakemake.output['json_run'], "w") as fh:
            fh.write('')
        with open(snakemake.output['json_sample'], "w") as fh:
            for sample in samples:
                fh.write('')
        sys.exit()
    logging.info(f'The Sample "{beone_sample["sample"]["summary"]["sample"]}" was created and linked to Run "{args.run_name}"')


def prepare_args(run_name: str = '') -> object:
    # derive run from json or use a constant BeONE run with many samples
    if run_name == '':
        if beone_metadata_pre_assembly['pipeline_metadata']['config']['params'].get('run_name', '') == '':
            run_name = str(int(time.mktime(datetime.datetime.strptime(beone_metadata_pre_assembly['timestamp'], "%Y-%m-%d %H:%M:%S.%f").timetuple())))
        else:
            run_name = beone_metadata_pre_assembly['pipeline_metadata']['config']['params']['run_name']
        snakemake.params['run_name'] = run_name

    # check if previous run with same run_name is present in database, if so get first $oid
    client = pymongo.MongoClient(os.environ["BIFROST_DB_KEY"])
    db = client.get_database()
    runs = db.runs
    run_name_list = [str(i["_id"]) for i in runs.find({"name": run_name})]
    run_id = next(iter(run_name_list), None)

    # replacement for commandline argument parsing
    args = argparse.Namespace(
        run_id = run_id,
        run_name = run_name,
        component = COMPONENT,
        run_metadata = None,
        reads_folder = os.path.dirname(beone_metadata_pre_assembly['sample_description'].get('read1_file')),
        run_type = "json_import",
        run_metadata_column_remap = None,
        component_subset = beone_metadata_pre_assembly['pipeline'],  # beone: i guess, that still has to be developed
        sample_subset = None  # beone: for testing 'Se-Germany-BfR-0002' - i guess, that still has to be developed
    )
    return (args)


# %% Init SampleComponent - code from whats_my_species pipeline.smk ###########

def initialise_SampleComponent():
    global samplecomponent
    client = pymongo.MongoClient(os.environ["BIFROST_DB_KEY"])
    db = client.get_database()
    samples = db.samples
    config = {
        'sample_id'     : [str(i['_id']) for i in samples.find({"display_name": beone_sample['sample']['summary']['sample']})][0],
        'sample_name'   : [str(i['name']) for i in samples.find({"display_name": beone_sample['sample']['summary']['sample']})][0],
        'component_name': COMPONENT['name']
    }

    try:
        sample_ref = SampleReference(_id = config.get('sample_id', None), name = config.get('sample_name', None))
        sample: Sample = Sample.load(sample_ref)  # schema 2.1
        if sample is None:
            raise Exception("invalid sample passed")
        component_ref = ComponentReference(name = config['component_name'])
        component: Component = Component.load(reference = component_ref)  # schema 2.1
        if component is None:
            raise Exception("invalid component passed")
        samplecomponent_ref = SampleComponentReference(name = SampleComponentReference.name_generator(sample.to_reference(), component.to_reference()))
        samplecomponent = SampleComponent.load(samplecomponent_ref)
        if samplecomponent is None:
            samplecomponent = SampleComponent(sample_reference = sample.to_reference(), component_reference = component.to_reference())  # schema 2.1
        common.set_status_and_save(sample, samplecomponent, "Running")
    except Exception as error:
        logging.error(f"Failed to set sample, component and/or samplecomponent")
        print(traceback.format_exc(), file = snakemake.log['log'])
        raise Exception("failed to set sample, component and/or samplecomponent")

    samplecomponent['path'] = os.path.join(snakemake.params['component_path'], component['name'])
    samplecomponent.save()


# %% Dump JSON - code from whats_my_species datadump.py #######################

def datadump(samplecomponent_ref_json: Dict):
    samplecomponent_ref = SampleComponentReference(value = samplecomponent_ref_json)
    samplecomponent = SampleComponent.load(samplecomponent_ref)
    sample = Sample.load(samplecomponent.sample)

    beone = samplecomponent.get_category("beone")
    if beone is None:  # TODO: when is it None?
        beone = Category(
            value = {"name"     : "beone",
                     "component": {"id": samplecomponent["component"]["_id"], "name": samplecomponent["component"]["name"]},
                     "summary"  : {},
                     "report"   : {}
                     }
        )

    contigs = samplecomponent.get_category("contigs")
    if contigs is None:
        contigs = Category(
            value = {"name"     : "contigs",
                     "component": {"id": samplecomponent["component"]["_id"], "name": samplecomponent["component"]["name"]},
                     "summary"  : {},
                     "report"   : {}
                     }
        )

    cgmlst = samplecomponent.get_category("cgmlst")
    if cgmlst is None:
        cgmlst = Category(
            value = {"name"     : "cgmlst",
                     "component": {"id": samplecomponent["component"]["_id"], "name": samplecomponent["component"]["name"]},
                     "summary"  : {},
                     "report"   : {}
                     }
        )

    if "status" not in samplecomponent["results"]:  # TODO: where does the status come from and why is samplecomponent["results"] filled?
        beone["summary"] = beone_sample['sample']
        beone["report"] = beone_sample['pipelines']

    contigs["summary"]["data"] = beone_metadata_post_assembly['sample_description']['fasta_file']
    contigs["summary"]["fasta_md5"] = beone_metadata_post_assembly['sample_description']['fasta_md5']

    cgmlst["summary"] = {"loci_found"               : beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('loci_found', None),
                         "loci_missing"             : beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('loci_missing', None),
                         "loci_total"               : beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('loci_total', None),
                         "loci_missing_fraction"    : beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('loci_missing_fraction', None),
                         "hashid"                   : beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('hashid', None),
                         "max_fraction_missing_loci": beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('max_fraction_missing_loci', None),
                         "allele_qc"                : beone_chewiesnake['chewiesnake'].get('allele_stats', {}).pop('allele_qc', None)}
    cgmlst["report"] = {'chewiesnake': beone_chewiesnake['chewiesnake']}

    samplecomponent.set_category(beone)
    samplecomponent.set_category(contigs)
    samplecomponent.set_category(cgmlst)
    sample.set_category(beone)
    sample.set_category(contigs)
    sample.set_category(cgmlst)
    samplecomponent.save_files()
    common.set_status_and_save(sample, samplecomponent, "Success")
    logging.info(f'The JSON of sample "{beone_sample["sample"]["summary"]["sample"]}" was imported into a "{snakemake.params["run_name"]}" SampleComponent')


# %% Main Function ############################################################

def main():
    clear_collections_switch = False
    verbose_switch = False
    debug_switch = False

    global snakemake
    global COMPONENT
    global samplecomponent
    global beone_sample
    global beone_metadata_pre_assembly
    global beone_metadata_post_assembly
    global beone_chewiesnake
    global run_name

    # Parse Arguments
    python_or_snakemake(debug = debug_switch)

    # Configure Logging
    logging.basicConfig(filename = snakemake.log['log'],
                        encoding='utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')
    logging.info("json_import.py is runnning in DEBUG mode") if debug_switch else None

    # Check DB Connection
    uri_target_input = json.loads(s = snakemake.params['db_key'])
    os.environ["BIFROST_DB_KEY"] = uri_target_input.get("uristring", uri_target_input)
    if not datahandling.has_a_database_connection():
        logging.error("No connection to the database could be established")
        print("No connection to the database could be established")
        sys.exit(1)

    # Workflow
    clear_collections(uri = os.environ["BIFROST_DB_KEY"]) if clear_collections_switch else None
    import_sample()
    initialize_component()
    pprint.pprint(COMPONENT.json) if verbose_switch else None
    run_pipeline()
    initialise_SampleComponent()
    datadump(samplecomponent_ref_json = samplecomponent.to_reference().json)
    logging.info('MongoDB JSON import for ' + snakemake.params['sample'] + ' finished')
    print('MongoDB JSON import for ' + snakemake.params['sample'] + ' finished')


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
