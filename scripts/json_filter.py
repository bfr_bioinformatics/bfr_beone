#!/usr/bin/env python3

print("""
json_filter.py - a script from the BfR Genome Surveillance pipelines:
https://gitlab.com/bfr_bioinformatics/bfr_beone.git
""")

# %% Packages #################################################################

# Python Standard Packages
import sys
import os
import traceback
import logging
import argparse
import json
from pprint import pprint

# ScriptInfo
from pathlib import Path
from datetime import datetime

# Other Packages
import cerberus
import pandas

from helper_functions import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(msg = ''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Script Info ##############################################################

class ScriptInfo:
    repo_path: Path
    script_name: str
    timestamp: datetime
    debug_switch: bool

    def __init__(self, debug_switch: bool = False):
        self.repo_path = Path(__file__).resolve().parent.parent
        self.script_name = Path(__file__).name
        self.timestamp = datetime.now()
        self.debug_switch = debug_switch


global script_info
global snakemake


# %% Define Source of Arguments ###############################################

def argument_parser() -> argparse.ArgumentParser:
    global script_info
    parser = argparse.ArgumentParser()

    # input
    parser.add_argument('-i', '--json_in', type = Path, required = True, help = 'Path to the JSON file of combined BfR pipeline results, e.g. sample.merge.json')
    # output
    parser.add_argument('-o', '--json_out', type = Path, required = True, help = 'Path to the JSON file of filtered results in the BeONE format, e.g. sample.beone.json')
    # params
    parser.add_argument('-s', '--sample', required = True, help = 'String of the Sample name')
    parser.add_argument('-v', '--validation', type = Path, required = True, help = f"Path to the BeONE JSON schema for Validation; default: {script_info.repo_path / 'resources' / 'BfR-GenSurv_schema_v20220619_full.json'}")
    parser.add_argument('-f', '--filter', type = Path, required = True, help = f"Path to the BeONE JSON schema for Filtering; default: {script_info.repo_path / 'resources' / 'BeONE_schema_filter_v20220619.json'}")
    # logfile
    parser.add_argument('-l', '--log', type = Path, required = True, help = 'Path for the logfile')

    return parser


# Create Arguments for Debugging
def create_arguments() -> argparse.Namespace:
    global script_info
    debug_workdir = script_info.repo_path / "test_data_workshop"  # 'test_data' 'test_data_beone' 'test_data_workshop'
    debug_sample = 'Se-BfR-0001'  # 'GMI-17-001-DNA' 'Se-Germany-BfR-0001' 'Se-BfR-0001' 'SRR498433'

    _args = argparse.Namespace(
        json_in = debug_workdir / '5_merge' / f'{debug_sample}.merge.json',
        json_out = debug_workdir / '6_filtered' / f'{debug_sample}.beone.json',
        sample = debug_sample,
        validation = script_info.repo_path / 'resources' / 'BfR-GenSurv_schema_v20220619_full.json',
        filter = script_info.repo_path / 'resources' / 'BeONE_schema_filter_v20220619.json',
        log = debug_workdir / '0_logs' / f'{debug_sample}_filterJson_debug.log'
    )
    return _args


# Translate ArgParse to Snakemake Object
def arguments_to_snakemake(args):
    snakemake_input_df = pandas.DataFrame(
        data = [['json_in', args.json_in]],
        columns = ['index', 'input'])
    snakemake_output_df = pandas.DataFrame(
        data = [['json_out', args.json_out]],
        columns = ['index', 'output'])
    snakemake_params_df = pandas.DataFrame(
        data = [['sample', args.sample],
                ['validation', args.validation],
                ['filter', args.filter]],
        columns = ['index', 'params'])
    snakemake_log_df = pandas.DataFrame(
        data = [['log', args.log]],
        columns = ['index', 'log'])
    snakemake_df = snakemake_input_df.merge(snakemake_output_df.merge(snakemake_params_df.merge(snakemake_log_df, how = 'outer'), how = 'outer'), how = 'outer')
    _snakemake = snakemake_df.set_index(snakemake_df['index'])
    return _snakemake


# Define Source of Arguments
def python_or_snakemake_args():
    global script_info
    try:
        snakemake.input[0]
    except NameError:
        if script_info.debug_switch:
            _snakemake = arguments_to_snakemake(args = create_arguments())
        else:
            parser = argument_parser()
            _snakemake = arguments_to_snakemake(args = parser.parse_args())
        return _snakemake


# %% Import Data ##############################################################


# %% Validate and Filter Sample ###############################################

def validate_sample(json_sample, json_schema_validation):
    global snakemake

    # Cerberus: Create Validator - Convert custom JSON schema to Cerberus schema
    cerberus_schema_validation = translateSchema_json2cerberus(schemaObj = json_schema_validation)['Root']['schema']
    cerberus_schema_validation_withDefault = translateSchema_json2cerberus(schemaObj = json_schema_validation, defaultValues = defaultValues_dict)['Root']['schema']

    # Create Validator objects
    validator_schema_validation = cerberus.Validator(cerberus_schema_validation, ignore_none_values = True, purge_unknown = True)
    validator_schema_validation_withDefaults = cerberus.Validator(cerberus_schema_validation_withDefault, ignore_none_values = True, purge_unknown = True)

    # Cerberus: Normalize with Validators
    json_sample_validated = validator_schema_validation.validate(json_sample)  # BEWARE of URL quoting
    json_sample_empty = validator_schema_validation_withDefaults.normalized({'sample': {}})

    if json_sample_validated:
        logging.info(f'JSON Schema Validation of {snakemake.params["sample"]}: Passed.')
    else:
        logging.warning(f'JSON Schema Validation of {snakemake.params["sample"]}: FAILED.')
        print(f'ERROR: JSON Schema Validation of {snakemake.params["sample"]}: FAILED.')
        with open(snakemake.output['json_out'].replace('.json', '_validationErrors.json'), 'w') as file:
            json.dump(validator_schema_validation.errors,
                      fp = file,
                      indent = 4,
                      sort_keys = False,
                      ensure_ascii = False,
                      allow_nan = False)


def filter_sample(json_sample, json_schema_filter):
    global snakemake

    # Cerberus: Create Validator - Convert custom JSON schema to Cerberus schema
    cerberus_schema_filter = translateSchema_json2cerberus(schemaObj = json_schema_filter)['Root']['schema']

    # Create Validator objects
    validator_schema_filter = cerberus.Validator(cerberus_schema_filter, ignore_none_values = True, purge_unknown = True)

    # Cerberus: Normalize with Validators
    json_sample_filtered = validator_schema_filter.normalized(json_sample)  # BEWARE of URL quoting

    if json_sample_filtered == json_sample:
        logging.info(f'JSON Schema Normalization of {snakemake.params["sample"]}: No changes.')
    else:
        logging.warning(f'Schema Normalization of {snakemake.params["sample"]}: Filtered.')

    return json_sample_filtered


# %% Main #####################################################################

def main():
    # %% Setup #####

    # Script Information
    global script_info
    global snakemake
    script_info = ScriptInfo(debug_switch = False)

    # Parse Arguments
    snakemake = python_or_snakemake_args()

    # Configure Logging
    logging.basicConfig(filename = snakemake.log['log'],
                        encoding = 'utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')

    # %% Workflow #####

    logging.info(f"[START] {__file__}")
    logging.warning(f"{script_info.script_name} is running in DEBUG mode") if script_info.debug_switch else None
    logging.info(msg = snakemake) if script_info.debug_switch else None

    # Import JSON
    with open(snakemake.input['json_in'], 'r') as file:
        json_sample = json.load(fp = file)  # without arg object_pairs_hook=unquote_hook to mimic ExtendsClasses schema

    # Import Schemas
    with open(snakemake.params['validation'], 'r') as file:
        json_schema_validation = json.load(fp = file)

    with open(snakemake.params['filter'], 'r') as file:
        json_schema_filter = json.load(fp = file)

    # Validate JSON
    validate_sample(json_sample = json_sample, json_schema_validation = json_schema_validation)

    # Filter JSON
    json_sample_filtered = filter_sample(json_sample = json_sample, json_schema_filter = json_schema_filter)

    # %% Write Filtered JSON structure #####

    if json_sample_filtered:
        with open(snakemake.output['json_out'], 'w') as file:
            json.dump(json_sample_filtered,
                      fp = file,
                      indent = 4,
                      sort_keys = False,
                      ensure_ascii = False,
                      allow_nan = False)
    else:
        sys.exit(1)

    logging.info(f'JSON Schema procedures for {snakemake.params["sample"]} finished')
    logging.info(f"[STOP] {__file__}")


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
