#!/usr/bin/env python3

'''
json_xcrypt.py - a script from the BfR Genome Surveillance pipelines:
https://gitlab.com/bfr_bioinformatics/bfr_beone.git
PREREQUISITE: environment variable SECRET has to be set
'''

# %% Packages #################################################################

import sys
import logging
import traceback
import argparse

import pandas
import hashlib
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from base64 import b64encode, b64decode

from helper_functions import *
from json_keychain import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Define Source of Arguments ###############################################

def parse_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    parser = argparse.ArgumentParser()

    # input
    parser.add_argument('--json_in', '-i', help = 'Path to the input JSON file in the BeONE format, e.g. (A) sample.beone.json or (B) sample.encrypted.json',
                        type = os.path.abspath, required = True)
    # output
    parser.add_argument('--json_out', '-o', help = 'Path to the output JSON file in the BeONE format, e.g. (A) sample.encrypted.json or (B) sample.decrypted.json',
                        type = os.path.abspath, required = True)
    # params
    parser.add_argument('--mode', '-m', help = 'String of the cryptographic mode, either (A) "encrypt" or (B) "decrypt"',
                        required = True)
    parser.add_argument('--keychain', '-k', help = 'Path to the BeONE keychain of AES keys; will be generated if none is provided for encryption',
                        type = os.path.abspath, required = False)
    # log
    parser.add_argument('--log', '-l', help = 'Path for the logfile',
                        type = os.path.abspath, required = True)

    return parser.parse_args()


# Create Arguments for Debugging
def create_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    debug_sample = 'Se-BfR-0001'  # 'GMI-17-001-DNA' 'Se-Germany-BfR-0001' 'Se-BfR-0001'
    debug_mode = 'decrypt'  # 'encrypt' or 'decrypt'
    debug_workdir = os.path.join(repo_dir, "test_data_workshop")  # 'test_data' 'test_data_beone' 'test_data_workshop'

    args = argparse.Namespace(
        json_in = os.path.join(debug_workdir, '6_filtered', debug_sample + '.beone.json'),
        json_out = os.path.join(debug_workdir, '9_crypto', debug_sample + '.beone.' + debug_mode + 'ed.json'),
        mode = debug_mode,
        keychain = os.path.join(debug_workdir, '9_crypto', 'keychain.json'),
        log = os.path.join(debug_workdir, '0_logs', debug_sample + '_' + debug_mode + 'Json_debug.log')
    )
    return args


# Translate ArgParse to Snakemake Object
def arguments_to_snakemake(args):
    global snakemake
    snakemake_input_df = pandas.DataFrame(
        data = [['json_in', args.json_in]],
        columns = ['index', 'input'])
    snakemake_output_df = pandas.DataFrame(
        data = [['json_out', args.json_out]],
        columns = ['index', 'output'])
    snakemake_params_df = pandas.DataFrame(
        data = [['mode', args.mode],
                ['keychain', args.keychain]],
        columns = ['index', 'params'])
    snakemake_log_df = pandas.DataFrame(
        data = [['log', args.log]],
        columns = ['index', 'log'])
    snakemake_df = snakemake_input_df.merge(snakemake_output_df.merge(snakemake_params_df.merge(snakemake_log_df, how = 'outer'), how = 'outer'), how = 'outer')
    snakemake = snakemake_df.set_index(snakemake_df['index'])


# Define Source of Arguments
def python_or_snakemake(debug: bool):
    try:
        snakemake.input[0]
    except NameError:
        if debug:
            arguments_to_snakemake(args = create_arguments())
        else:
            arguments_to_snakemake(args = parse_arguments())


# %% Crypto Functions #########################################################

def encrypt(plain_text, password, salt: str = ''):
    # generate a random salt if none is provided
    if salt is None:
        nacl = get_random_bytes(AES.block_size)
    else:
        nacl = b64decode(salt)

    # use the scrypt key derivation function to get a private key from the password
    private_key = hashlib.scrypt(password.encode(), salt = nacl, n = 2 ** 14, r = 8, p = 1, dklen = 32)

    # create cipher config
    cipher_config = AES.new(private_key, AES.MODE_GCM)

    # return a dictionary with the encrypted text
    ciphertext, tag = cipher_config.encrypt_and_digest(bytes(plain_text, 'utf-8'))

    enc_dict = {'ciphertext': b64encode(ciphertext).decode('utf-8'),
                'nonce'     : b64encode(cipher_config.nonce).decode('utf-8'),
                'tag'       : b64encode(tag).decode('utf-8')}

    if salt is None:
        enc_dict['salt'] = b64encode(nacl).decode('utf-8')

    return enc_dict


def decrypt(enc_dict, password, salt):
    # decode the dictionary entries from base64
    nacl = b64decode(salt)
    ciphertext = b64decode(enc_dict['ciphertext'])
    nonce = b64decode(enc_dict['nonce'])
    tag = b64decode(enc_dict['tag'])

    # generate the private key from the password and salt
    private_key = hashlib.scrypt(password.encode(), salt = nacl, n = 2 ** 14, r = 8, p = 1, dklen = 32)

    # create the cipher config
    cipher = AES.new(private_key, AES.MODE_GCM, nonce = nonce)

    # decrypt the cipher text
    decrypted = cipher.decrypt_and_verify(ciphertext, tag)

    return decrypted


# %% Metadata Class Crypto Functions ##########################################

class dcf_term(dict):
    _critical_keys = ['parentCode', 'termCode', 'termName']
    global keychain

    def __init__(self, dictionary: dict = None):
        if isinstance(dictionary.get('termCode', ''), dict):
            self.status_encryption = True
        else:
            self.status_encryption = False
        if dictionary.get('class') == 'EFSA_DCF_TERM':
            self.data = {**dictionary}
            self.nacl = keychain.get(self.data.get('dcf'), {}).get(str(self.data.get('hierarchyLevel')), None)
            self.pwd = keychain.get('password', None)
        else:
            raise ValueError('The dictionary does not adhere to the standard DCF Term structure.')

    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ('{}={!r}'.format(k, self.__dict__[k]) for k in keys)
        return '{}({})'.format(type(self).__name__, ', '.join(items))

    def encrypt(self, password: str = os.getenv('BEONE_SECRET'), salt: str = os.getenv('BEONE_SALT')):
        if self.status_encryption is False:
            nacl_string = 'with no salt' if salt is None else 'via static environment salt'
            nacl_string = 'via term-specific keychain salt' if self.nacl is not None else nacl_string
            for key in self._critical_keys:
                self.data[key] = encrypt(self.data.get(key), password = self.pwd or password or '', salt = self.nacl or salt or '')
            self.status_encryption = True
            logging.info(f"Critical {self.data.get('dcf')} values are now AES256 encrypted {nacl_string}.")
        else:
            logging.warning(f"Critical {self.data.get('dcf')} values are already ciphertext.")
        return self

    def decrypt(self, password: str = os.getenv('BEONE_SECRET'), salt: str = os.getenv('BEONE_SALT')):
        if self.status_encryption is True:
            nacl_string = 'with no salt' if salt is None else 'via static environment salt'
            nacl_string = 'via term-specific keychain salt' if self.nacl is not None else nacl_string
            for key in self._critical_keys:
                self.data[key] = bytes.decode(decrypt(self.data.get(key), password = self.pwd or password or '', salt = self.nacl or salt or ''))
            self.status_encryption = False
            logging.info(f"Critical {self.data.get('dcf')} values are now decrypted {nacl_string}.")
        else:
            logging.warning(f"WARNING: Critical {self.data.get('dcf')} values are already cleartext.")
        return self


def encrypt_dcf_list(sample_obj: dict, key: str):
    dcf_dict_list = sample_obj['sample']['metadata'].get(key)
    # if not isinstance(dcf_dict_list[0], dict):  # TODO: remove list check, once the R metadata_sheet_parser is fixed - DONE
    #     dcf_dict_list = dcf_dict_list[0]
    if dcf_dict_list is None:
        return None  # TODO: remove list wrap, once the R metadata_sheet_parser is fixed - DONE
    dcf_obj_list = [dcf_term(dictionary = item) for item in dcf_dict_list]
    dcf_obj_encr = [item.encrypt() for item in dcf_obj_list]
    dcf_dict_encr = [item.data for item in dcf_obj_encr]
    return dcf_dict_encr


def decrypt_dcf_list(sample_obj: dict, key: str):
    dcf_dict_list = sample_obj['sample']['metadata'].get(key)
    # if not isinstance(dcf_dict_list[0], dict):  # TODO: remove list check, once the R metadata_sheet_parser is fixed - DONE
    #     dcf_dict_list = dcf_dict_list[0]
    if dcf_dict_list is None:
        return None  # TODO: remove list wrap, once the R metadata_sheet_parser is fixed - DONE
    dcf_obj_list = [dcf_term(dictionary = item) for item in dcf_dict_list]
    dcf_obj_decr = [item.decrypt() for item in dcf_obj_list]
    dcf_dict_encr = [item.data for item in dcf_obj_decr]
    return dcf_dict_encr


# %% Generate or Load DCF Term-specific Keychain ##############################

def get_keychain(path):
    global keychain
    try:
        keychain = load_keychain(keyfile = path)
    except:
        generate_keychain(keyfile = path, overwrite = False)
        keychain = load_keychain(keyfile = path)


# %% Perform Cryptographic Function

def xcrypt_sample(sample, mode):
    # Define Cryptographic Focus
    dcf_term_sections = ['Country', 'Region', 'Source_Type', 'Source_Detail', 'Food_Exposures', 'Travel_Destinations']  # Epidemiological Fields encoded by DCF Terms

    # Perform Cryptographic Function
    if mode == 'encrypt':
        for key in dcf_term_sections:
            sample['sample']['metadata'][key] = encrypt_dcf_list(sample_obj = sample, key = key)
    if mode == 'decrypt':
        for key in dcf_term_sections:
            decrypted_obj = decrypt_dcf_list(sample_obj = sample, key = key)
            if decrypted_obj == None:  # TODO: remove list wrap, once the R metadata_sheet_parser is fixed - DONE
                sample['sample']['metadata'][key] = decrypted_obj
            else:
                sample['sample']['metadata'][key] = [decrypted_obj]


# %% Main Function ############################################################

def main():
    debug_switch = False

    global snakemake
    global keychain

    # Parse Arguments
    python_or_snakemake(debug = debug_switch)

    # Configure Logging
    logging.basicConfig(filename = snakemake.log['log'],
                        encoding='utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')
    logging.info("json_xcrypt.py is runnning in DEBUG mode") if debug_switch else None

    # Import Sample
    with open(snakemake.input['json_in'], 'r') as file:
        beone_sample = json.load(fp = file, object_pairs_hook = unquote_hook)
    get_keychain(path = snakemake.params['keychain'])

    # En-/Decode Sample Metadata
    xcrypt_sample(sample = beone_sample, mode = snakemake.params['mode'])

    # Write to URLencoded JSON
    with open(snakemake.output['json_out'], 'w') as file:
        json.dump(
            obj = strictJsonObj(
                nestedObj = json.loads(
                    s = nestedDict_to_json(
                        nestedObj = beone_sample)),
                uriCompliant = True),
            fp = file,
            indent = 4,
            sort_keys = False,
            ensure_ascii = False,
            allow_nan = False)  # Improvement: Use .encode('utf-8') in combo with ensure_ascii=False if None values are omitted instead of coerced to None

    logging.info(f"JSON {snakemake.params['mode']}ion for {beone_sample['sample']['summary']['sample']} finished.")
    print(f"JSON {snakemake.params['mode']}ion for {beone_sample['sample']['summary']['sample']} finished.")


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
