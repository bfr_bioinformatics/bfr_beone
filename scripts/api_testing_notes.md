# Docker MongoDB on de.NBI Cluster

Note: docker was installed via `snap` before cloud crash in May 2022, after that via `apt`
UPDATE: this manual is outdated since the reinstall, no more docker volumes but mapped directory for mongodb

```shell
systemctl status snap.docker.dockerd.service
sudo groupadd docker-snap
sudo usermod -aG docker-snap $USER
newgrp docker-snap
```

Prerequisite:
1) ownership of /var/run/docker.sock is root:docker-snap
2) /snap/bin is in $PATH

```shell
docker info
docker pull mongo
docker volume create bifrostdata
```

Use `docker volume prune` to remove unused volumes;
**BEWARE:** this targets non-running mongo instances, too.

```shell
docker volume inspect bifrostdata
```

```text
[
    {
        "CreatedAt": "2022-02-24T15:33:08Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/snap/docker/common/var-lib-docker/volumes/bifrostdata/_data",
        "Name": "bifrostdata",
        "Options": {},
        "Scope": "local"
    }
]
```

```shell
docker run -p 27017:27017 -v bifrostdata:/data/db --name bifrost -d mongo
```

```shell
docker exec bifrost sh -c 'mongo --version'
```

```text
MongoDB shell version v5.0.6
Build Info: {
    "version": "5.0.6",
    "gitVersion": "212a8dbb47f07427dae194a9c75baec1d81d9259",
    "openSSLVersion": "OpenSSL 1.1.1f  31 Mar 2020",
    "modules": [],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "ubuntu2004",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}
```

## docker-compose.yaml

```yaml
version: "3.9"

services:
  mongo:
    image: mongo:latest
    container_name: bifrost
    ports:
      - 27017:27017
    volumes:
      - bifrostdata:/data/db
    restart: unless-stopped

volumes:
  bifrostdata:
    external: true
    name: bifrostdata
```

start
```shell
$ docker-compose up -d
Creating network "docker_mongo_db_default" with the default driver
Creating bifrost ... done
```

stop
```shell
$ docker-compose down
Stopping bifrost ... done
Removing bifrost ... done
Removing network docker_mongo_db_default
```

setup mongodb structure
```shell
$ docker exec -it bifrost bash
root@89f6724ce757:/# mongo
MongoDB shell version v5.0.6
connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("70244c9d-6f31-45ee-b8a4-b2efd6178a3e") }
MongoDB server version: 5.0.6
...
```

create by hand
```mongo
show dbs
use bifrost_denbi  # creates db if non-existent
show dbs
db
db.createCollection("dummy")
show collections
show dbs
exit
```

# Singularity MongoDB on BfR HPC

```shell
~/data/git/mongo/start_mongo.sh
singularity instance list
ps aux | grep mongod
singularity shell instance://mongo
# Singularity>
mongo mongodb://127.0.0.1:26016/bifrost_bfr
show dbs
exit
```
