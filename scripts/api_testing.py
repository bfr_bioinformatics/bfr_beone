#!/usr/bin/env python3

'''
Testing of other beoneapi functions
Make sure mongod is up and running and environment variable BIFROST_DB_KEY is set to a TEST database.
$ sudo systemctl restart mongod
'''

# %% Packages #################################################################

import os
import sys
import logging
import traceback
import argparse
import glob
from datetime import datetime
from functools import reduce
import pprint

import bson
import pymongo
import pandas
import yaml
from paramiko import SSHConfig
import beoneapi
from helper_functions import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Define Source of Arguments ###############################################

def parse_arguments():
    parser = argparse.ArgumentParser()

    # params, a pair of these is required
    parser.add_argument('--db_key_source', '-a', help = 'String of the Source MongoDB database to copy FROM',
                        required = False)
    parser.add_argument('--db_key_target', '-b', help = 'String of the Target MongoDB database to copy TO',
                        required = False)
    parser.add_argument('--uri_file_source', '-u', help = 'Path to the Source MongoDB URI file to copy FROM',
                        type = os.path.abspath, required = False)
    parser.add_argument('--uri_file_target', '-v', help = 'Path to the Target MongoDB URI file to copy TO',
                        type = os.path.abspath, required = False)
    parser.add_argument('--ssh_yaml_source', '-y', help = 'Path to the Remote Source MongoDB SSH config yaml to copy FROM',
                        type = os.path.abspath, required = False)
    parser.add_argument('--ssh_yaml_target', '-z', help = 'Path to the Remote Target MongoDB SSH config yaml to copy TO',
                        type = os.path.abspath, required = False)
    # optional params
    parser.add_argument('--sample', '-s', help = 'String of the Sample name',
                        required = False)
    parser.add_argument('--run_name', '-r', help = 'String of the Run name',
                        required = False)
    parser.add_argument('--mode', '-m', help = 'String of the cryptographic mode, either (A) "encrypt" or (B) "decrypt"',
                        required = False)
    parser.add_argument('--keychain', '-k', help = 'Path to the BeONE keychain of AES keys; will be generated if none is provided for encryption',
                        type = os.path.abspath, required = False)
    # database
    parser.add_argument('--drop_collections', '-d', help = 'String of a MongoDB database. Drop all BeONE collections therein',
                        required = False)
    # log
    parser.add_argument('--log', '-l', help = 'Path for the logfile',
                        type = os.path.abspath, required = True)

    return parser.parse_args()


# Create Arguments for Debugging
def create_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    debug_sample = "Se-BfR-0001"  # "GMI15-001-DNA" "GMI-17-001-DNA" "Se-Germany-BfR-0001" "Se-BfR-0001"
    debug_runname = "BeONE"
    debug_workdir = os.path.join(repo_dir, "test_data_workshop")  # "test_data" "test_data_beone" "test_data_workshop"
    debug_drop_collections = 'target'  # '' "source" or "target"
    debug_mode = 'encrypt'  # 'encrypt' or 'decrypt'
    # debug_mode = 'decrypt'  # 'encrypt' or 'decrypt'

    args = argparse.Namespace(
        sample = debug_sample,
        run_name = debug_runname,
        db_key_source = 'mongodb://bfr:mypassword@127.0.0.1:28018/beone_bfr_private?authSource=beone_bfr_private',
        db_key_target = 'mongodb://bfr:mypassword@127.0.0.1:26016/beone_bfr_public?authSource=beone_bfr_public',
        # db_key_source = 'mongodb://bfr:mypassword@127.0.0.1:26016/beone_bfr_public?authSource=beone_bfr_public',
        # db_key_target = 'mongodb://bfr:mypassword@127.0.0.1:28018/beone_bfr_merge?authSource=beone_bfr_merge',
        # uri_file_source = os.path.join(repo_dir, "credentials", "looneyVenter.bfr.beone_bfr_private.uri"),
        # uri_file_target = os.path.join(repo_dir, "credentials", "schenki.bfr.beone_bfr_public.uri"),
        # ssh_yaml_source = os.path.join(os.path.expanduser('~'), '.ssh', 'looneyVenter.bfr.beone_bfr_private.yaml'),
        # ssh_yaml_target = os.path.join(os.path.expanduser('~'), '.ssh', 'timelessEuler.bfr.beone_bfr_public.yaml'),
        # json_out = os.path.join(debug_workdir, '10_transfer_encrypt', debug_sample + '.transfer.json'),
        drop_collections = debug_drop_collections,
        mode = debug_mode,
        keychain = os.path.join(debug_workdir, '9_crypto', 'keychain.json'),
        log = os.path.join(debug_workdir, '0_logs', debug_sample + '_transferJson_debug.log')
    )
    return args


# Translate ArgParse to Snakemake Object
def snakemake_to_arguments(snakemake):
    args = argparse.Namespace(
        sample = snakemake.params['sample'],
        run_name = snakemake.params['run_name'],
        db_key_source = snakemake.params['db_key_source'],
        db_key_target = snakemake.params['db_key_target'],
        # json_out = snakemake.params['json_out'],
        mode = snakemake.params['mode'],
        keychain = snakemake.params['keychain'],
        drop_collections = snakemake.params['drop_collections'],
        log = snakemake.log['log'],
    )
    return args


# Define Source of Arguments
def python_or_snakemake(debug: bool):
    global args
    try:
        snakemake.params[0]
    except NameError:
        if debug:
            args = create_arguments()
        else:
            args = parse_arguments()
    else:
        args = snakemake_to_arguments(snakemake = snakemake)


# %% Helper Functions #########################################################

def select_uri(args):
    uri_source = None
    uri_target = None

    if hasattr(args, "db_key_source") and hasattr(args, "db_key_target"):
        uri_source = args.db_key_source
        uri_target = args.db_key_target

    if hasattr(args, "uri_file_source") and hasattr(args, "uri_file_source"):
        try:
            with open(args.uri_file_source, 'r') as file:
                uri_source = file.readline().rstrip('\n')
            with open(args.uri_file_source, 'r') as file:
                uri_target = file.readline().rstrip('\n')
        except FileNotFoundError:
            logging.error('The MongoDB credentials could not be loaded from the URI file path')

    if hasattr(args, "ssh_yaml_source") and hasattr(args, "ssh_yaml_target"):
        try:
            with open(args.ssh_yaml_source, 'r') as file:
                uri_source = yaml.safe_load(file)
            with open(args.ssh_yaml_target, 'r') as file:
                uri_target = yaml.safe_load(file)
        except FileNotFoundError:
            logging.error('The MongoDB credentials could not be loaded from the YAML file path')

    if not uri_source or not uri_target:
        logging.error('No argument provided a parseable URI key')
        print('No argument provided a parseable URI key')
        sys.exit(1)

    return uri_source, uri_target


def has_a_database_connection(connection_name: str = 'default') -> bool:
    """Checks if you have a database connection

    Returns:
        bool: True - has a connection, False - No connection established
    """
    try:
        connection = beoneapi.get_connection(connection_name = connection_name)
        logging.info(f'Connection to MongoDB "{connection_name}" established. DB version is {connection.server_info().get("version", "")}.')
        return True
    except:
        message = """
        For SSH tunneling, conversion of the private key into a format that is recognized by paramiko might help:
        `cp id.pem id.pem.bak && ssh-keygen -p -m pem -f id.pem` 
        """
        print(message)
        print(traceback.format_exc())
        return False


def test_database(connection_name: str = 'default'):
    print("##############################")
    if has_a_database_connection(connection_name = connection_name):
        connection = beoneapi.get_connection(connection_name = connection_name)
        db = connection.get_database()
        print("Connection:  " + connection_name)
        print("Collections: " + json.dumps(db.list_collection_names()))
    else:
        logging.error(f'Cannot connect to "{connection_name}" database.')


def clear_collections(uri):
    """Perform db.collection.drop() on all BeONE collections

    :param uri: URI of the MongoDB to clear from the BeONE collections (run_components, runs, sample_components, samples, components)

    Returns:
        System Exit
    """
    logging.info(f"Clearing all BeOne collections from {uri} and exiting")
    print(f"Clearing all BeOne collections from '{os.path.basename(uri).split('?')[0]}' and exiting")
    client = pymongo.MongoClient(uri)
    db = client.get_database()
    db.drop_collection("run_components")
    db.drop_collection("runs")
    db.drop_collection("sample_components")
    db.drop_collection("samples")
    db.drop_collection("components")
    print("Collections cleared")
    sys.exit()


def get_uri(credentials, host, db_substring):
    _uri = [value for key, value in credentials.items() if db_substring in key and host in key][0]
    return _uri


def read_openssh_config(host, config_file = None):
    """Parses user's OpenSSH config for per hostname configuration for
    hostname, user, port and identity file
    :param host: Hostname to lookup in config
    """
    _ssh_config_file = config_file if config_file else os.path.join(os.path.expanduser('~'), '.ssh', 'config')
    # Load ~/.ssh/config if it exists to pick up username
    # and host address if set
    if not os.path.isfile(_ssh_config_file):
        return
    ssh_config = SSHConfig()
    ssh_config.parse(open(_ssh_config_file))
    host_config = ssh_config.lookup(host)
    credentials = {'host': host_config['hostname'] if 'hostname' in host_config else host,
                   'port': int(host_config['port']) if 'port' in host_config else 22,
                   'user': host_config['user'] if 'user' in host_config else None,
                   'key' : host_config['identityfile'][0] if 'identityfile' in host_config else None}
    return credentials


# %% Database Functions #######################################################

def retrieve_cgmlst_profiles(connection_name: str,
                             schema_name: str = 'enterobase_senterica_cgmlst',
                             method: str = 'aggregation',
                             verbose_switch: bool = False):
    '''
    This section retrieves all samples from a database and retrieves a their allele
        profiles by searching for their _id's and retricting output by a projection.
    One may replace the search by a pymongo aggregation function with additional filters,
        e.g. same cgMLST schema, cgmlst_qc passed, etc. and reshape the aggregation output
        instead of the dictionary mapreduce.
    '''

    # TODO: Maybe add failsafe for samples without allele_profile

    # By find(Projection)
    if method == "projection":
        all_samples = pandas.DataFrame.from_dict([dict(item, **{'_id': str(item['_id'])}) for item in beoneapi.get_all_samples(connection_name = connection_name)]).assign(source = connection_name).set_index(keys = '_id')
        allele_projection = {'_id': False, 'display_name': True, 'categories': {'cgmlst': {'report': {'chewiesnake': {'allele_profile': True}}}}}
        allele_profiles_projection = beoneapi.get_samples(sample_id_list = [bson.ObjectId(oid = item) for item in all_samples.index.to_list()], projection = allele_projection, connection_name = connection_name)
        allele_profiles_projection_mapreduce = [pandas.DataFrame(item['categories']['cgmlst']['report']['chewiesnake']['allele_profile']).rename(columns = {'allele_crc32': item['display_name']}) for item in allele_profiles_projection]
        allele_profiles_projection_df = reduce(lambda x, y: pd.merge(x, y, on = 'locus', how = 'outer'), allele_profiles_projection_mapreduce)
        pprint.pprint(allele_profiles_projection_df) if verbose_switch else None
        return allele_profiles_projection_df

    # By custom aggregation function with filter for cgMLST==PASS and schema_name; sample_id_list filter is optional
    if method == "aggregation":
        allele_profiles_aggregation = beoneapi.get_allele_profiles(schema_name = schema_name, connection_name = connection_name)  # sample_id_list = [bson.ObjectId(oid = item) for item in all_samples_local.index.to_list()]
        allele_profiles_aggregation_mapreduce = [pandas.DataFrame(item['allele_profile']).rename(columns = {'allele_crc32': item['display_name']}) for item in allele_profiles_aggregation]
        allele_profiles_aggregation_df = reduce(lambda x, y: pd.merge(x, y, on = 'locus', how = 'outer'), allele_profiles_aggregation_mapreduce)
        allele_profiles_aggregation_meta = pandas.DataFrame([{k: v for k, v in d.items() if k != 'allele_profile'} for d in allele_profiles_aggregation])
        pprint.pprint(allele_profiles_aggregation_df) if verbose_switch else None
        pprint.pprint(allele_profiles_aggregation_meta) if verbose_switch else None
        return allele_profiles_aggregation_df, allele_profiles_aggregation_meta


# %% Main Function ############################################################

def main():
    verbose_switch = True
    debug_switch = True

    global args

    # Parse Arguments
    python_or_snakemake(debug = debug_switch)

    # Configure Logging
    logging.basicConfig(filename = args.log,
                        encoding = 'utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')
    logging.info("api_testing.py is runnning in DEBUG mode") if debug_switch else None

    # Add URI to API
    uri_source, uri_target = select_uri(args = args)
    beoneapi.add_URI(mongoURI = uri_source, connection_name = 'source')
    beoneapi.add_URI(mongoURI = uri_target, connection_name = 'target')
    pprint.pprint(beoneapi.CONNECTION_URIS) if verbose_switch else None

    # Check DB Connection
    if not has_a_database_connection(connection_name = "source"):
        logging.error("No connection to the SOURCE database could be established")
        print("No connection to the SOURCE database could be established")
        sys.exit(1)
    if not has_a_database_connection(connection_name = "target"):
        logging.error("No connection to the TARGET database could be established")
        print("No connection to the TARGET database could be established")
        sys.exit(1)
    test_database(connection_name = 'source') if verbose_switch else None
    test_database(connection_name = 'target') if verbose_switch else None

    # Clear Collections
    if args.drop_collections:
        clear_collections(uri = beoneapi.CONNECTION_URIS.get(args.drop_collections))

    # %% Query both Databases #################################################

    # Determine run_type for source and target
    if beoneapi.CONNECTION_URIS.get('source').endswith('private'):
        source_run_type = 'json_import'
    else:
        source_run_type = 'virtual'

    if beoneapi.CONNECTION_URIS.get('target').endswith('private'):
        target_run_type = 'json_import'
    else:
        target_run_type = 'virtual'

    # Show all runs and samples in source
    print("############################## source|get_run_list") if verbose_switch else None
    source_run_list = beoneapi.get_run_list(run_type = source_run_type, connection_name = 'source')
    source_run_list_df = pandas.json_normalize(source_run_list, record_path = 'samples', sep = '.')  # meta = 'name', meta_prefix = "run_" avoided, use cell split to recover run_name
    source_run_df = deepcopy(source_run_list_df)
    source_run_df[['run', 'name']] = source_run_df.name.str.split("___", expand = True)
    source_run_df = source_run_df[['run', 'name', '_id']]
    pprint.pprint(source_run_df) if verbose_switch else None

    # Fetch a run
    print("############################## source|get_run") if verbose_switch else None
    source_run = beoneapi.get_run(run_name = 'BeONE', connection_name = 'source')
    pprint.pprint(source_run) if verbose_switch else None

    # Get Sample ID from previously fetched run by sample name matching
    print("############################## source|find sample in run") if verbose_switch else None
    source_sample = [item for item in source_run['samples'] if item['name'].find(args.sample) != -1][0]
    pprint.pprint(source_sample) if verbose_switch else None

    # Get complete Sample Data via Sample ID
    print("############################## source|get_sample") if verbose_switch else None
    source_sample_data = beoneapi.get_sample(sample_id = source_sample['_id'], connection_name = 'source')
    pprint_head(source_sample_data) if verbose_switch else None

    # TODO: find non-intersecting samples and loop over them

    # Sanity check of sample json
    if source_sample_data['categories'].get('beone', None) is None:
        logging.error(f'The sample {source_sample_data["name"]} does not contain the category "beone"')
        print(f'The sample {source_sample_data["name"]} does not contain the category "beone"')

    if source_sample_data['categories'].get('cgmlst', {}).get('report', None) is None:
        logging.error(f'The sample {source_sample_data["name"]} does not contain the category "cgmlst"')
        print(f'The sample {source_sample_data["name"]} does not contain the category "cgmlst"')
    else:
        json_module_chewiesnake = source_sample_data['categories']['cgmlst']['report']
        json_module_chewiesnake['chewiesnake']['allele_stats'].update(source_sample_data['categories']['cgmlst']['summary'])

    # restructure sample json to beone data model
    print("############################## source|sample_json_restored") if verbose_switch else None
    json_assembly = {
        'sample'   : {
            **source_sample_data['categories']['beone']['summary']
        },
        'pipelines': {
            **source_sample_data['categories']['beone']['report'],
            **json_module_chewiesnake
        }
    }
    pprint_head(json_assembly) if verbose_switch else None

    # %% xCrypt Samples in-place ##############################################

    import json_xcrypt
    json_xcrypt.get_keychain(path = args.keychain)
    json_xcrypt.xcrypt_sample(sample = {'sample': {**source_sample_data['categories']['beone']['summary']}}, mode = args.mode)

    # %% Transfer Samples between Databases #######################################

    # Get an overview of the source db
    print("############################## source|get_run_list") if verbose_switch else None
    pprint.pprint(beoneapi.get_run_list(run_type = 'json_import', connection_name = 'source')) if verbose_switch else None  # this should be populated for private/merge
    pprint.pprint(beoneapi.get_run_list(run_type = 'virtual', connection_name = 'source')) if verbose_switch else None  # this should be empty for private/merge

    # Get an overview of the target db
    print("############################## target|get_run_list") if verbose_switch else None
    pprint.pprint(beoneapi.get_run_list(run_type = 'json_import', connection_name = 'target')) if verbose_switch else None  # this should be empty for public
    pprint.pprint(beoneapi.get_run_list(run_type = 'virtual', connection_name = 'target')) if verbose_switch else None  # this should be populated for public

    target_run_list = beoneapi.get_run_list(run_type = target_run_type, connection_name = 'target')
    target_run_list_df = pandas.json_normalize(target_run_list, record_path = 'samples', sep = '.')  # meta = 'name', meta_prefix = "run_" avoided, use cell split to recover run_name
    target_run_df = deepcopy(target_run_list_df)
    target_run_df[['run', 'name']] = target_run_df.name.str.split("___", expand = True) if not target_run_df.empty else print("The target database has no samples in virtual runs.")
    target_run_df = target_run_df[['run', 'name', '_id']] if not target_run_df.empty else pandas.DataFrame(columns = ['run', 'name', '_id'])
    pprint.pprint(target_run_df) if verbose_switch else None

    # Save Sample from section "Query Local Database" as new Sample to target db, IMPORTANT: the ObjectId remains the same
    if not source_sample_data.get('_id') in target_run_df._id.tolist():
        # Upsert Sample
        new_Sample = beoneapi.save_sample(data_dict = source_sample_data, upsert = True, connection_name = 'target')  # this fails for a user with read-only privileges

        # Associate newly inserted Sample with a new Virtual Run which should contain infos about user, institute and time (only done once, hence commented-out)
        timestamp = datetime.now().strftime("%Y-%m-%dT%X")
        new_run_id = beoneapi.create_virtual_run(name = 'BeONE', ip = 'HB_' + timestamp, samples = source_run['samples'], connection_name = 'target')
        beoneapi.add_samples_to_virtual_run(name = 'BeONE', ip = 'HB_' + timestamp, samples = [source_sample], connection_name = 'target')  # TODO: only add matching sample
    else:
        new_Sample = beoneapi.get_sample(sample_id = source_sample_data['_id'], connection_name = 'target')

    print("############################## target|inserted_Sample") if verbose_switch else None
    pprint_head(new_Sample) if verbose_switch else None

    print("############################## target|get_sample_runs") if verbose_switch else None
    target_search_result_sample = beoneapi.get_sample_runs(sample_ids = [source_sample_data['_id']], connection_name = 'target')
    pprint.pprint(target_search_result_sample) if verbose_switch else None

    print("############################## target|virtual_run_with_inserted_Sample") if verbose_switch else None
    pprint.pprint(beoneapi.get_run_list(run_type = 'virtual', connection_name = 'target')) if verbose_switch else None  # this should be populated for public

    print("############################## target|get_run") if verbose_switch else None
    pprint.pprint(beoneapi.get_run(run_name = 'BeONE', connection_name = 'target')) if verbose_switch else None
    pprint.pprint(beoneapi.get_run(run_name = 'BeONE_2022-02-28T14:18:58', connection_name = 'target')) if verbose_switch else None

    print("############################## target|runs with new_Sample") if verbose_switch else None
    print(f"Runs with the sample {new_Sample['name']}: {', '.join([item['name'] for item in target_search_result_sample])}") if new_Sample is not None else None

    # %% Sync Samples between Databases #######################################

    print("############################## source|table source_all_samples") if verbose_switch else None
    source_all_samples = pandas.DataFrame.from_dict([dict(item, **{'_id': str(item['_id'])}) for item in beoneapi.get_all_samples(connection_name = 'source')]).assign(source = 'source').set_index(keys = '_id')
    print(source_all_samples) if verbose_switch else None

    print("############################## source|table target_all_samples") if verbose_switch else None
    target_all_samples = pandas.DataFrame.from_dict([dict(item, **{'_id': str(item['_id'])}) for item in beoneapi.get_all_samples(connection_name = 'target')]).assign(source = 'target')  # .set_index(keys = '_id') # db is empty
    print(target_all_samples) if verbose_switch else None

    # %% Get Hashed Allele Profiles ###########################################

    retrieve_cgmlst_profiles(connection_name = 'source', schema_name = 'enterobase_senterica_cgmlst', verbose_switch = verbose_switch)


# %% Main Call ################################################################

if __name__ == '__main__':
    main()


# %% Scratch Area #############################################################

def scribbles():

    # Collect URIs
    credential_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "credentials")
    credentials = dict()
    for filename in glob.glob(credential_path + "/" + "*.uri"):
        with open(filename, 'r') as file:
            uri = file.readline().replace("\n", "")
        credentials = {**credentials, filename: uri}

    # pprint.pprint(credentials)

    ## remote BfR @ tipsyDawkins.deNBI or gandalf
    if args.ssh_yaml_source:
        try:
            with open(args.ssh_yaml_source, 'r') as fw:
                beone_bfr_private = yaml.safe_load(fw)
        except FileNotFoundError:
            logging.warning('The MongoDB credentials for the de.NBI cloud were not found')
            beone_bfr_private = None

    host = 'tipsyDawkins.deNBI.ubuntu'
    host = 'gandalf'
    if not beone_bfr_private:
        try:
            ssh_config = read_openssh_config(host = host)
            beone_bfr_private = {**ssh_config,
                                 'key_password': os.getenv('SSH_SECRET'),
                                 'uri'         : get_uri(host = host, db_substring = "bfr_private")}  # the password is overt b/c the ssh key is secret
            with open(os.path.join(os.path.expanduser('~'), '.ssh', 'mongo_denbi_bfr.yaml'), 'w') as fw:  # BEWARE: this also saves the SSH_SECRET aka ssh key password
                yaml.safe_dump(beone_bfr_private, fw, default_flow_style = False, sort_keys = False)
        except FileNotFoundError:
            logging.error('No SSH config for the de.NBI cloud was found in ~/.ssh/config')
    beone_bfr_merge = {**beone_bfr_private, 'uri': get_uri(host = host, db_substring = "bfr_merge")}

    ## local BfR
    host = 'hpc'
    beone_bfr_private = get_uri(host = host, db_substring = "bfr_private")

    ## remote RKI @ looneyventer.deNBI
    try:
        with open(os.path.join(os.path.expanduser('~'), '.ssh', 'mongo_denbi_rki.yaml'), 'r') as fw:
            beone_rki_private = yaml.safe_load(fw)
    except FileNotFoundError:
        logging.warning('The MongoDB credentials for the de.NBI cloud were not found')
        beone_rki_private = None

    host = 'looneyVenter.deNBI.ubuntu'
    host = 'gandalf'
    if not beone_rki_private:
        try:
            ssh_config = read_openssh_config(host = host)
            beone_rki_private = {**ssh_config,
                                 'key_password': os.getenv('SSH_SECRET'),
                                 'uri'         : get_uri(host = host, db_substring = "rki_private")}  # the password is overt b/c the ssh key is secret
            with open(os.path.join(os.path.expanduser('~'), '.ssh', 'mongo_denbi_rki.yaml'), 'w') as fw:  # BEWARE: this also saves the SSH_SECRET aka ssh key password
                yaml.safe_dump(beone_rki_private, fw, default_flow_style = False, sort_keys = False)
        except FileNotFoundError:
            logging.error('No SSH config for the de.NBI cloud was found in ~/.ssh/config')
    beone_rki_merge = {**beone_rki_private, 'uri': get_uri(host = host, db_substring = "rki_merge")}

    ## local RKI
    host = 'hpc'
    beone_rki_private = get_uri(host = host, db_substring = "rki_private")

    ## Public DB @ MongoDB Atlas
    beone_atlas = [value for key, value in credentials.items() if 'atlas' in key]  # read-only access credentials to non-sensitive dummy data, hence the password

    ## Add
    # beoneapi.add_URI(mongoURI = os.getenv('BIFROST_DB_KEY'), connection_name = 'local')
    # beoneapi.add_URI(mongoURI = os.getenv('BIFROST_ATLAS_KEY'), connection_name = 'atlas_write')  # a user with read-write privileges
    # beoneapi.add_URI(mongoURI = beone_atlas, connection_name = 'atlas_read')  # a user with read-only privileges
    beoneapi.add_URI(mongoURI = beone_bfr_private, connection_name = 'bfr_private')
    beoneapi.add_URI(mongoURI = beone_bfr_merge, connection_name = 'bfr_merge')
    beoneapi.add_URI(mongoURI = beone_rki_private, connection_name = 'rki_private')
    beoneapi.add_URI(mongoURI = beone_rki_merge, connection_name = 'rki_merge')
