#!/usr/bin/env python3

'''
json_keychain.py - a script from the BfR Genome Surveillance pipelines:
https://gitlab.com/bfr_bioinformatics/bfr_beone.git
'''

# %% Packages #################################################################

import logging
import os
import json

from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from base64 import b64encode, b64decode


# %% Crypto Functions #########################################################

def get_salt():
    nacl = b64encode(get_random_bytes(AES.block_size)).decode('utf-8')
    return nacl


def generate_keychain(keyfile: str = 'keychain.json', overwrite: bool = False):
    file = os.path.abspath(keyfile)
    if not os.path.isfile(file) or (os.path.isfile(file) and overwrite):
        dcf_catalogs = ('NUTS', 'MTX', 'SAMPNT', 'MTXTYP', 'ZOO_CAT_SMPTYP')
        hierarchy_levels = range(0, 11)
        term_grid = {'password': os.getenv('SECRET')}
        for dcf in dcf_catalogs:
            term_grid[dcf] = {}
            for lvl in hierarchy_levels:
                term_grid[dcf].update({lvl: get_salt()})
        with open(file, 'w') as fp:
            json.dump(obj = term_grid,
                      fp = fp,
                      indent = 4,
                      sort_keys = False,
                      ensure_ascii = False,
                      allow_nan = False)  # Improvement: Use .encode('utf-8') in combo with ensure_ascii=False if None values are omitted instead of coerced to None
        logging.info(f'The keychain has been generated: {file}')
    else:
        logging.warning(f'The existing keychain will not be overwritten: {file}')


def load_keychain(keyfile: str = 'keychain.json'):
    try:
        with open(os.path.abspath(keyfile), 'r') as fp:
            keychain = json.load(fp = fp)
        return keychain
    except FileNotFoundError:
        logging.error(f'The keychain file path is not correct: {fp}')
