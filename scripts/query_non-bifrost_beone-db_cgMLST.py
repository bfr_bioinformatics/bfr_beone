import pymongo
import pandas
from functools import reduce
from datetime import datetime

## set vars
verbose_switch = False
schema_name = 'enterobase_senterica_cgmlst'

## define query
pipeline = [
    {'$match': {
        "$and": [
            {'pipelines.chewiesnake.allele_stats.allele_qc': 'PASS'},
            {'pipelines.chewiesnake.run_metadata.database_information.scheme_name': schema_name}
        ]
    }
    },
    {'$project': {'_id'           : '$_id',
                  'name'          : '$pipelines.chewiesnake.run_metadata.sample_description.sample',
                  'hashid'        : '$pipelines.chewiesnake.allele_stats.hashid',
                  'allele_profile': '$pipelines.chewiesnake.allele_profile'
                  }
     }
]

## query db
print(f"[{datetime.now()}] Querying Database.")
client = pymongo.MongoClient(host = "mongodb://127.0.0.1:26016")
db = client.bfr_bulk
collection = db.beone
allele_profiles_aggregation = list(collection.aggregate(pipeline))
client.close()

## parse aggregation
print(f"[{datetime.now()}] Restructuring metadata from DB query.")
allele_profiles_aggregation_meta = pandas.DataFrame([{k: v for k, v in d.items() if k != 'allele_profile'} for d in allele_profiles_aggregation])
print(f"[{datetime.now()}] Restructuring allele profiles from DB query.")
allele_profiles_aggregation_mapreduce = [pandas.DataFrame(item['allele_profile']).rename(columns = {'allele_crc32': item['name'], 'locus': 'FILE'}) for item in allele_profiles_aggregation]
print(f"[{datetime.now()}] MapReduce dataframe.")
allele_profiles_aggregation_df = reduce(lambda x, y: pandas.merge(x, y, on = 'FILE', how = 'outer'), allele_profiles_aggregation_mapreduce).set_index('FILE').T

## export tables
print(f"[{datetime.now()}] Exporting metadata table.")
allele_profiles_aggregation_meta.to_csv(f"/home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_allele_profile_{schema_name}_metadata.tsv", sep = "\t", index = False)
print(f"[{datetime.now()}] Exporting allele table.")
allele_profiles_aggregation_df.replace(to_replace = 'LOTSC', value = None).astype('Int64').to_csv(f"/home/Brendeba/data/git/bfr_beone/beone_dataset/BeONE_allele_profile_{schema_name}.tsv", sep = "\t", na_rep = "")
print(f"[{datetime.now()}] Done.")
