#!/bin/bash

## [Goal] Helper Functions for Bash Scripts of https://gitlab.bfr.bund.de/4nsz/scripts.git
## Author: Holger Brendebach

## Logging --------------------------------------------------

# messaging template from https://betterdev.blog/minimal-safe-bash-script-template/

logfile="/dev/null" # logfile for detailed, parameter-specific logging
logfile_global="/dev/null" # logfile for script execution logging

msg() {
  # this function is meant to be used to print everything that is NOT a script output.
  echo >&2 -e "${1-}"
}

logentry() {
  local message="$@"
  printf -v logdate '%(%Y-%m-%d %H:%M:%S)T' -1
  echo "[$logdate] $message" | tee -a $logfile $logfile_global
  echo
}

logheader() {
  local message="${1-}"
  echo "${cyan}$message${normal}" | tee >(decolorize >> $logfile)
}

logwarn() {
  local message="${1-}"
  printf -v logdate '%(%Y-%m-%d %H:%M:%S)T' -1
  echo "[$logdate] ${yellow}[WARNING]${normal} $message"
  echo "[$logdate] ${yellow}[WARNING]${normal} $message" | decolorize | tee -a $logfile $logfile_global > /dev/null
}

die() {
  local message="${1-}"
  local code=${2-1} # default exit status 1
  printf -v logdate '%(%Y-%m-%d %H:%M:%S)T' -1
  echo "[$logdate] ${red}[ERROR]${normal} $message"
  echo "[$logdate] ${red}[ERROR]${normal} $message" | decolorize | tee -a $logfile $logfile_global > /dev/null
  exit "$code"
}

## Functions --------------------------------------------------

colorize() {
  ## Fancy echo
  bold=$(tput bold)
  normal=$(tput sgr0)
  red=$(tput setaf 1)
  green=$(tput setaf 2)
  yellow=$(tput setaf 3)
  blue=$(tput setaf 4)
  magenta=$(tput setaf 5)
  cyan=$(tput setaf 6)
  grey=$(tput setaf 7)
  inverted=$(tput rev)
}

decolorize() {
  sed -r 's,\x1B[[(][0-9;]*[a-zA-Z],,g'
}

body() {
  IFS= read -r header
  printf '%s\n' "$header"
  "$@"
}

dev_notes() {
  cat <<- DEV

${red}Development Notes:${normal}

${bold}Goal:${normal}
$(grep -ioP "(?<=\[Goal\] ).*$" "$1")

${bold}Rationales:${normal}
$(grep -ionP "(?<=\[Rationale\]).*$" "$1")

${bold}ToDos:${normal}
$(grep -ionP "(?<=# TODO:).*$" "$1")

${bold}Notes:${normal}
$(grep -ionP "(?<=# NOTE:).*$" "$1")

DEV
  exit
}

interactive_query() {
  read -p "Do you want to continue? [Yy] " -n 1 -r
  echo
  [[ ! $REPLY =~ ^[Yy]$ ]] && exit 1
  echo
}

check_conda() {
  # NOTE: requirement: script_path=$(realpath -P "${BASH_SOURCE[0]}" | dirname )
  conda_env_var="${1-}"
  conda_env_var_name=$(basename "$conda_env_var")
  script_path=${script_path-}

  logheader "Conda Environment:"
  printf "Checking CONDA installation... "
  conda_bin=$(which conda)
  [[ -z ${conda_bin-} ]] && echo "${red}Not Detected${normal}" && conda_status=0 && return 1
  [[ -x "$(command -v ${conda_bin:-conda})" ]] && echo "${green}Detected${normal}" && conda_status=1
  conda_info=$($conda_bin info --json)
  conda_version=$(grep -Po '(?<="conda_version": ).*$' <<< $conda_info | tr -d '",\0')
  conda_base=$(grep -Po '(?<="root_prefix": ).*$' <<< $conda_info | tr -d '",\0')
  conda_env_active=$(grep -Po '(?<="active_prefix_name": ).*$' <<< $conda_info | tr -d '",\0')
  conda_envs=$(grep -Pzo '(?s)(?<="envs": \[\n).*?(?=])' <<< $conda_info | tr -d '",\0' | sed 's/^[[:space:]]*//')
  conda_env_path=$(grep -e "\/${conda_env_var_name}$" <<< $conda_envs) # use first entry if multiple envs with the same name exist, e.g. from other users


  printf "Checking MAMBA installation... "
  mamba_bin=$(which mamba)
  [[ -z ${mamba_bin-} ]] && echo "${red}Not Detected${normal}" && mamba_status=0
  [[ -x "$(command -v ${mamba_bin:-mamba})" ]] && echo "${green}Detected${normal}" && mamba_status=1
  [[ ${mamba_status} == 1 ]] && conda_version=$($mamba_bin --version)

  printf "Checking APK source........... "
  [[ ${script_path##$conda_base} != "$script_path" ]] && bioconda_status=1
  [[ ${bioconda_status-0} == 0 ]] && echo "${bold}GitLab${normal}"
  [[ ${bioconda_status-0} == 1 ]] && echo "${bold}BioConda${normal}"

  echo
  logheader "Conda Parameters:"
  echo "
conda_env_name:|${conda_env_var_name}
conda_base:|${conda_base/$'\n'/' ; '}
conda_env_path:|${conda_env_path/$'\n'/' ; '}
conda_env_active:|$conda_env_active
conda_shell_level:|${CONDA_SHLVL-}
conda_version:|${conda_version/$'\n'/' ; '}
" | column -t -s="|" | tee -a $logfile
  echo
}

## Preload Functions --------------------------------------------------
colorize