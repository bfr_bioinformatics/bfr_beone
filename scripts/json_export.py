#!/usr/bin/env python3

'''
BeONE JSON export from Bifrost DB structure via new Component "beone"
Make sure mongod is up and running and environment variable BIFROST_DB_KEY is set to a TEST database.
$ systemctl restart mongod
'''

# %% Packages #################################################################

import sys
import logging
import traceback
import argparse
from json import loads

import pandas
import beoneapi
from bifrostlib import datahandling

from helper_functions import *


# %% Exception Handling #######################################################

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logging.error(''.join(["Uncaught exception: ", *traceback.format_exception(exc_type, exc_value, exc_traceback)]))


# Install exception handler
sys.excepthook = handle_exception


# %% Define Source of Arguments ###############################################


def parse_arguments():
    parser = argparse.ArgumentParser()

    # output
    parser.add_argument('--json_out', '-o', help = 'Output path for the Sample JSON, e.g. sample.export.json',
                        type = os.path.abspath, required = True)
    # params
    parser.add_argument('--sample', '-s', help = 'String of the Sample name',
                        required = True)
    parser.add_argument('--run_name', '-r', help = 'String of the Run name',
                        required = False)
    parser.add_argument('--db_key', '-d', help = 'JSON String of the local MongoDB database: {"uristring": key}',
                        required = True)
    # log
    parser.add_argument('--log', '-l', help = 'Path for the logfile',
                        type = os.path.abspath, required = True)

    return parser.parse_args()


def create_arguments():
    repo_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    debug_sample = 'Se-BfR-0001'  # 'GMI-17-001-DNA' 'Se-Germany-BfR-0001' 'Se-BfR-0001'
    debug_runname = 'BeONE'
    debug_workdir = os.path.join(repo_dir, "test_data_workshop")  # 'test_data' 'test_data_beone' 'test_data_workshop'

    args = argparse.Namespace(
        json_out = os.path.join(debug_workdir, '8_export', debug_sample + '.export.json'),
        sample = debug_sample,
        run_name = debug_runname,
        db_key = '{"uristring": "mongodb://bfr:mypassword@localhost:28018/beone_bfr_private?authSource=beone_bfr_private"}',  # os.environ['BIFROST_DB_KEY']
        log = os.path.join(debug_workdir, '0_logs', debug_sample + '_exportJson_debug.log')
    )
    return args


# Translate ArgParse to Snakemake Object
def arguments_to_snakemake(args):
    global snakemake
    snakemake_output_df = pandas.DataFrame(
        data = [['json_out', args.json_out]],
        columns = ['index', 'output'])
    snakemake_params_df = pandas.DataFrame(
        data = [['sample', args.sample],
                ['run_name', args.run_name],
                ['db_key', args.db_key]],
        columns = ['index', 'params'])
    snakemake_log_df = pandas.DataFrame(
        data = [['log', args.log]],
        columns = ['index', 'log'])
    snakemake_df = snakemake_output_df.merge(snakemake_params_df.merge(snakemake_log_df, how = 'outer'), how = 'outer')
    snakemake = snakemake_df.set_index(snakemake_df['index'])


# Define Source of Arguments
def python_or_snakemake(debug: bool):
    try:
        snakemake.input[0]
    except NameError:
        if debug:
            arguments_to_snakemake(args = create_arguments())
        else:
            arguments_to_snakemake(args = parse_arguments())


# %% Query Database ###########################################################

def query_beone_sample(uri, sample, run_name):
    beoneapi.add_URI(uri)

    beone_run = beoneapi.get_run(run_name = run_name)
    if beone_run is None:
        logging.error(f'The run {run_name} was not found in the database {uri}')
        print(f'The run {run_name} was not found in the database {uri}')
        return None

    run_sample = [item for item in beone_run['samples'] if item["name"].find(sample) != -1][0]
    if run_sample is None:
        logging.error(f'The sample {sample} was not found in the database {uri}')
        print(f'The sample {sample} was not found in the database {uri}')
        return None

    run_sample_data = beoneapi.get_sample(sample_id = run_sample["_id"])
    if run_sample_data['categories'].get('beone', None) is None:
        logging.error(f'The sample {sample} does not contain the category "beone"')
        print(f'The sample {sample} does not contain the category "beone"')
        return None

    if run_sample_data['categories'].get('cgmlst', {}).get('report', None) is None:
        logging.error(f'The sample {sample} does not contain the category "cgmlst"')
        print(f'The sample {sample} does not contain the category "cgmlst"')
        return None
    elif run_sample_data['categories'].get('cgmlst', {}).get('report', {}).get('chewiesnake', {}).get('allele_stats', None) is None:
        logging.error(f'The sample {sample} does not contain the module "chewiesnake"')
        print(f'The sample {sample} does not contain the module "chewiesnake"')
        return None
    else:
        json_module_chewiesnake = run_sample_data['categories']['cgmlst']['report']
        json_module_chewiesnake['chewiesnake']['allele_stats'].update(run_sample_data['categories']['cgmlst']['summary'])

    json_assembly = {
        'sample'   : {
            **run_sample_data['categories']['beone']['summary']
        },
        'pipelines': {
            **run_sample_data['categories']['beone']['report'],
            **json_module_chewiesnake
        }
    }
    return json_assembly


# %% Main Function ############################################################

def main():
    debug_switch = False

    global snakemake

    # Parse Arguments
    python_or_snakemake(debug = debug_switch)

    # Configure Logging
    logging.basicConfig(filename = snakemake.log['log'],
                        encoding='utf-8',  # NOTE: enabled since upgrade to python v3.9
                        level = logging.INFO,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%Y-%m-%d %H:%M:%S')
    logging.info("json_export.py is runnning in DEBUG mode") if debug_switch else None

    # Check DB Connection
    uri_target_input = json.loads(s = snakemake.params['db_key'])
    os.environ["BIFROST_DB_KEY"] = uri_target_input.get("uristring", uri_target_input)
    if not datahandling.has_a_database_connection():
        logging.error("No connection to the database could be established")
        print("No connection to the database could be established")
        sys.exit(1)

    # Workflow
    json_assembly = query_beone_sample(uri = os.environ["BIFROST_DB_KEY"],
                                       sample = snakemake.params['sample'],
                                       run_name = snakemake.params['run_name'])

    # Write to URLencoded JSON
    if json_assembly:
        with open(snakemake.output['json_out'], 'w') as file:
            json.dump(
                obj = strictJsonObj(
                    nestedObj = json.loads(
                        s = nestedDict_to_json(
                            nestedObj = json_assembly)),
                    uriCompliant = True),
                fp = file,
                indent = 4,
                sort_keys = False,
                ensure_ascii = False,
                allow_nan = False)  # Improvement: Use .encode('utf-8') in combo with ensure_ascii=False if None values are omitted instead of coerced to None
    else:
        sys.exit(1)

    logging.info('MongoDB JSON export for ' + snakemake.params['sample'] + ' finished')
    print('MongoDB JSON export for ' + snakemake.params['sample'] + ' finished')


# %% Main Call ################################################################

if __name__ == '__main__':
    main()
