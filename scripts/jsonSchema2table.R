#!/usr/bin/R

## Libraries for Debug
library(tidyverse)
library(rrapply)
library(urltools)
library(openxlsx)

## Current Data Directory
cdd <- file.path('', 'home', 'Brendeba', 'data', 'git', 'bfr_beone', 'resources')
cdd <- file.path('', 'home', 'brendy', 'data', 'git', 'bfr_beone', 'resources')

target <- "BeONE_schema_filter_v20220619"
target <- "BfR-GenSurv_schema_v20220619_full"

## Load AQUAMIS json schema
schema <- jsonlite::fromJSON(
  txt = file.path(cdd, paste0(target, ".json")),
  simplifyVector = TRUE,
  simplifyDataFrame = TRUE,
  simplifyMatrix = TRUE,
  flatten = FALSE) 

schema_tbl <- unlist(schema, recursive = TRUE) %>% enframe()

.string <- paste(
  schema_tbl$value[schema_tbl$name %in% "properties.pipelines.properties.chewiesnake.properties.allele_profile.items.properties.allele_crc32.type1"],
  schema_tbl$value[schema_tbl$name %in% "properties.pipelines.properties.chewiesnake.properties.allele_profile.items.properties.allele_crc32.type2"])

.x <- schema_tbl %>% 
  add_row(name = "properties.pipelines.properties.chewiesnake.properties.allele_profile.items.properties.allele_crc32.type", value = .string,
          .before = grep(pattern = "properties.pipelines.properties.chewiesnake.properties.allele_profile.items.properties.allele_crc32.type1", x = schema_tbl$name)) %>% 
  filter(grepl(pattern = "type$|title$", x = name, perl = TRUE)) %>% 
  mutate(group = gl(n = n(), k = 2, length = n()),
         code = factor(x = str_extract(string = name, pattern = "description$|type$|title$"),
                       levels = c("title", "description", "type", "path")),
         level = str_count(string = name, pattern = "\\."),
         schema_path = gsub("\\/", ".", dirname(gsub("\\.", "/", name)))) %>% 
  rename(path = name)

.y <- inner_join(
  x = .x %>% select(group, code, value) %>% pivot_wider(data = ., id_cols = group, names_from = code, values_from = value),
  y = .x %>% select(group, level, schema_path) %>% distinct(),
  by = "group") %>% 
  mutate(title = urltools::url_decode(basename(gsub("\\.", "/", schema_path))),  # replace misformated JSON schema title with urldecode value
         json_path = gsub("properties\\.", "", schema_path), 
         level = str_count(string = json_path, pattern = "\\."),  # replace level in JSON schema by level in JSON result 
         module = gsub("^\\w+\\.(\\w+).*", "\\1", json_path)) %>% 
  select(module, title, type, level, json_path)

stopifnot(length(.y[[1]]) == max(as.integer(as.character(.x$group))))

wb <- openxlsx::createWorkbook()
openxlsx::addWorksheet(wb = wb, sheetName = "GenSurv_JSON_Structure")
openxlsx::writeDataTable(wb = wb, sheet = "GenSurv_JSON_Structure", x = .y %>% filter(json_path != "." & module != "."))
openxlsx::saveWorkbook(wb, file = file.path(cdd, paste0(target, ".xlsx")) , overwrite = TRUE)
