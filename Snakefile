# BeONE Genome Surveillance

import os
import yaml
import json
import pandas as pd

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: os.path.join(workflow.basedir,config["workdir"])

# Collect sample names from sample list
samples_raw = pd.read_csv(os.path.join(workflow._workdir,'samples.tsv'),index_col = "sample",sep = "\t")
samples_raw.index = samples_raw.index.astype('str',copy = False)  # in case samples are integers, need to convert them to str
samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out


# Get DB Credentials JSON Schema
def parse_uri(urifile):
    if urifile == "":
        uri = None
    if urifile.endswith(".yaml"):
        with open(os.path.join(workflow.basedir,"credentials",urifile),'r') as file:
            uri = yaml.safe_load(file)
    if urifile.endswith(".uri"):
        with open(os.path.join(workflow.basedir,"credentials",urifile),'r') as file:
            uriline = file.readline().replace("\n","")
            uri = {'uristring': uriline}
    uristring = json.dumps(obj = uri,sort_keys = False,ensure_ascii = False,allow_nan = False)
    return uristring


uri_private = parse_uri(urifile = config.get("urifile_private", ""))
uri_public = parse_uri(urifile = config.get("urifile_public", ""))
uri_merge = parse_uri(urifile = config.get("urifile_merge", ""))

# Get Validation JSON Schema
for file in os.listdir(workflow.basedir + "/resources"):
    if file.startswith("BfR-GenSurv_schema_v") and file.endswith("_full.json"):
        validation_scheme = workflow.basedir + "/resources/" + file

# Get BeONE JSON Schema for Filtering
for file in os.listdir(workflow.basedir + "/resources"):
    if file.startswith("BeONE_schema_filter_v") and file.endswith(".json"):
        filter_scheme = workflow.basedir + "/resources/" + file

# %% Functions ------------------------------------------------------

def _get_path(wildcards, column = 'aquamis'):
    return samples.loc[wildcards.sample, [column]].dropna()[0]


# %% All and Local Rules --------------------------------------------

localrules: all

rule all:
    input:
        expand("5_merge/{sample}.merge.json",sample = samples.index),  # run_merge_json
        expand("6_filtered/{sample}.beone.json",sample = samples.index),  # filter_json
        expand("7_import/{sample}.beone_sample.json",sample = samples.index),  # import_json
        expand("8_export/{sample}.export.json",sample = samples.index),  # export_json
        expand("9_crypto/{sample}.beone.encrypted.json",sample = samples.index),  # encrypt_json
        expand("9_crypto/{sample}.beone.decrypted.json",sample = samples.index),  # decrypt_json
        "10_transfer/placeholder.transfer.encrypted.json",  # sync_from_private_to_public
        "10_transfer/placeholder.transfer.decrypted.json"   # sync_from_public_to_merge

# %% merging rule ---------------------------------------------------

rule merge_json:
    input:
        aquamis = lambda wildcards: _get_path(wildcards,column = 'aquamis'),
    params:
        metadata = lambda wildcards: _get_path(wildcards,column = 'metadata'),
        bakcharak = lambda wildcards: _get_path(wildcards,column = 'bakcharak'),
        chewiesnake = lambda wildcards: _get_path(wildcards,column = 'chewiesnake'),
        format = config['format']
    output:
        merge = "5_merge/{sample}.merge.json"
    message:
        "Running merge_json.py on sample {wildcards.sample}"
    log:
        log = "0_logs/{sample}_mergeJson.log"
    script:
        "scripts/json_merger.py"

# %% create beone json rule -----------------------------------------

rule filter_json:
    input:
        json_in = "5_merge/{sample}.merge.json"
    params:
        sample = "{sample}",
        validation = validation_scheme,
        filter = filter_scheme
    output:
        json_out = "6_filtered/{sample}.beone.json"
    resources:
        cpus = 1
    message:
        "Filtering results according to JSON schema for sample {wildcards.sample}"
    log:
        log = "0_logs/{sample}_filterJson.log"
    script:
        "scripts/json_filter.py"

# %% import beone json rule -----------------------------------------

rule import_json:
    input:
        json_in = "6_filtered/{sample}.beone.json"
    params:
        sample = "{sample}",
        run_name = "BeONE",# if '' a run name will be derived from the sample json
        component_path = os.path.join(workflow.basedir,'bifrost_beone'),
        db_key = uri_private
    output:
        json_run = "7_import/{sample}.beone_run.json",
        json_sample = "7_import/{sample}.beone_sample.json"
    resources:
        cpus = 1
    message:
        "JSON Import into MongoDB for sample {wildcards.sample}"
    log:
        log = "0_logs/{sample}_importJson.log"
    script:
        "scripts/json_import.py"

# %% export beone json rule -----------------------------------------

rule export_json:
    input:
        json_in = "7_import/{sample}.beone_sample.json"  # just a smk flag for sample import
    params:
        sample = "{sample}",
        run_name = "BeONE",# if '' a run name will be derived from the sample json
        db_key = uri_private
    output:
        json_out = "8_export/{sample}.export.json"
    resources:
        cpus = 1
    message:
        "JSON Export from MongoDB for sample {wildcards.sample}"
    log:
        log = "0_logs/{sample}_exportJson.log"
    script:
        "scripts/json_export.py"

# %% json cryptography rules ----------------------------------------

rule encrypt_json:
    input:
        json_in = "6_filtered/{sample}.beone.json"
    params:
        mode = "encrypt",
        keychain = "keychain.json"
    output:
        json_out = "9_crypto/{sample}.beone.encrypted.json"
    resources:
        cpus = 1
    message:
        "JSON Encryption of DCF Terms for sample {wildcards.sample}"
    log:
        log = "0_logs/{sample}_encryptJson.log"
    script:
        "scripts/json_xcrypt.py"

rule decrypt_json:
    input:
        json_in = "9_crypto/{sample}.beone.encrypted.json"
    params:
        mode = "decrypt",
        keychain = "keychain.json"
    output:
        json_out = "9_crypto/{sample}.beone.decrypted.json"
    resources:
        cpus = 1
    message:
        "JSON Decryption of DCF Terms for sample {wildcards.sample}"
    log:
        log = "0_logs/{sample}_decryptJson.log"
    script:
        "scripts/json_xcrypt.py"

# %% database synchronization rules ---------------------------------

rule sync_from_private_to_public:
    input:
        json_in = expand("7_import/{sample}.beone_sample.json",sample = samples.index)
    params:
        db_key_source = uri_private,
        db_key_target = uri_public,
        username=config['username'],
        mode = "encrypt",
        keychain = "keychain_" + config['username'] + ".json",
        drop_collections = '',# '' or "source" or "target"
    output:
        json_out = "10_transfer/placeholder.transfer.encrypted.json"
    resources:
        cpus = 1
    message:
        "Transfer of Samples with on-the-fly JSON Encryption from private-to-public"
    log:
        log = "0_logs/transfer_encryptJson.log"
    script:
        "scripts/json_transfer.py"

rule sync_from_public_to_merge:
    input:
        json_in = "10_transfer/placeholder.transfer.encrypted.json"
    params:
        db_key_source = uri_public,
        db_key_target = uri_merge,
        username=config['username'],
        mode = "decrypt",
        keychain = "keychain_" + config['username'] + ".json",
        drop_collections= ''  # '' or "source" or "target"
    output:
        json_out = "10_transfer/placeholder.transfer.decrypted.json"
    resources:
        cpus = 1
    message:
        "Transfer of Samples with on-the-fly JSON Decryption from public-to-merge"
    log:
        log = "0_logs/transfer_decryptJson.log"
    script:
        "scripts/json_transfer.py"


# %% logging information --------------------------------------------

onstart:
    shell('echo -e "running\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status_json_merger.txt')

onsuccess:
    shell('echo -e "success\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status_json_merger.txt')

onerror:
    shell('echo -e "error\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status_json_merger.txt'),
    print("An error occurred")
