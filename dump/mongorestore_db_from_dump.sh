#!/bin/bash
set -Eeuo pipefail

## (1) Argument logic - template from https://betterdev.blog/minimal-safe-bash-script-template/
cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "Done."
}

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS]

Description:
This script is a wrapper for the command 'mongorestore' of the MongoDB Database Tools.
It requires a GZ archive of the database stored in the connection URI file.
If the string --ssh_host is provided, a SSH tunnel will be set-up for connecting to the remote MongoDB instance.
Note: the remote database is hardcoded to "beone_*_private"
.For more information, please visit https://gitlab.com/bfr_bioinformatics/bfr_beone

Options:
  -u, --uri_target           [REQUIRED] Path of the URI file for the target BeONE MongoDB database to restore to.
  -d, --dump_file            Path of the 'mongodump' GZ archive. If none is provided, the latest from "${SCRIPT_DIR}/" will be restored.
  -s, --ssh_host             Name of the server running the MongoDB instance, as stated in the ~/.ssh/config
  -x, --drop                 Caution! mongorestore will drop collections that are existing in both DB as well as in backup collection.

EOF
  exit
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  uri_file_target=''
  dump_file=''
  ssh_host=''
  drop=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -u | --uri_target)
      uri_file_target="$2"
      shift 2
      ;;
    -d | --dump_file)
      dump_file="$2"
      shift 2
      ;;
    -s | --ssh_host)
      ssh_host="$2"
      shift 2
      ;;
    -s | --drop)
      drop="--drop"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
  done

  ## Debug vars
#  ssh_host="looneyVenter.deNBI.bfr"
  uri_file_target="${INSTALL_DIR}/credentials/looneyVenter.bfr.beone_bfr_merge.uri"

  ## check required params and arguments
  [[ -z "${uri_file_target-}" ]] && die "Please provide a parameter for the argument --uri_source. Try $(basename "${BASH_SOURCE[0]}") --help."

  return 0
}

# Retrieve script paths
SCRIPT=$(readlink -f $0)
SCRIPT_DIR=$(dirname $SCRIPT)
INSTALL_DIR=$(dirname $SCRIPT_DIR)

# Setup cleanup-on-exit function
trap cleanup SIGINT SIGTERM ERR EXIT

# Parse arguments
echo "For help, type $(basename "${BASH_SOURCE[0]}") --help"
echo ""
parse_params "$@"


## (2) Script logic
timestamp=$(date '+%Y%m%d%H%M%S')

# check dump archive path and find the latest in the default directory if none is provided
[[ -z "${dump_file-}" ]] && dump_file=$(find ${SCRIPT_DIR} -type f -path "*beone_[0-9a-zA-Z]*_private*" -name "*_mongodump.gz" -print0 | xargs -r -0 ls -1 -t | head -1)
dump_file_dir=$(dirname "$dump_file")
database_source=$(echo "$dump_file_dir" | grep -o "beone_[0-9a-zA-Z]*_private")

# read uri file
uri_target=$(head -n1 "$uri_file_target")
port_target=$(echo $uri_target | sed -E "s@.*:([0-9]{5})\/.*@\1@g")
database_target=$(basename $uri_target | sed "s/\?.*//g")

# remove db string from uri
uri_target=$(echo $uri_target | sed -E "s@/$database_target@/@g")

# find next available port and start ssh tunnel
if [[ -z "${ssh_host}" ]]; then
  echo "No SSH connection defined, using local MongoDB" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongorestore.log
else
  NEXTPORT_target=`comm -23 <(seq 28100 28999 | sort) <(ss -tan | awk '{print $4}' | cut -d':' -f2 | grep "[0-9]\{1,5\}" | sort -u) | sort -n | head -n 1`
  echo "Connect to the remote target MongoDB with the SSH tunnel: '$NEXTPORT_target:localhost:$port_target'"  2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongorestore.log
  echo "[Command]  ssh -L $NEXTPORT_target:localhost:$port_target $ssh_host -f -N" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongorestore.log
                   ssh -L $NEXTPORT_target:localhost:$port_target $ssh_host -f -N
  tunnel_uri_target=$(echo $uri_target | sed -E "s/$port_target/$NEXTPORT_target/g")
fi

# restore archive to target db
[[ $drop == "--drop" ]] && echo "[Warning] Dropping all preexisting databases"
echo "[Command]  mongorestore --verbose --uri="${tunnel_uri_target:-$uri_target}" $drop --nsFrom="${database_source}.*" --nsTo="${database_target}.*" --gzip --archive="$dump_file"" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongorestore.log
                 mongorestore --verbose --uri="${tunnel_uri_target:-$uri_target}" $drop --nsFrom="${database_source}.*" --nsTo="${database_target}.*" --gzip --archive="$dump_file"  2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongorestore.log

# stop ssh tunnel
if [[ ! -z "${ssh_host}" ]]; then
  process_id=$(ps aux | grep -m 1 "ssh -L $NEXTPORT_target:localhost:$port_target" | awk '{print $2}')
  echo "[Command]  kill -9 $process_id" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongorestore.log
  kill -9 $process_id
fi
