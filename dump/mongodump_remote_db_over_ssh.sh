#!/bin/bash
set -Eeuo pipefail

## (1) Argument logic - template from https://betterdev.blog/minimal-safe-bash-script-template/
cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "Done."
}

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS]

Description:
This script is a wrapper for the command 'mongodump' of the MongoDB Database Tools.
It generates a GZ archive of the database stored in the connection URI file.
If the string --ssh_host is provided, a SSH tunnel will be set-up for connecting to the remote MongoDB instance.
Note: remote db is hardcoded to "beone_*_private".
For more information, please visit https://gitlab.com/bfr_bioinformatics/bfr_beone

Options:
  -u, --uri_source           [REQUIRED] Path of the URI file for the source BeONE MongoDB database to dump from.
  -d, --dump_base_path       Parent directory path where the GZ is dumped to. If none is provided, "${SCRIPT_DIR}/" will be used.
  -s, --ssh_host             Name of the server running the MongoDB instance, as stated in the ~/.ssh/config

EOF
  exit
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  uri_file_source=''
  dump_base_path=''
  ssh_host=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -u | --uri_source)
      uri_file_source="$2"
      shift 2
      ;;
    -d | --dump_base_path)
      dump_base_path="$2"
      shift 2
      ;;
    -s | --ssh_host)
      ssh_host="$2"
      shift 2
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
  done

  ## Debug vars
#  ssh_host="looneyVenter.deNBI.bfr"
  uri_file_source="${INSTALL_DIR}/credentials/looneyVenter.bfr.beone_bfr_private.uri"

  ## check required params and arguments
  [[ -z "${uri_file_source-}" ]] && die "Please provide a parameter for the argument --uri_source. Try $(basename "${BASH_SOURCE[0]}") --help."
  [[ -z "${dump_base_path-}" ]] && dump_base_path="$SCRIPT_DIR"

  return 0
}

# Retrieve script paths
SCRIPT=$(readlink -f $0)
SCRIPT_DIR=$(dirname $SCRIPT)
INSTALL_DIR=$(dirname $SCRIPT_DIR)

# Setup cleanup-on-exit function
trap cleanup SIGINT SIGTERM ERR EXIT

# Parse arguments
echo "For help, type $(basename "${BASH_SOURCE[0]}") --help"
echo ""
parse_params "$@"


## (2) Script logic
timestamp=$(date '+%Y%m%d%H%M%S')

# read uri file
uri_source=$(head -n1 "$uri_file_source")
port_source=$(echo $uri_source | sed -E "s@.*:([0-9]{5})\/.*@\1@g")
database_source=$(basename $uri_source | sed "s/\?.*//g")

dump_file_dir="${dump_base_path}/${database_source}"
[[ ! -d "${dump_file_dir}" ]] && mkdir -p ${dump_file_dir}

# find next available port and start ssh tunnel
if [[ -z "${ssh_host}" ]]; then
  echo "No SSH connection defined, using local MongoDB" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongodump.log
else
  NEXTPORT_source=`comm -23 <(seq 28100 28999 | sort) <(ss -tan | awk '{print $4}' | cut -d':' -f2 | grep "[0-9]\{1,5\}" | sort -u) | sort -n | head -n 1`
  echo "Connect to the remote source MongoDB with the SSH tunnel: '$NEXTPORT_source:localhost:$port_source'"
  echo "[Command]  ssh -L $NEXTPORT_source:localhost:$port_source $ssh_host -f -N"
                   ssh -L $NEXTPORT_source:localhost:$port_source $ssh_host -f -N
  tunnel_uri_source=$(echo $uri_source | sed -E "s/$port_source/$NEXTPORT_source/g")
fi

# dump collection in archive to directory
collections=(runs samples sample_components components)
echo "[Command]  mongodump --verbose --uri="${tunnel_uri_source:-$uri_source}" --db="$database_source" --excludeCollection="dummy" --gzip --archive=${dump_file_dir}/${timestamp}_mongodump.gz" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongodump.log
                 mongodump --verbose --uri="${tunnel_uri_source:-$uri_source}" --db="$database_source" --excludeCollection="dummy" --gzip --archive=${dump_file_dir}/${timestamp}_mongodump.gz  2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongodump.log

# stop ssh tunnel
if [[ ! -z "${ssh_host}" ]]; then
  process_id=$(ps aux | grep -m 1 "ssh -L $NEXTPORT_source:localhost:$port_source" | awk '{print $2}')
  echo "[Command]  kill -9 $process_id" 2>&1 | tee -a ${dump_file_dir}/${timestamp}_mongodump.log
  kill -9 $process_id
fi

echo "To restore, use the script: ${SCRIPT_DIR}/mongorestore_db_from_dump.sh"
