## AQUAMIS_schema_v20220125.json

This schema is based on an AQUAMIS analysis of the GMI-17-001-DNA test dataset
and the conversion of the full JSON result by the online schema generator
ExtendsClass (https://extendsclass.com/json-schema-validator.html) and
augmentation with a 'description' field dummy for later redaction.

### Recipe for full schema generation

1) paste `bfr_beone/test_data/merge/GMI-17-001-DNA.merge.json` into JSON box of above URL
2) check box "Options for schema generation: Required"
3) Generate schema from JSON
4) paste JSON schema into new AQUAMIS_schema_vYYYYMMDD.json file
5) PyCharm | Code | Reformat Code (CTRL+ALT+L) with indent=4 (result: step1)
6) Regex Steps (result: step2):
   1) \s+"default":.* | <delete>
   2) "properties": \{\s*\} | "properties": {}
   3) "required": \[\s*\] | "required": []
   4) (?<!instance.",\n[ ]+)"type": | "description": "An explanation about the purpose of this instance.",\n"type":
7) other mods:
   1) remove strings in required list from key "allele_profile" # TODO: turn "allele_profile" into array
   2) "type": "null" is not allowed: replace e.g. "count_circular_contigs" | from "type": "null" to "type": "integer"
   3) update "AQUAMIS_schema_vYYYYMMDD.json" keys in file "AQUAMIS_schema_vYYYYMMDD.json"
8) optional mods:
   1) remove items from nucleotide arrays
   2) URL decode into title string
   3) replace root "title": "BfR Genome Surveilance Sample JSON Schema" & "description": "version: YYYYMMDD",
9) PyCharm | Code | Reformat Code (CTRL+ALT+L) with indent=4
10) update aquamis.py at default --json_schema and --json_filter
11) delete directory AQUAMIS/test_data/json/filtered
12) rerun AQUAMIS to complete missing files

If file SRR498433.aquamis_validationErrors.json during the above integrity check, relax type from integer to number.

### Schema Filter Integrity Check

1) copy AQUAMIS_schema_vYYYYMMDD.json to AQUAMIS_schema_filter_vYYYYMMDD.json
2) remove any occurrence (n=8) of fields "circular_contigs" in AQUAMIS_schema_**filter**_vYYYYMMDD.json
3) delete directory AQUAMIS/test_data/json/filtered
4) rerun AQUAMIS to complete missing files
5) compare diff of new AQUAMIS/test_data/json/filtered vs. old AQUAMIS/test_data/json/post_qc

## AQUAMIS_schema_filter_v20220125.json

This schema is based on above full schema and was arbitrarily redacted (circular_contigs field removed). It is used as
a positive filter for normalization of full results via the package CERBERUS
in the snakemake rule 'filter_json'.

## kraken2_db_hashes.json

SHA256 hashes of various kraken database files and their location