#!/bin/bash

## [Goal] Wrapper for scripts parsing BeONE sample data
## Author: Holger Brendebach

## Shell behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e
trap cleanup SIGINT SIGTERM ERR EXIT

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
source "$script_path/scripts/helper_functions.sh"
repo_path=$script_path
# NOTE: this script has to reside in the base directory of https://gitlab.com/bfr_bioinformatics/bfr_beone.git

## (1) Argument logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name -u <user> -s <host> [Options]

${bold}Description:${normal}
This script is a wrapper for the BeONE Workshop dataset, concatenating scripts from the toolbox.
For more information, please visit https://gitlab.com/bfr_bioinformatics/bfr_beone

${bold}Required Named Arguments:${normal}
  -u, --username             Username string, e.g. your institute 'bfr'
  -s, --hostname             Server-/Hostname string as prefix for the credentials/.uri filename, e.g. your server 'looneyVenter'

${bold}Optional Named Arguments:${normal}
  -d, --data-dir             Path to the parent directory of BfR-ABC JSON sets
  -n, --dryrun               Show only the final Snakemake command

USE
  exit 0
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # echo "BeONE_Wrapper executed."
}

parse_params() {
  local args=("$@")

  ## default values of variables
  username=''
  hostname=''
  jobs=1
  json_format=json # TODO: turn hard-coded format=json_line into CLI option
  json_format=json_line # TODO: turn hard-coded format=json_line into CLI option

  ## default directory with jsons
  dir_data=$repo_path/test_data_beone
  dir_data=$repo_path/beone_dataset
  dir_data=$repo_path/test_data_workshop
  dir_data=$repo_path/beone_simulation

  conda_recipe=$repo_path/envs/pyjson.yaml
  conda_env_name=$(head -1 "$conda_recipe" | cut -d' ' -f2)

  ## default values of switches
  dryrun=false

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose) set -x ;;
      -u | --username)
        username="$2"
        shift 2
        ;;
      -s | --hostname)
        hostname="$2"
        shift 2
        ;;
      -d | --data-dir)
        dir_data="$2"
        shift 2
        ;;
      -n | --dryrun)
        dryrun=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required params and arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. Try ${blue}bash $script_real --help${normal}"
  [[ -z ${username-} ]] && die "Please provide a parameter for the argument --username. Try ${blue}bash $script_real --help${normal}"
  [[ -z ${hostname-} ]] && die "Please provide a parameter for the argument --hostname. Try ${blue}bash $script_real --help${normal}"
  [[ -z ${dir_data-} ]] && die "Please provide a parameter for the argument --dir_data. Try ${blue}bash $script_real --help${normal}"

  ## MongoDB URIs
  # TODO: remove hard-coded public server
  # TODO: allow mongo instances on different servers
  hostname_private=$hostname
  hostname_public=$hostname # TODO: this is for testing, when public is on the same container as private/merge DBs
  hostname_public="timelessEuler"
  hostname_merge=$hostname

  beone_private="${hostname_private}.${username}.beone_${username}_private.uri"
  beone_public="${hostname_public}.${username}.beone_${username}_public.uri" # TODO: this is for testing, when public is on the same container as private/merge DBs
  beone_public="${hostname_public}.${username}.beone_${username}_public.yaml"
  beone_merge="${hostname_merge}.${username}.beone_${username}_merge.uri"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Parse Arguments --------------------------------------------------
parse_params "$@"

## (2) Script logic

## Define paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths() use realpaths for proper awk splitting

  ## logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile=$dir_data/$logfile_name
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
beone_private:|$beone_private
beone_public:|$beone_public
beone_merge:|$beone_merge
dir_data:|$dir_data
dryrun:|$dryrun
" | column -t -s="|" | tee -a $logfile $logfile_global
  echo

  logheader "To monitor the Snakemake log use:"
  echo "tail -F -s3 \$(find $dir_data/.snakemake/log -name "*.snakemake.log" -print0 | xargs -r -0 ls -1 -t | head -1)" | tee -a $logfile
  echo
}

## Modules --------------------------------------------------

activate_conda() {
  ## [Rationale] activate_conda() look into conda recipe from the repo to check if the conda name is available on the system

  check_conda "$conda_env_name"

  [[ -z $conda_env_path ]] && die "The Conda environment was not found on this system."
  if [[ ${conda_env_active-} != $conda_env_name ]]; then
    logentry "Conda environment $conda_env_name has not been activated yet. Activating Conda: $conda_env_path"
    source $conda_base/etc/profile.d/conda.sh
    conda activate $conda_env_path
  else
    logentry "Conda environment $conda_env_name was already activated."
  fi
}

checks() {
  [[ -d $dir_data ]] || die "The data directory does not exist."
  [[ -f $dir_data/samples.tsv ]] || die "The data directory does not contain a samples sheet."
  [[ -f $repo_path/credentials/$beone_private ]] || die "The credential directory does not contain the URI file: $beone_private"
  [[ -f $repo_path/credentials/$beone_public  ]] || die "The credential directory does not contain the URI file: $beone_public"
  [[ -f $repo_path/credentials/$beone_merge   ]] || die "The credential directory does not contain the URI file: $beone_merge"
  [[ $jobs -gt 1 ]] && die "With jobs=$jobs greater than 1, parallel execution of the Snakemake rule \"import_json\" may cause errors."
}

run_snakemake() {
  ## [Rationale] run_snakemake() build a complex snakemake call

  ## for CLI options, see https://snakemake.readthedocs.io/en/stable/executing/cli.html
  read -r -d '' smk_cmd <<- CMD
  snakemake
    --snakefile $repo_path/Snakefile
    --jobs $jobs
    --config workdir=$dir_data
    username=$username
    format=$json_format
    urifile_private=$beone_private
    urifile_public=$beone_public
    urifile_merge=$beone_merge
    --until filter_json
CMD
  ## unused options
#    --dryrun --quiet
#    --until merge_json
#    --unlock
#    --forceall --report reports/smk_report.html --dryrun --quiet
#    --configfile $dir_data/config_beone.yaml
#    --allowed-rules write_pre_assembly_report write_report parse_json_after_qc filter_json delete_ephemeral_dirs
#    --ignore-incomplete
#    --forcerun download_mash_reference
#    --until download_mash_reference
#    --summary
#    --notemp
#    --keep-incomplete
#    --list-params-changes
#    --cleanup-metadata  $(snakemake --configfile $dir_data/config.yaml --snakefile $repo_path/Snakefile --list-params-changes)
#    --list-input-changes
#    --list-code-changes
#    --list-version-changes
#    --cleanup-shadow

  cmd_array=($(echo "$smk_cmd"))
  logheader "Running BeONE workflow via direct Snakemake command:"
  echo "${cmd_array[@]}" | tee -a $logfile
  echo
  [[ $dryrun == false ]] && "${cmd_array[@]}"
}

## (3) Main --------------------------------------------------

define_paths
logentry "$script_real" "START" # from helper_functions.sh
activate_conda
show_info

checks
run_snakemake

echo "All steps were logged to: $logfile" | tee -a $logfile $logfile_global
logentry "$script_real" "STOP" # from helper_functions.sh
