.
├── sample:/
│   ├── run_metadata:
│   ├── pipeline: AQUAMIS/
│   │   ├── timestamp: '2022-02-10 22:59:47.558201'
│   │   ├── sample_description:/
│   │   │   ├── sample: GMI-17-001-DNA
│   │   │   ├── read1_file: "./AQUAMIS/test_data/raw/GMI-17-001-DNA_S1_L001_R1_001.fastq.gz"
│   │   │   ├── read2_file: "./AQUAMIS/test_data/raw/GMI-17-001-DNA_S1_L001_R2_001.fastq.gz"
│   │   │   ├── read1_md5: cdee08db6baf597f1f75478b0727cfd2
│   │   │   └── read2_md5: 28251a842f8eca7f7061a097635cb21a
│   │   ├── pipeline_metadata: {}
│   │   └── database_information: {}
│   ├── pipeline: chewieSnake/
│   │   ├── timestamp: '2022-02-10 23:42:52.792432'
│   │   ├── sample_description:/
│   │   │   ├── sample: GMI-17-001-DNA
│   │   │   ├── fasta_file: "./chewieSnake/testdata/chewiesnake/data/GMI-17-001-DNA.fasta"
│   │   │   ├── fasta_md5: b986367a050f015d1fa818091f3e091c
│   │   │   ├── allele_profile: "./chewieSnake/testdata/chewiesnake/test_202202032202/cgmlst/GMI-17-001-DNA/allele_profiles.tsv"
│   │   │   ├── allele_profile_md5: 98af37389dd9803c6432a41dc55a2ab5
│   │   │   └── timestamp_analysis: '2022-02-08'
│   │   ├── pipeline_metadata: {}
│   │   └── database_information: {}
│   ├── summary: {}
│   ├── qc_assessment: {}
│   └── metadata: {}
└── pipelines:/
    └── chewiesnake:/
        ├── run_metadata: {}
        ├── allele_profile:
        │   └── locus: SC0831.fasta/
        │   └── allele_crc32: 2486472784
        │   └── locus: SEN0401.fasta/
        │   └── allele_crc32: 524152461
        │   └── ...
        └── allele_stats: {}
