# BfR-BeONE

JSON Merger, JSON Filter, BeONE data model, JSON Import/Export, JSON Encryption,

## Description

This collection of scripts merges the results of BfR Genomic Surveillance pipelines and generates a data model for the exchange between BeONE partners.

## Table of Contents

[[_TOC_]]

## Installation

### From Source

```
cd <path_to_repo_parent>
git clone https://gitlab.com/bfr_bioinformatics/bfr_beone
```

This repo relies on the package manager `conda` for all dependencies. Please set up conda on your system as explained [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).
Please provide a conda `base` environment with the dependency manager `mamba` (mamba is faster in resolving dependencies), then:

```
mamba env create -f <path_to_repo>/envs/pyjson.yaml
conda activate pyjson
pip install -i https://test.pypi.org/simple/ beoneapi==0.2.0
bash <path_to_repo>/scripts/update_bifrostlib_jsonSchema.sh
```

This creates an environment named `pyjson` containing all [dependencies](/envs/pyjson.yaml). It is found under `<path_to_conda>/envs/pyjson`.

## Snakemake Automation

The Snakemake path system in not fully flexible yet. Provide a `test_data` sub-directory within the repository and create the tab-delimited table `test_data/samples.tsv`, e.g. as shown with the provided test dataset.
Missing JSON files should have a whitespace as placeholder.

## Select a _local, private_ MongoDB and import data

Either set the environmental variable BEONE_DB_KEY to an URI string or point the snakemake config variable `urifile` to a file in the directory `<path_to_repo>/credentials` holding the MongoDB URI string.
BEONE_DB_KEY has a higher priority.

```shell
cd <path_to_repo_base>
export BEONE_DB_KEY=mongodb://bfr:mypassword@127.0.0.1:28018/beone_bfr_private
snakemake -j 1 --config workdir=test_data format=json
```
or
```shell
cd <path_to_repo_base>
snakemake -j 1 --config workdir=test_data format=json urifile=looneyVenter.bfr.beone_bfr_private.uri
```

Choose `format=json_line` if the sample column refers to a multi-sample metadata file according to JSON Line specification.

## BeONE data model

Note: empty braces are populated but were removed for this view for clarity.

The JSON path `sample|summary` holds most assembly metrics that have to be matched by dictionary translation from and to other pipelines. A cast of the `resources/BeONE_schema_filter_v20220619.json` into an Excel structure may help with the translation,
see `resources/BeONE_schema_filter_v20220619.xlsx`

```text
.
├── sample:
│   ├── run_metadata: [this is a list of any executed pipeline that is associated with the sample]
│   ├── pipeline: AQUAMIS
│   │   ├── timestamp: '2022-02-10 22:59:47.558201'
│   │   ├── sample_description:
│   │   │   ├── sample: GMI-17-001-DNA
│   │   │   ├── read1_file: "./AQUAMIS/test_data/raw/GMI-17-001-DNA_S1_L001_R1_001.fastq.gz"
│   │   │   ├── read1_md5: cdee08db6baf597f1f75478b0727cfd2
│   │   │   ├── read2_file: "./AQUAMIS/test_data/raw/GMI-17-001-DNA_S1_L001_R2_001.fastq.gz"
│   │   │   ├── read2_md5: 28251a842f8eca7f7061a097635cb21a
│   │   │   ├── fasta_file: "./AQUAMIS/test_data/Assembly/assembly/GMI-17-001-DNA.fasta"
│   │   │   └── fasta_md5: b986367a050f015d1fa818091f3e091c
│   │   ├── pipeline_metadata: {}
│   │   └── database_information: {}
│   ├── pipeline: chewieSnake
│   │   ├── timestamp: '2022-02-10 23:42:52.792432'
│   │   ├── sample_description:/
│   │   │   ├── sample: GMI-17-001-DNA
│   │   │   ├── fasta_file: "./chewieSnake/testdata/chewiesnake/data/GMI-17-001-DNA.fasta"
│   │   │   ├── fasta_md5: b986367a050f015d1fa818091f3e091c
│   │   │   ├── allele_profile: "./chewieSnake/testdata/chewiesnake/test_202202032202/cgmlst/GMI-17-001-DNA/allele_profiles.tsv"
│   │   │   ├── allele_profile_md5: 98af37389dd9803c6432a41dc55a2ab5
│   │   │   └── timestamp_analysis: '2022-02-08'
│   │   ├── pipeline_metadata: {}
│   │   └── database_information: {}
│   ├── summary: {}        [BeONE result metrics]
│   ├── qc_assessment: {}  [BeONE qc metrics]
│   └── metadata: {}       [BeONE sample sheet data]
└── pipelines:
    └── chewiesnake:
        ├── run_metadata: {}
        ├── allele_profile:
        │   └── locus: SC0831.fasta/
        │   └── allele_crc32: 2486472784
        │   └── locus: SEN0401.fasta/
        │   └── allele_crc32: 524152461
        │   └── ...
        └── allele_stats: {}
```

## Script Collection

### BfR JSON Merger

#### Help

```shell
python scripts/json_merger.py --help
```

```text
This is the JSON Merger version 0.0.1
usage: json_merger.py [-h] -m METADATA [-a AQUAMIS] -c CHEWIESNAKE -b BAKCHARAK
                      [-o OUTFILE]

arguments:
  -h, --help            show this help message and exit
  -m METADATA, --metadata METADATA
                        Path to JSON file of the BeONE sample sheet metadata
  -l METADATA, --metadata_jsonl METADATA_JSONL
                        Path to JSON Line file of the BeONE sample sheet metadata
  -a AQUAMIS, --aquamis AQUAMIS
                        [REQUIRED] Path to JSON file of AQUAMIS results
  -c CHEWIESNAKE, --chewiesnake CHEWIESNAKE
                        Path to JSON file of chewieSnake results
  -b BAKCHARAK, --bakcharak BAKCHARAK
                        Path to JSON file of BakCharak results
  -o OUTFILE, --outfile OUTFILE
                        [REQUIRED] Output file
```


### JSON Validation and Filtering with JSON schemes

#### Help

```shell
python scripts/json_filter.py -h
```

```text
usage: json_filter.py [-h] --json_in JSON_IN --json_out JSON_OUT --sample SAMPLE --validation VALIDATION --filter FILTER --log LOG

optional arguments:
  -h, --help            show this help message and exit
  --json_in JSON_IN, -i JSON_IN
                        Path to the JSON file of combined BfR pipeline results, e.g. sample.merge.json
  --json_out JSON_OUT, -o JSON_OUT
                        Path to the JSON file of filtered results in the BeONE format, e.g. sample.beone.json
  --sample SAMPLE, -s SAMPLE
                        String of the Sample name
  --validation VALIDATION, -v VALIDATION
                        Path to the BeONE JSON schema for Validation, default: /home/brendy/data/git/bfr_beone/resources/BfR-GenSurv_schema_v20220619_full.json
  --filter FILTER, -f FILTER
                        Path to the BeONE JSON schema for Filtering, default: /home/brendy/data/git/bfr_beone/resources/BeONE_schema_filter_v20220619.json
  --log LOG, -l LOG     Path for the logfile
```

#### Execution

For now, this python script is a executable with snakemake dependencies that are provided by the file `json_filter_debug.py` where the sample name `GMI-17-001-DNA` is hardcoded. It takes the merged JSON `test_data/merge/GMI-17-001-DNA.merge.json` as input and evalutates it with schemes
from `./resources`. The filtered output is written to `test_data/filtered/GMI-17-001-DNA.beone.json`. Compare it with a _diff_ to the input json to see the effects.

Watch out for files matching `*_validationErrors.json` where the input did not match the type defined in the schema. The corresponding log file `test_data/logs/GMI-17-001-DNA_filterJson_debug.log`
With the current settings, mismatching values will be kept in the filtered output file. The type change from-float-to-string causing the error `"busco_duplicates": ["must be of float type"]` was manually introduced into the test input file as a proof-of-concept. When the json_merger.py
overwrites `test_data/merge/GMI-17-001-DNA.merge.json`, the error will no longer appear.


### BeONE Sample Import

#### Help

```shell
python scripts/json_import.py -h
```

```text
usage: json_import.py [-h] --json_in JSON_IN --json_run JSON_RUN --json_sample JSON_SAMPLE --sample SAMPLE [--run_name RUN_NAME] --component_path COMPONENT_PATH --db_key DB_KEY --log LOG

optional arguments:
  -h, --help            show this help message and exit
  --json_in JSON_IN, -i JSON_IN
                        Path to JSON file of BeONE results, e.g. sample.beone.json
  --json_run JSON_RUN, -x JSON_RUN
                        Output path for the db representation of the created run object, e.g. sample.bifrost_run.json
  --json_sample JSON_SAMPLE, -y JSON_SAMPLE
                        Output path for the db representation of the created sample object, i.e. e.g. sample.bifrost_sample.json
  --sample SAMPLE, -s SAMPLE
                        String of the Sample name
  --run_name RUN_NAME, -r RUN_NAME
                        String of the Run name. If left empty, a run name will be created in section prepare_args()
  --component_path COMPONENT_PATH, -c COMPONENT_PATH
                        Path to the BIFROST component BeONE description directory, default: /home/brendy/data/git/bfr_beone
  --db_key DB_KEY, -d DB_KEY
                        String of the local MongoDB database
  --log LOG, -l LOG     Path for the logfile
```

### BeONE Sample Export

#### Help

```shell
python scripts/json_export.py -h
```

```text
usage: json_export.py [-h] --json_out JSON_OUT --sample SAMPLE [--run_name RUN_NAME] --db_key DB_KEY --log LOG

optional arguments:
  -h, --help            show this help message and exit
  --json_out JSON_OUT, -o JSON_OUT
                        Output path for the Sample JSON, e.g. sample.export.json
  --sample SAMPLE, -s SAMPLE
                        String of the Sample name
  --run_name RUN_NAME, -r RUN_NAME
                        String of the Run name
  --db_key DB_KEY, -d DB_KEY
                        String of the local MongoDB database
  --log LOG, -l LOG     Path for the logfile
```

### BeONE Metadata Cryptography

#### Help

```shell
python scripts/json_xcrypt.py -h
```

```text
usage: json_xcrypt.py [-h] --json_in JSON_IN --json_out JSON_OUT --mode MODE [--keychain KEYCHAIN] --log LOG

optional arguments:
  -h, --help            show this help message and exit
  --json_in JSON_IN, -i JSON_IN
                        Path to the input JSON file in the BeONE format, e.g. (A) sample.beone.json or (B) sample.encrypted.json
  --json_out JSON_OUT, -o JSON_OUT
                        Path to the output JSON file in the BeONE format, e.g. (A) sample.encrypted.json or (B) sample.decrypted.json
  --mode MODE, -m MODE  String of the cryptographic mode, either (A) "encrypt" or (B) "decrypt"
  --keychain KEYCHAIN, -k KEYCHAIN
                        Path to the BeONE keychain of AES keys; will be generated if none is provided for encryption
  --log LOG, -l LOG     Path for the logfile
```

## BeONE Workshop & Simulation at SSI, Copenhagen, DK

For the working material of the BeONE Workshop & Simulation, please visit the [Wiki pages](https://gitlab.com/bfr_bioinformatics/bfr_beone/-/wikis/home). 

## Authors and acknowledgment

* Holger Brendebach <Holger.Brendebach (at) bfr.bund.de>

## License

see [License](/LICENSE)

## Project status

under active development, beta status
